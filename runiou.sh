#!/usr/bin/env bash

###########################################################################
#
#  runiou - A bash script frontend for Cisco IOU.
#
#  version 0055
#
#  Copyright 2012-2019 by Miguel Scapolla <mascapolla@yahoo.com.ar>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
###########################################################################

#--- runiou configuration.
function scrconfigure {
    #--- Return:
    #---  CFGxxxx: Global configuration variables.

    #--- OS.
    CFGOPERSYSTEM="OTHER"
    if [ "`uname -s`" == "Linux" ]; then
        CFGOPERSYSTEM="LINUX"
    elif [ ! -z "`uname -s | grep 'BSD$'`" ]; then
        CFGOPERSYSTEM="BSD"
    fi

    #--- Directory for temporary files created by runiou. The parent directory
    #--- (for example /tmp) must be mounted without the attribute 'noexec'
    #--- for permit the execution of temporary scripts.
    CFGTEMPDIRECTORY="/tmp/runiou${UID}/temporal"

    #--- General.
    CFGBINARYDIR="/opt/ciscoiou/bin"
    CFGSCRIPTDIR="/opt/ciscoiou/scripts"
    CFGCAPTURESDIR="/tmp/runiou${UID}/captures"
    CFGNVRAMLEN=128
    CFGSCREENNAME="runiou"
    CFGCONFIGPREFIX="config"

    #--- IOU executables files and RAM memory in Mb.
    CFGIOSADVENT="i86bi-linux-l3-adventerprisek9-15.4.1T.bin"
    CFGRAMADVENT="256"

    CFGIOSIPBASE="i86bi-linux-l3-ipbase-12.4.bin"
    CFGRAMIPBASE="128"

    CFGIOSL2SW="i86bi-linux-l2-adventerprisek9-15.1a.bin"
    CFGRAML2SW="256"

    CFGIOSPAGENT="i86bi-linux-l3-tpgen-adventerprisek9-12.4.bin"
    CFGRAMPAGENT="128"

    #--- IOU accesories.
    CFGIOUKEYGEN="keygen.py"
    CFGIOUWRAPPER="wrapper.bin"
    CFGIOU2NET="iou2net.pl"
    CFGIOUSNIFF="iousniff"
    CFGNETMAPFILE="NETMAP"

    #--- Use 'su' or 'sudo' for root privileges.
    #--- This is used for iou2net with the interfaces and tap connections.
    #---    0: use sudo with the password of the current user. Typically used in Ubuntu.
    #---    1: use su with the password of root. Typically used in Debian.
    CFGROOTSUDOSU=1

    #--- Default screen params.
    #----- See https://www.gnu.org/software/screen/manual/screen.html
    #----- Colors: acronym - description - windows list - background.
    CFGSCREENCOLORS="WcWb"
    #----- Caption mode: splitonly / always.
    CFGSCREENMODE="splitonly"
    #----- Caption text.
    CFGSCREENTEXT="%t"

    #--- Path environment variable.
    PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
    if [ "${CFGOPERSYSTEM}" == "BSD" ]; then
        PATH="${PATH}:/usr/pkg/bin:/usr/pkg/sbin:/home/${USER}/bin"
    fi

    #--- Set the language to english. Please don't modify this. runiou parses
    #--- the output of commands in english language.
    LANG="C"

    #--- Encapsulation for capture files.
    #--- See the complete list in http://www.tcpdump.org/linktypes.html
    CFGPCAPENCAPSHORT=("null" "eth" "x25" "ieee" "arcnet" "slip" "ppp" "ppphdlc" "pppoe" "atm" "raw" "ciscoppp" "fr" "loop" "ltalk" "arcnet" "pppd" "lapd" "pppdir" "hdlcdir" "frdir" "netlink")
    CFGPCAPENCAPVALUE=(    0     1     3      6        7      8     9        50      51   100   101        104   107   108     114      129    166    177      204       205     206       253)
    CFGPCAPENCAPDESCR=("BSD loopback encapsulation" "Ethernet 10Mb, 100Mb, 1000Mb, and up" "AX.25 packet" "IEEE 802.5 Token Ring"
                       "ARCNET Data Packets" "Serial Line Internet Protocol" "Point-to-Point Protocol" "PPP in HDLC-like framing"
                       "Point-to-point protocol over Ethernet" "LLC/SNAP-encapsulated ATM" "Raw IP" "Cisco PPP with HDLC framing"
                       "Frame Relay" "OpenBSD loopback encapsulation" "Apple LocalTalk" "ARCNET Data Packets" "PPP in HDLC-like encapsulation"
                       "Link Access Procedures on the D Channel (LAPD) frames" "PPP, as per RFC 1661 and RFC 1662" "Cisco PPP with HDLC framing"
                       "Frame Relay 2" "Linux Netlink" )

    #--- Name of TAP and Bridge interfaces.
    CFGINTFEXTTAP="rioutapext"
    CFGINTFEXTBRG="rioubrgext"
    CFGINTFINTTAP="rioutapint"

    #--- Separator in TAP pre-* and post-* commands.
    CFGTAPSPRTR=";"

    #--- Initial startup config of the IOU devices.
    #---    %H: acronym.
    #---    %D: router id.
    read -r -d '' CFGTEMPLATESTART << EOFTMPL
    !
    hostname %H
    ip dhcp bootp ignore
    logging buffered 8192 informational
    logging on
    no cdp run
    no ip bootp server
    no ip domain-lookup
    no ip finger
    no ip http secure-server
    no ip http server
    no ip icmp rate-limit unreachable
    no service config
    no service dhcp
    no service pad
    service tcp-keepalives-in
    service tcp-keepalives-out
    service timestamps debug datetime localtime msec
    service timestamps log datetime localtime msec
    banner login $
    ---------------------
    DevName: %H
    DevID: %D
    ---------------------
    $
    line con 0
     exec-timeout 0 0
     logging synchronous
     no login
     privilege level 15
    line vty 0 4
     exec-timeout 15 0
     logging synchronous
     login
     privilege level 15
     transport input telnet
     password %H
    end
    !
EOFTMPL

    return
    }

#--- Return the netmap template.
function loc_template {
    #--- Return:
    #---  NETMAPTEMPLT: text with the netmap template.

    #--- Text of the template.
    read -r -d '' NETMAPTEMPLT << EOFCFG
        #########################################################################
        #
        # runiou - A bash script frontend for Cisco IOU.
        #
        #          Template of the netmap file and brief help.
        #
        #------------------------------------------------------------------------
        #
        # Full documentation:
        #
        #   https://bitbucket.org/mangelo/runiou/
        #
        #------------------------------------------------------------------------
        #
        # Interfaces:
        # ===========
        #               e0/0 .. e0/3  Ethernet first.
        #               e1/0 .. e1/3
        #               s2/0 .. s2/3
        #               s3/0 .. s3/3  Serial last.
        #
        #
        # Short help:
        # ===========
        #
        # #%    define an IOU router/switch device.
        # #/    define an external device: command for console.
        # #$    define an external connection in UDP mode.
        # #@    define an external connection with a physical interface.
        # #^    define an external connection with a TAP interface.
        # #&    define an external telnet IP for the IOU consoles.
        # #=    define a text visible on the caption of screen.
        # #+    define a separator in the list of windows in screen.
        # #<    define the initial startup configuration commands.
        #
        #
        # Long help:
        # ==========
        #
        # #% ----> define an IOU router/switch device.
        #
        #   Syntax:
        #       #%  ID     Acronym     IOS       Eth    Serial
        #
        #   Examples:
        #       #%  21     RTCUSTOM    advipsrv   2       1
        #       #%  22     SWMAIN      l2         4       0
        #
        #   Valid IOS: advipsrv
        #              ipbase
        #              l2
        #              pagent
        #
        #   The ID is a numeric decimal value between 1 and 1023.
        #   The acronym is a small name which identifies the device in the list of
        #   windows in screen.
        #   The eth and serial are the quantity of interfaces (not modules).
        #
        #
        # #/ ----> define an external device: command to the console.
        #
        #   Syntax:
        #       #/   ID     Acronym     Command     Optional_Args
        #
        #   Examples:
        #       #/  2134    REALSWITCH  telnet 192.168.0.99
        #       #/  98      REALLINUX   ssh -l user -v 2 192.168.0.98
        #       #/  7822    REALWINDOWS vncviewer -quality 1 -noshared server.example.com:2
        #
        #   This non IOU devices appears in the list command and in the list of
        #   windows in screen. The Command with Args are executed with the runiou
        #   telnet command inside a window of screen. This devices are not started
        #   nor stopped with the start/stop commands.
        #   The ID is a unique decimal numeric value which identifies the device.
        #   Unlike IOU, this number is not limited to 1023, but must be equal or
        #   greather than 1.
        #
        #
        # #$ ----> define an external connection in UDP mode.
        #
        #   Syntax:
        #       #$  ID     LocalPort   RemoteIP        RemotePort
        #
        #   Examples:
        #       #$  11       9000      192.168.0.15       9001
        #       #$  12       9002      glados             9003
        #       #$  13       9004      server.domain.com  9005
        #
        #   Connections syntax:
        #       <RouterID>:<slot>/<port>     <IouNetID>:0/0
        #
        #   An external UDP connection (also called UDP tunnel), is a connection
        #   between an IOU device and a virtual machine. This connection is made
        #   with the iou2net.pl script in UDP mode.
        #
        #   Syntax in VirtualBox:
        #       vboxmanage modifyvm VMNAME|UUID --nicN generic
        #         --nictypeN Am79C970A|Am79C973|82540EM|82543GC|82545EM|virtio
        #         --cableconnectedN on --nicpropertyN dest=<IP_of_machine_running_iou2net>
        #         --nicpropertyN dport=<LocalPort> --nicpropertyN sport=<RemotePort>
        #         --nicgenericdrvN UDPTunnel --macaddressN auto|HHHHHHHHHHHH
        #
        #   Syntax in qemu:
        #       -net nic,model=virtio|e1000,vlan=N,macaddr=HH:HH:HH:HH:HH:HH
        #        -net socket,vlan=N,udp=<IP_of_machine_running_iou2net>:<LocalPort>,localaddr=:<RemotePort>
        #
        #   Syntax in dynagen:
        #       <interface> = NIO_udp:<RemotePort>:<IP_of_machine_running_iou2net>:<LocalPort>
        #
        #
        # #@ ----> define an external connection to a physical interface.
        #
        #   Syntax:
        #       #@  ID  Interface
        #
        #   Examples:
        #       #@  76  eth0
        #       #@  77  eth2.35
        #
        #   Connections syntax:
        #       <RouterID>:<slot>/<port>     <IouNetID>:0/0
        #
        #   runiou will configure and bring to up the physical interface. If it is
        #   a subinterface (a vlan), runiou loads the 8021q module and creates the
        #   subinterface. When you stop the connection or the entire lab, runiou
        #   bring down and delete the (sub)interfaces only if they have no ip address
        #   configured. runiou will ask for the root or user password for start and
        #   stop these connections (need root privileges).
        #
        #
        # #^ ----> define an external connection to a TAP interface.
        #
        #   Syntax:
        #       #^  ID      pre-up     command [params...]
        #       #^  ID      post-up    command [params...]
        #       #^  ID      pre-down   command [params...]
        #       #^  ID      post-down  command [params...]
        #
        #   Examples:
        #       Creates the rioutapint0045 interface and execute some commands:
        #       #^   45     pre-up     /path/to/some/script
        #       #^   45     pre-up     iptables-save > /tmp/iptables-org.txt
        #       #^   45     post-up    ifconfig <TAP> 192.168.45.1 netmask 255.255.255.0
        #       #^   45     post-up    route add -net 172.16.45.0 netmask 255.255.255.0 gw 192.168.45.2
        #       #^   45     post-up    route add -net 10.200.20.0 netmask 255.255.255.0 gw 192.168.45.3
        #       #^   45     post-up    iptables -I INPUT 1 -s 10.200.20.4/32 -i <TAP> -j ACCEPT
        #       #^   45     post-up    iptables -t nat -A POSTROUTING -o <TAP> -j MASQUERADE
        #
        #       #^   29     post-up    brctl addbr wanbr0
        #       #^   29     post-up    brctl addif wanbr0 <TAP>
        #       #^   29     post-up    brctl stp wanbr0 off
        #       #^   29     post-up    ifconfig wanbr0 up
        #
        #       #^   37     post-up    echo "1" > /proc/sys/net/ipv4/ip_forward
        #       #^   37     post-up    iptables -A FORWARD -j ACCEPT
        #
        #   Connections syntax:
        #       <RouterID>:<slot>/<port>     <IouNetID>:0/0
        #
        #   pre-up: specifies the commands executed before the tap interface creation.
        #   post-up: specifies the commands executed after the tap interface creation.
        #   pre-down: specifies the commands executed before the tap interface deletion.
        #   post-down: specifies the commands executed after the tap interface deletion.
        #
        #   Every pre-* and post-* keywords can appear multiple times for every
        #   interface. These commands are executed in order with root privileges.
        #   The <TAP> keyword is replaced by the name of the tap interface. The
        #   name of the tap interface is composed with the "rioutapint" string and
        #   the zero padded ID with four digits (for example: rioutapint0003,
        #   rioutapint0329).
        #   At least one command is mandatory. If you not want to execute any
        #   command, specify the /bin/false command in any pre or post keyword.
        #   In external connections, the iou2net port are always 0/0.
        #   runiou will ask for the root or user password for start and stop these
        #   connections (need root privileges).
        #
        #   Integration:
        #       NETMAP:
        #           #^   ID     post-up    brctl addbr BRIDGENAME0
        #           #^   ID     post-up    brctl addif BRIDGENAME0 <TAP>
        #           #^   ID     post-up    brctl stp BRIDGENAME0 off
        #           #^   ID     post-up    ifconfig BRIDGENAME0 up
        #
        #       Docker:
        #           docker network create -d macvlan --gateway X.X.X.X --subnet X.X.X.X/NN -o parent=BRIDGENAME0 DKNETNAME0
        #           docker create ... -h HOSTNAME --ip X.X.X.X --name CONTNAME --network DKNETNAME0 --entrypoint COMMAND IMAGE ARGS
        #           docker network connect ... --ip X.X.X.X DKNETNAME0 CONTNAME
        #
        #       Qemu:
        #           su root -c 'tunctl -t TAPNAME0 -u USERNAME ; ifconfig TAPNAME0 up ; brctl addif BRIDGENAME0 TAPNAME0'
        #           qemu-system-x86_64 ... -net nic,model=virtio|e1000,vlan=N,macaddr=HH:HH:HH:HH:HH:HH -net tap,vlan=N,ifname=TAPNAME0,script=no,downscript=no
        #
        #       VirtualBox:
        #           vboxmanage modifyvm VMNAME|UUID --nicN bridged --nictypeN Am79C970A|Am79C973|82540EM|82543GC|82545EM|virtio
        #             --cableconnectedN on --bridgeadapterN BRIDGENAME0 --macaddressN auto|HHHHHHHHHHHH
        #
        #
        # #& ----> define an external telnet IP for IOU consoles.
        #
        #   Syntax:
        #       #&  <IP_ADDRESS>
        #
        #   Examples:
        #       #&  192.168.0.31
        #       #&  server.example.com
        #
        #   This is need when run the IOU lab in one machine and the telnet to the
        #   consoles (and screen) are executed in another machine. In this
        #   scenario, you need to copy the remote NETMAP file of the machine which
        #   run the IOU to the local machine which run the telnet with screen.
        #   With this, you can free some RAM memory for use by IOU. In the local
        #   machine, when you run 'runiou telnet', the telnet command are executed
        #   as 'telnet <IP_ADDRESS> <port>' instead 'telnet 127.0.0.1 <port>'.
        #
        #
        # #= ----> define a text visible on the caption of screen.
        #
        #   Case Syntax
        #   ---- ----------
        #   0    <none>
        #   1    #=
        #   2    #= A descriptive text with defaults colors.
        #   3    #= {WXYZ}
        #   4    #= {WXYZ} A descriptive text with custom colors.
        #   5    #= splitonly "TU" "V"
        #   6    #= always "TU" "V"
        #
        #   Examples:
        #       #=
        #       #= Foo Bar Lab 1
        #       #= {kKkg} Training to the company Foo Bar
        #       #= {RRRk} Foo Bar presentation
        #       #= splitonly "kw" "%{b kw}%H %{r} %{w}| %{g}%c %{w}| %{y}%d.%m.%Y %{w}| %{g}%l %{w}| %{-b kw}%u %-Lw%{= rW}%50> %n%f %t %{-}%+Lw%<"
        #       #= always "Ry" "%{= kc}%H (system load: %l)%-21=%{= .m}%D %d.%m.%Y %0c"
        #
        #   Case 0: The caption is hidden and appears when you split a window.
        #           The caption shows the acronym of the actual device at the
        #           left bottom whith the defaults colors.
        #
        #   Case 1: The caption is always visible. The caption shows the
        #           acronym of the actual device at the left bottom whith the
        #           defaults colors.
        #
        #   Case 2: The caption is always visible and shows the acronym of the
        #           actual device at the left bottom and the descriptive text
        #           at the right bottom with the defaults colors.
        #
        #   Case 3: The caption is always visible and shows the acronym of the
        #           actual device at the left bottom with the custom colors.
        #
        #   Case 4: The caption is always visible and shows the acronym of the
        #           actual device at the left bottom and the descriptive text
        #           at the right bottom with the custom colors.
        #
        #   Case 5: The caption is hidden and appears when you split a window.
        #           The cursor in the window list appears with the "TU" colors
        #           and the caption text is the "V" text. "V" can be any string
        #           escapes supported by screen.
        #
        #   Case 6: The caption is always visible. The cursor in the window
        #           list appears with the "TU" colors and the caption text is
        #           the "V" text. "V" can be any string escapes supported by
        #           screen.
        #
        #   Colors for (3) & (4):
        #       W: color of the acronym.
        #       X: color of the descriptive text.
        #       Y: color of the text in the cursor of the windows list.
        #       Z: color of the background.
        #
        #   Colors for (5) & (6):
        #       T: color of the text in the cursor of the windows list.
        #       U: color of the background in the cursor of the windows list.
        #
        #   See https://www.gnu.org/software/screen/manual/screen.html#String-Escapes
        #   for the color codes.
        #
        #   Caption text for (5) & (6):
        #       V: any text for the caption window with string escapes support.
        #          See https://www.gnu.org/software/screen/manual/screen.html#String-Escapes
        #          for the complete list of strings and colors.
        #
        #   If this option is missing, the caption bar is hidden. If you want
        #   the acronym be visible without the optional text, simply put
        #   the #= with an empty text.
        #
        #
        # #+ ----> define a separator in the list of windows in screen.
        #
        #   Syntax:
        #       #+ ID separator_text
        #
        #   Examples:
        #       #+  123     ---- Backbone routers ----
        #       #+  9233    ****[ Branch switchs ]****
        #       #+  89      % Real devices.
        #
        #   The first non blank character is used to pad the text to obtain the
        #   same width in all separators.
        #
        #   The separator text appears at the right of the acronym in the
        #   list of windows in screen.
        #
        #
        # #< ----> define the initial startup configuration of IOU devices.
        #
        #   Syntax:
        #       #<  global
        #       #<  command
        #
        #   Examples:
        #       #<  global
        #       #<  hostname %H-%D
        #       #<  banner motd $
        #       #<  Welcome to %H [%D]
        #       #<  $
        #
        #   The initial startup config is the configuration pushed to the nvram
        #   file of every IOU device when boots the first time and the
        #   configuration is empty and the nvram file not exist. This is util to
        #   preconfigure all IOU devices with a configuration template. The
        #   initial configuration is applied to all IOU devices.
        #   You must define only one command by line.
        #   The 'command' can be any valid command configuration in IOS.
        #   The 'global' command is replaced by the CFGTEMPLATESTART constant (a
        #   template) defined in the scrconfigure function.
        #   You not need to enter the 'configure terminal' or the 'end' command.
        #   The commands are parsed from top to down, inserting the global
        #   template when the 'global' command appears. The 'global' command can
        #   appears multiple times.
        #   The %H keyword is replaced by the acronym of the device.
        #   The %D keyword is replaced by de device id.
        #   If the len of the configuration is greather than 30Kb, the
        #   configuration is compressed and the command 'service compress-config'
        #   is inserted.
        #
        # -------> connections between IOU devices.
        #
        #   Syntax:
        #       <DevID1>:<slot1>/<port1>   <DevID2>:<slot2>/<port2>
        #
        #   Example:
        #       654:1/3     456:7/2
        #        83:2/1      12:6/1     34:7/1      90:0/0
        #
        #   The syntax of the connections between devices are the standard in IOU.
        #   The connections do not have a '#' preceding it.
        #   You can not capture traffic in a multipoint connection.
        #
        #########################################################################
        #
        # Example of a NETMAP
        # ====================
        #
        # -- Routers and switchs:
        #         ID     Acronym     IOS       Eth    Serial
        #%        20     RTCORE      advipsrv   1       2
        #%        21     RTCUSTOM    ipbase     2       1
        #%        22     SWMAIN      l2         4       0
        #%        23     RTGEN       pagent     1       1
        #
        #
        # -- External connections in UDP mode:
        #         ID     LocalPort   RemoteIP        RemotePort
        #$        11       9000      192.168.0.15       9001
        #$        12       9002      glados             9003
        #$        13       9004      server.domain.com  9005
        #
        #
        # -- External connections in interface mode:
        #         ID     Interface
        #@        14     eth0.786
        #
        #
        # -- External connection in TAP mode:
        #         ID     When       Command & Arguments
        #^        87     post-up    ifconfig <TAP> 10.9.9.9 netmask 255.255.255.0 up
        #^        87     post-up    route add -net 172.16.0.0 netmask 255.255.255.0 gw 10.9.9.1
        #
        # -- External console IP:
        #      IP_ADDRESS
        #&    192.168.0.32
        #
        #
        # -- Optional text in screen caption:
        #      Text
        #= A wonderful lab
        #
        #
        # -- External devices (consoles):
        #    ID     Acronym     Command     Optional_Args
        #/  9987    REALSWITCH  telnet 192.168.0.99
        #/  1234    REALUNIX    ssh 192.168.0.98
        #
        #
        # -- Separators:
        #       ID      Separator text
        #+      20      --- Routers ---
        #+      22      *** Switchs ***
        #
        #
        # -- Initial configuration:
        #   Command
        #<  global
        #<  interface Ethernet0/0
        #<  description LAN INTERFACE - %H
        #
        #
        # -- IOU connections:
        20:2/0 21:2/1
        20:0/1 21:0/0
        20:0/2 22:0/1
        20:0/3 21:0/1 22:0/0 23:0/0
        22:0/2 11:0/0
        23:0/1 12:0/0
        13:0/0 23:0/2
        14:0/0 22:0/3
        20:0/0 87:0/0
        #
        #########################################################################
EOFCFG

    #--- Remove the initial spaces.
    NETMAPTEMPLT=`echo "${NETMAPTEMPLT}" | sed -e 's/^ *//'`

    return
    }

#--- Check for one command.
function checkonecmd {
    #--- Params: $1: command to check.
    if [ -z "`which ${1}`" ]; then
        echo "ERROR: command ${1} not found."
        echo "SOLUTION: install it."
        exit 10
    fi

    return
    }

#--- Check for some commands and apps.
function checkapps {
    local ZAPPSLST
    local ZAPPTEST

    ZAPPSLST=( "basename" "bash" "cat" "chmod" "chown" "compress" "cp" "cut"
               "date"  "dd" "dirname" "find" "getent" "grep" "hostname"
               "ifconfig" "kill" "ls" "mkdir" "mktemp" "modprobe" "mv" "od"
               "perl" "printf" "ps" "pwd" "python" "rm"  "screen" "sed"
               "sleep" "stat" "tail" "tcpdump" "telnet" "touch" "tr" "uname"
               "uncompress" "wc" "which" "xxd" )
    for ZAPPTEST in ${ZAPPSLST[@]}; do
        checkonecmd "${ZAPPTEST}"
    done

    if [ ${CFGROOTSUDOSU} -eq 0 ]; then
        checkonecmd "sudo"
    else
        checkonecmd "su"
    fi
    return
    }

#--- Check for some specific platform commands.
function checkplatform {
    if [ "${CFGOPERSYSTEM}" == "BSD" ]; then
        #--- NetBSD, FreeBSD, OpenBSD.
        checkonecmd "gethost"
    else
        #--- Other OS (Linux).
        checkonecmd "brctl"
        checkonecmd "gethostip"
        checkonecmd "tunctl"
        checkonecmd "vconfig"
    fi
    return
    }

#--- Check for one IOU executable.
function checkoneiou {
    #--- Params: $1: IOU file to check.
    if [ ! -x "${1}" ]; then
        echo "ERROR: IOU executable not found: ${1}"
        echo "SOLUTION: check the CFGBINARYDIR or CFGSCRIPTDIR variable."
        exit 11
    fi
    return
    }

#--- Check for all IOU executables.
function checkalliou {
    checkoneiou "${CFGBINARYDIR}/${CFGIOSADVENT}"
    checkoneiou "${CFGBINARYDIR}/${CFGIOSIPBASE}"
    checkoneiou "${CFGBINARYDIR}/${CFGIOSL2SW}"
    checkoneiou "${CFGBINARYDIR}/${CFGIOSPAGENT}"
    checkoneiou "${CFGSCRIPTDIR}/${CFGIOUKEYGEN}"
    checkoneiou "${CFGSCRIPTDIR}/${CFGIOUWRAPPER}"
    checkoneiou "${CFGSCRIPTDIR}/${CFGIOU2NET}"
    checkoneiou "${CFGSCRIPTDIR}/${CFGIOUSNIFF}"

    return
    }

#--- Check the existence of libcrypto.so.4.
function checklibcrypto {
    local ZCRYPTO

    if [ ! -e "/usr/lib/libcrypto.so.4" ] && [ ! -h "/usr/lib/libcrypto.so.4" ]; then
        echo "ERROR: /usr/lib/libcrypto.so.4 not found."
        ZCRYPTO=`ls -1 /lib/libcrypto.so.* 2> /dev/null | head -n 1`
        if [ -z "${ZCRYPTO}" ]; then
            ZCRYPTO=`ls -1 /lib/i386-linux-gnu/libcrypto.so.* 2> /dev/null | head -n 1`
            if [ -z "${ZCRYPTO}" ]; then
                ZCRYPTO="/path/to/libcrypto.so.x.x"
            fi
        fi
        echo "SOLUTION: As root, make 'ln -s ${ZCRYPTO} /usr/lib/libcrypto.so.4'"
        exit 12
    fi

    return
    }

#--- Check for the patched /etc/hosts file.
function checkhosts {
    local ZMYIPADDR
    local ZTAB

    ZTAB=`printf "[ \t]"`
    if [ -z "`getent hosts | grep -E \"(127.0.0.1|192.168.0.1)${ZTAB}${ZTAB}*xml.cisco.com\"`" ]; then
        echo "ERROR: /etc/hosts file is not patched."
        echo "SOLUTION: As root, append '127.0.0.1 xml.cisco.com' to the /etc/hosts file."
        exit 13
    fi

    ZMYIPADDR=`getlocalip`
    if [ -z "`ifconfig -a | grep -E 'inet |inet6 ' | grep -v '127.0.0.1' | grep -v '::1' | grep \"${ZMYIPADDR}\"`" ]; then
        echo "ERROR: /etc/hosts file is not patched."
        echo "SOLUTION: As root, modify the /etc/hosts file with the"
        echo "SOLUTION: host `hostname -s` with any of these ip address:"
        ifconfig -a | grep -E 'inet |inet6 ' | grep -v '127.0.0.1' | grep -v '::1' | cleanline | sed -e "s/^/SOLUTION: /"
        exit 14
    fi
    return
    }

#--- Check for the netmap file.
function checknetmap {
    if [ ! -e "${CFGNETMAPFILE}"  ] && [ ! -h "${CFGNETMAPFILE}" ]; then
        echo "ERROR: ${CFGNETMAPFILE} file not found."
        echo "SOLUTION: create a ${CFGNETMAPFILE} file."
        exit 15
    fi

    return
    }

#--- Check for the license iourc file.
function checklicense {
    if [ ! -e ~/".iourc" ]; then
        #--- Executes the keygen.
        python "${CFGSCRIPTDIR}/${CFGIOUKEYGEN}" | grep -A 1 "^\[license\]$" > ~/".iourc"
    fi

    return
    }

#--- Other checks.
function checkothers {
    local ZOWNER
    local ZRESULT
    local ZTMPFILE

    #--- Make the temporary directory.
    mkdir -p "${CFGTEMPDIRECTORY}" > /dev/null 2> /dev/null
    chmod 700 "${CFGTEMPDIRECTORY}" > /dev/null 2> /dev/null

    #--- Check the temp directory 1.
    if [ ! -d "${CFGTEMPDIRECTORY}" ]; then
        echo "ERROR: the parent of temporary dir `dirname ${CFGTEMPDIRECTORY}` is not writable."
        echo "SOLUTION: make a chmod to it or mount in rw mode."
        exit 16
    fi

    #--- Check the temp directory 2.
    ZTMPFILE="`mktemp \"${CFGTEMPDIRECTORY}/checkrwx-XXXXXXXX\" 2> /dev/null`"
    if [ ! -f "${ZTMPFILE}" ]; then
        echo "ERROR: the temporary dir ${CFGTEMPDIRECTORY} is not writable."
        echo "SOLUTION: make a chmod to it or mount in rw mode."
        exit 17
    fi
    echo "#!/usr/bin/env bash" > "${ZTMPFILE}"
    echo "echo \"SuCcEsS\"" >> "${ZTMPFILE}"
    chmod 700 "${ZTMPFILE}"
    ZRESULT=`"${ZTMPFILE}" 2> /dev/null`
    rm -f "${ZTMPFILE}"
    if [ "${ZRESULT}" != "SuCcEsS" ]; then
        echo "ERROR: in the temporary dir ${CFGTEMPDIRECTORY} can not execute scripts."
        echo "SOLUTION: change the temp dir in the config or mount it without noexec."
        exit 18
    fi

    #--- Check the language.
    if [ -z "`ifconfig -a | grep -E 'packets|dropped'`" ]; then
        echo "ERROR: the local language is not english (${LANG})."
        echo "SOLUTION: check the LANG variable in the configuration."
        exit 19
    fi

    #--- Check /tmp/netio0.
    if [ -d "/tmp/netio0" ] || [ -h "/tmp/netio0" ]; then
        if [ "${CFGOPERSYSTEM}" == "BSD" ]; then
            ZOWNER=`stat -f "%u" "/tmp/netio0"`
        else
            ZOWNER=`stat -c "%u" "/tmp/netio0"`
        fi
        if [ ${ZOWNER} -eq ${UID} ]; then
            chmod -R 700 "/tmp/netio0" > /dev/null 2> /dev/null
            rm -fr "/tmp/netio0" > /dev/null 2> /dev/null
        else
            echo "ERROR: the /tmp/netio0 is owned by UID ${ZOWNER}."
            echo "SOLUTION: as UID ${ZOWNER} delete it if is not used."
            exit 20
        fi
    fi

    return
    }

#--- Check for all necessary to run this script.
function checkallscr {
    #--- Params: $1: command.
    scrconfigure
    checkapps
    checkplatform
    checkothers

    case "${1}" in
        "start"|"rtstart")
            checklibcrypto
            checkhosts
            checkalliou
            checklicense
            checknetmap
            ;;
        "cnstart"|"captureon")
            checkalliou
            checkhosts
            checknetmap
            ;;
        "netmap"|"template"|"capturelist"|"capturehelp"|"version"|"help"|"--help"|"")
            ;;
        *)
            checknetmap
            ;;
    esac

    return
    }

#--- Check for an empty string.
function checkempty {
    #--- Params: $1: name of the string.
    #---         $2: string to check.
    #---         $3: original text line.
    ZFNCEMPTYNAME="${1}"
    ZFNCEMPTYSTR="${2}"
    ZFNCEMPTYORG="${3}"

    if [ -z "${ZFNCEMPTYSTR}" ]; then
        echo "ERROR: empty ${ZFNCEMPTYNAME} in line '${ZFNCEMPTYORG}'"
        exit 21
    fi
    return
    }

#--- Validate a number.
function validatenum {
    #--- Params:
    #---    $1: number to validate.
    #---    $2: min value, or -1.
    #---    $3: max value, or -1.
    #---    $4: text line for error.
    local ZFNCVALERR
    local ZFNCVALMAX
    local ZFNCVALMIN
    local ZFNCVALNUM

    #--- Params.
    ZFNCVALNUM=${1}
    ZFNCVALMIN=${2}
    ZFNCVALMAX=${3}
    ZFNCVALERR="${4}"

    #--- Check if all chars are decimal digits.
    if [ -z `echo "${ZFNCVALNUM}" | grep "^[0-9]\+$"` ]; then
        echo "ERROR: ${ZFNCVALERR}."
        exit 22
    fi

    #--- Check for leanding zeros.
    if [ ! -z `echo "${ZFNCVALNUM}" | grep "^0"` ] && [ "${ZFNCVALNUM}" != "0" ]; then
        echo "ERROR: ${ZFNCVALERR}."
        exit 23
    fi

    #--- Check min limit.
    if [ ${ZFNCVALMIN} -ne -1 ] && [ ${ZFNCVALNUM} -lt ${ZFNCVALMIN} ]; then
        echo "ERROR: ${ZFNCVALERR}."
        exit 24
    fi

    #--- Check max limit.
    if [ ${ZFNCVALMAX} -ne -1 ] && [ ${ZFNCVALNUM} -gt ${ZFNCVALMAX} ]; then
        echo "ERROR: ${ZFNCVALERR}."
        exit 25
    fi

    return
    }

#--- Make a sleep.
function make_sleep {
    #--- Params: $1: delay in seconds.
    local ZFNCDELAY

    ZFNCDELAY=${1}
    echo -n "wait ..."
    while [ ${ZFNCDELAY} -gt 0 ]; do
        sleep 2
        echo -n "."
        ZFNCDELAY=$(( ${ZFNCDELAY} - 2 ))
    done
    echo ""
    return
    }

#--- Get the local ip address.
function getlocalip {
    local ZFNCLOCIP

    ZFNCLOCIP=`hostname -s`

    if [ "${CFGOPERSYSTEM}" == "BSD" ]; then
        ZFNCLOCIP=`gethost "${ZFNCLOCIP}" 2> /dev/null | grep "Address:" | cut -d ' ' -f 2`
    else
        ZFNCLOCIP=`gethostip -d "${ZFNCLOCIP}" 2> /dev/null`
    fi

    if [ -z "${ZFNCLOCIP}" ]; then
        ZFNCLOCIP="127.0.0.1"
    fi

    echo "${ZFNCLOCIP}"

    return
    }

#--- Clean the line.
function cleanline {
    #--- Params: $1 or stdin: line of text to clean.
    local ZCLNTAB
    local ZFNCCLNLIN

    if [ ! -z "${1}" ]; then
        ZFNCCLNLIN="${1}"
    else
        ZFNCCLNLIN=`cat`
    fi
    ZCLNTAB=`printf "[ \t]"`
    echo "${ZFNCCLNLIN}" | tr -d '\r' | sed -e "s/${ZCLNTAB}\+/ /g" -e 's/^ \+//' -e 's/ \+$//g'
    return
    }

#--- Read an IOU nvram file and export the startup config to a file.
function readnvram {
    # Params:
    #   $1: nvram file (input).
    #   $2: text file, startup config (output).
    #
    # Ported from iou_export.py python script.
    #   iou_export.py is part of GNS3.
    #   iou_export.py Copyright Bernhard Ehlers
    #   GNS3 Copyright (C) GNS3 Technologies Inc.
    #   See https://github.com/GNS3/
    #
    local ZCONFIG
    local ZDECOMP
    local ZFORMAT
    local ZLENGTH
    local ZNVRAMFILE
    local ZNVRAMLEN
    local ZOFFSET
    local ZTEXTFILE

    #--- Params.
    ZNVRAMFILE="$1"
    ZTEXTFILE="$2"

    #--- Constants.
    ZOFFSET=36

    #--- Check if the nvram file is readable.
    if [ ! -f "${ZNVRAMFILE}" ]; then
        return
    fi

    #--- Check the nvram size.
    ZNVRAMLEN=`wc -c < "${ZNVRAMFILE}"`
    if [ ${ZNVRAMLEN} -lt ${ZOFFSET} ]; then
        return
    fi

    #--- Check the magic number.
    if [ "`xxd -p -l 2 -s 0 \"${ZNVRAMFILE}\"`" != "abcd" ]; then
        return
    fi

    #--- Format and length.
    ZFORMAT=$((16#`xxd -p -l 2 -s 2 "${ZNVRAMFILE}"`))
    ZLENGTH=$((16#`xxd -p -l 4 -s 16 "${ZNVRAMFILE}"`))

    #--- Check the length.
    if [ ${ZNVRAMLEN} -lt $((${ZOFFSET} + ${ZLENGTH})) ]; then
        return
    fi

    #--- Is compressed?.
    if [ ${ZFORMAT} -eq 2 ]; then
        ZDECOMP="uncompress -q -c"
    else
        ZDECOMP="cat"
    fi

    #--- Get the config.
    ZCONFIG=`xxd -s ${ZOFFSET} -l ${ZLENGTH} "${ZNVRAMFILE}" | xxd -r -s -${ZOFFSET} | ${ZDECOMP}`

    #--- Remove empty lines. Write the config.
    echo "${ZCONFIG}" | sed -e 's/[ \t]*$//' | sed '/^$/d' > "$ZTEXTFILE"

    #--- Finish.
    return
    }

#--- Create an IOU nvram file with a startup config.
function writenvram {
    # Params:
    #   $1: nvram file (output).
    #   $2: text (no file), startup config (input).
    #   $3: show errors: 1:yes. 0:no.
    #
    # Ported from iou_import.py python script.
    #   iou_import.py is part of GNS3.
    #   iou_import.py Copyright (C) Bernhard Ehlers
    #   GNS3 Copyright (C) GNS3 Technologies Inc.
    #   See https://github.com/GNS3/
    #
    local ZBASEADDR
    local ZCHKSUM
    local ZCOMPRESS
    local ZFILENAME
    local ZLCONFIG
    local ZMAXCFGLEN
    local ZNEWL
    local ZNVRINT
    local ZNVRSIZE
    local ZNWLNCFG
    local ZOFFSET
    local ZPADD
    local ZSHOWERR
    local ZTCONFIG
    local ZTEMPLATE
    local ZUNKNOWNA
    local ZUNKNOWNB
    local ZWORD

    #--- Params.
    ZFILENAME="$1"
    ZTEMPLATE="$2"
    ZSHOWERR="$3"

    #--- Avoid overwrite an existing nvram file.
    if [ -f "${ZFILENAME}" ]; then
        if [ ${ZSHOWERR} -ne 0 ]; then
            echo "ERROR: the file ${ZFILENAME} already exist."
            echo "SOLUTION: manually delete the nvram file."
            exit 110
        fi
        return
    fi

    #--- Constants.
    ZNVRSIZE=$(( ${CFGNVRAMLEN} * 1024 ))
    ZBASEADDR=$(( 256 * 1024 * 1024 ))
    ZOFFSET=36
    ZMAXCFGLEN=$(( 62 * 1024 ))

    #--- Get the template.
    ZTCONFIG=$'\n'"${ZTEMPLATE}"$'\n'
    ZLCONFIG=${#ZTCONFIG}

    #--- Pad the length of the config to four bytes.
    ZNEWL=$'\n'$'\n'$'\n'$'\n'
    ZPADD=$(( 4 - ( ${ZLCONFIG} % 4 ) ))
    ZTCONFIG="${ZTCONFIG}${ZNEWL:0:${ZPADD}}"
    ZLCONFIG=${#ZTCONFIG}

    #--- If length of the config is greater than $ZMAXCFGLEN,
    #--- then compress the config.
    if [ ${ZLCONFIG} -gt ${ZMAXCFGLEN} ]; then
        ZTCONFIG=$'\n'"service compress-config${ZTCONFIG}"
        ZLCONFIG=${#ZTCONFIG}
        ZTCONFIG=`echo -n "${ZTCONFIG}" | compress -b 12 -c | xxd`
        ZCOMPRESS=2
        ZLCONFIG=${ZLCONFIG}
        ZUNKNOWNA=1
        ZUNKNOWNB=65536
    else
        ZTCONFIG=`echo -n "${ZTCONFIG}" | xxd`
        ZCOMPRESS=1
        ZLCONFIG=0
        ZUNKNOWNA=0
        ZUNKNOWNB=0
    fi
    ZNWLNCFG=`echo "${ZTCONFIG}" | xxd -r | wc -c`

    #--- If the length of the config is greater than $ZMAXCFGLEN,
    #--- then cancel the creation of the nvram file.
    if [ ${ZNWLNCFG} -gt ${ZMAXCFGLEN} ]; then
        if [ ${ZSHOWERR} -ne 0 ]; then
            echo "ERROR: the configuration file is too big."
            echo "SOLUTION: reduce the configuration size."
            exit 111
        fi
        return
    fi

    #--- Create an empty nvram file.
    dd if=/dev/zero of="${ZFILENAME}" bs=${ZNVRSIZE} count=1 2> /dev/null

    #--- Fill the nvram file.
    echo "0:abcd" | xxd -r -s 0 - "${ZFILENAME}"                                                #- Startup config: magic number.
    echo "0:`printf \"%04X\" ${ZCOMPRESS}`" | xxd -r -s 2 - "${ZFILENAME}"                      #- Startup config: compress flag.
    echo "0:0000" | xxd -r -s 4 - "${ZFILENAME}"                                                #- Checksum (0 for now).
    echo "0:0f04" | xxd -r -s 6 - "${ZFILENAME}"                                                #- Startup config: IOS version: 15.4.
    echo "0:`printf \"%08X\" $(( ${ZBASEADDR} + ${ZOFFSET} ))`" | xxd -r -s 8 - "${ZFILENAME}"  #- Startup config: start address of config.
    echo "0:`printf \"%08X\" $(( ${ZBASEADDR} + ${ZOFFSET} + ${ZNWLNCFG} ))`" | xxd -r -s 12 - "${ZFILENAME}"   #- Startup config: end address of config.
    echo "0:`printf \"%08X\" ${ZNWLNCFG}`" | xxd -r -s 16 - "${ZFILENAME}"                      #- Startup config: len of compressed config.
    echo "0:`printf \"%08X\" ${ZUNKNOWNA}`" | xxd -r -s 24 - "${ZFILENAME}"                     #- Startup config: unknown data.
    echo "0:`printf \"%08X\" ${ZUNKNOWNB}`" | xxd -r -s 28 - "${ZFILENAME}"                     #- Startup config: unknown data.
    echo "0:`printf \"%08X\" ${ZLCONFIG}`" | xxd -r -s 32 - "${ZFILENAME}"                      #- Startup config: len of uncompressed config.
    echo "${ZTCONFIG}" | xxd -r -s ${ZOFFSET} - "${ZFILENAME}"                                  #- Startup config: config.
    echo "0:fedc" | xxd -r -s "$((${ZOFFSET} + ${ZNWLNCFG} + 0))" - "${ZFILENAME}"              #- Private config: magic number.
    echo "0:0001" | xxd -r -s "$((${ZOFFSET} + ${ZNWLNCFG} + 2))" - "${ZFILENAME}"              #- Private config: raw data.
    echo "0:`printf \"%08X\" $((${ZBASEADDR} + ${ZOFFSET} + ${ZNWLNCFG} + 16))`" | xxd -r -s "$((${ZOFFSET} + ${ZNWLNCFG} + 4))" - "${ZFILENAME}"    #- Private config: start address of config.
    echo "0:`printf \"%08X\" $((${ZBASEADDR} + ${ZOFFSET} + ${ZNWLNCFG} + 16))`" | xxd -r -s "$((${ZOFFSET} + ${ZNWLNCFG} + 8))" - "${ZFILENAME}"    #- Private config: end address of config.
    echo "0:00000000" | xxd -r -s "$((${ZOFFSET} + ${ZNWLNCFG} + 12 ))" - "${ZFILENAME}"        #- Private config: len address of config.

    #--- Checksum.
    ZNVRINT=`od -A n -d --endian=big -v "${ZFILENAME}"`
    ZCHKSUM=0
    for ZWORD in ${ZNVRINT} ; do
        ZCHKSUM=$(( ${ZCHKSUM} + ${ZWORD} ))
    done
    while [ $(( ${ZCHKSUM} / 65536 )) -gt 0 ]; do
        ZCHKSUM=$(( (${ZCHKSUM} % 65536) + (${ZCHKSUM} / 65536) ))
    done
    ZCHKSUM=$(( ${ZCHKSUM} ^ 65535 ))
    echo "0:`printf "%04X" $ZCHKSUM`" | xxd -r -s 4 - "${ZFILENAME}"

    #--- Finish.
    if [ ${ZSHOWERR} -ne 0 ]; then
        echo "configuration imported ok."
    fi
    return
    }

#--- Parse the netmap file for all devices.
function parsealldevices {
    #--- Return arrays:
    #---    NMAPALLDEVSIDX: device id.
    local ZDEVID
    local ZLISTXT
    local ZNUMLIN
    local ZTXTLINE

    #--- Do not refill the array.
    if [ ${#NMAPALLDEVSIDX[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the array.
    unset NMAPALLDEVSIDX

    #--- Parse the netmap file.
    ZNUMLIN=1
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -E '^#%|^#\/' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZDEVID=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 2`

        #--- Check the device id.
        validatenum ${ZDEVID} 1 -1 "invalid device id in line '${ZTXTLINE}'"

        #--- Fill the array.
        NMAPALLDEVSIDX[${ZNUMLIN}]=${ZDEVID}

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done

    return
    }

#--- Parse the netmap file for separators.
function parseseparat {
    #--- Return arrays:
    #---    NMAPSEPRTIDX: device id.
    #---    NMAPSEPRTTXT: text of separator.
    #---    NMAPSEPRTCHR: first char of separator.
    #--- Return var:
    #---    NMAPSEPRTLNG: length of the widest separator.
    local ZLISTXT
    local ZNUMLIN
    local ZROUTERCHR
    local ZROUTERIDX
    local ZROUTERSEP
    local ZTXTLINE

    #--- Do not refill the arrays.
    if [ ${#NMAPSEPRTIDX[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPSEPRTIDX
    unset NMAPSEPRTTXT
    unset NMAPSEPRTCHR

    #--- Parse the netmap file.
    NMAPSEPRTLNG=0
    ZNUMLIN=1
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#+' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZROUTERIDX=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 2`
        ZROUTERSEP=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 3-`

        #--- Check if empty.
        checkempty "deviceid" "${ZROUTERIDX}" "${ZTXTLINE}"

        #--- Device id.
        validatenum ${ZROUTERIDX} 1 -1 "invalid device id in line '${ZTXTLINE}'"

        #--- Separator.
        if [ -z "${ZROUTERSEP}" ]; then
            ZROUTERSEP="----------"
        fi

        #--- First char of the separator.
        ZROUTERCHR="${ZROUTERSEP:0:1}"

        #--- Widest separator.
        if [ ${#ZROUTERSEP} -gt ${NMAPSEPRTLNG} ]; then
            NMAPSEPRTLNG=${#ZROUTERSEP}
        fi

        #--- Fill the array.
        NMAPSEPRTIDX[${ZROUTERIDX}]="${ZROUTERIDX}"
        NMAPSEPRTTXT[${ZROUTERIDX}]="${ZROUTERSEP}"
        NMAPSEPRTCHR[${ZROUTERIDX}]="${ZROUTERCHR}"

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done
    return
    }

#--- Parse the netmap file for IOU devices.
function parseiourtsw {
    #---  Return arrays:
    #---    NMAPIOURTIDX: device id.
    #---    NMAPIOURTACR: acronym.
    #---    NMAPIOURTIOS: IOS executable.
    #---    NMAPIOURTRAM: RAM memory.
    #---    NMAPIOURTETH: quantity of ethernet slots.
    #--     NMAPIOURTSER: quantity of serial slots.
    #---    NMAPIOURTRUN: 0:is stopped - <> 0: pid (device).
    #---    NMAPIOURTWRP: 0:is stopped - <> 0: pid (wrapper).
    #---    NMAPIOURTPRT: console port.
    #--- Return var:
    #---    NMAPIOURTLNG: length of the widest acronym.
    local ZLISTXT
    local ZMATCH
    local ZNUMLIN
    local ZPROCCESS
    local ZRAMMEMEXE
    local ZROUTERACR
    local ZROUTERETH
    local ZROUTEREXE
    local ZROUTERIDX
    local ZROUTERIOS
    local ZROUTERPORT
    local ZROUTERRUNN
    local ZROUTERSER
    local ZTOOMANYPM
    local ZTXTLINE
    local ZWIDESTACR
    local ZWRAPPERRUNN

    #--- Force to clear the arrays.
    if [ "$1" == "FORCE" ]; then
        unset NMAPIOURTIDX
    fi

    #--- Do not refill the arrays.
    if [ ${#NMAPIOURTIDX[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPIOURTIDX
    unset NMAPIOURTACR
    unset NMAPIOURTIOS
    unset NMAPIOURTRAM
    unset NMAPIOURTETH
    unset NMAPIOURTSER
    unset NMAPIOURTRUN
    unset NMAPIOURTWRP
    unset NMAPIOURTPRT

    #--- Parse the netmap file.
    ZWIDESTACR=0
    ZNUMLIN=1
    ZPROCCESS=`ps ax -w -w | cleanline`
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#%' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZROUTERIDX=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 2`
        ZROUTERACR=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 3`
        ZROUTERIOS=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 4`
        ZROUTERETH=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 5`
        ZROUTERSER=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 6`
        ZTOOMANYPM=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 7-`

        #--- Check for too many parameters.
        if [ ! -z "${ZTOOMANYPM}" ]; then
            echo "ERROR: too many parameters in line '${ZTXTLINE}'."
            exit 26
        fi

        #--- Check if empty.
        checkempty "deviceid"       "${ZROUTERIDX}" "${ZTXTLINE}"
        checkempty "acronym"        "${ZROUTERACR}" "${ZTXTLINE}"
        checkempty "ios"            "${ZROUTERIOS}" "${ZTXTLINE}"
        checkempty "numethernets"   "${ZROUTERETH}" "${ZTXTLINE}"
        checkempty "numserials"     "${ZROUTERSER}" "${ZTXTLINE}"

        #--- Check the device id.
        validatenum ${ZROUTERIDX} 1 1023 "invalid device id in line '${ZTXTLINE}'"

        #--- Check the device console port.
        ZROUTERPORT=$(( 2000 + ${ZROUTERIDX} ))
        validatenum ${ZROUTERPORT} 2000 65535 "invalid device id in line '${ZTXTLINE}'"

        #--- Calc the widest acronym.
        if [ ${#ZROUTERACR} -gt ${ZWIDESTACR} ]; then
            ZWIDESTACR=${#ZROUTERACR}
        fi

        #--- IOS of the device.
        case "${ZROUTERIOS}" in
            "advipsrv")
                        ZROUTEREXE="${CFGBINARYDIR}/${CFGIOSADVENT}"
                        ZRAMMEMEXE="${CFGRAMADVENT}"
                        ;;
            "ipbase")
                        ZROUTEREXE="${CFGBINARYDIR}/${CFGIOSIPBASE}"
                        ZRAMMEMEXE="${CFGRAMIPBASE}"
                        ;;
            "l2")
                        ZROUTEREXE="${CFGBINARYDIR}/${CFGIOSL2SW}"
                        ZRAMMEMEXE="${CFGRAML2SW}"
                        ;;
            "pagent")
                        ZROUTEREXE="${CFGBINARYDIR}/${CFGIOSPAGENT}"
                        ZRAMMEMEXE="${CFGRAMPAGENT}"
                        ;;
            *)
                        echo "ERROR: invalid IOS in line '${ZTXTLINE}'."
                        exit 27
                        ;;
        esac

        #--- Ethernet ports.
        validatenum ${ZROUTERETH} 0 64 "invalid ethernet interfaces in line '${ZTXTLINE}'"
        if [ ${ZROUTERETH} -gt 0 ]; then
            ZROUTERETH=$(( ( ( ${ZROUTERETH} - 1 ) / 4 ) + 1 ))
        fi

        #--- Serial ports.
        validatenum ${ZROUTERSER} 0 64 "invalid serial interfaces in line '${ZTXTLINE}'"
        if [ ${ZROUTERSER} -gt 0 ]; then
            ZROUTERSER=$(( ( ( ${ZROUTERSER} - 1 ) / 4 ) + 1 ))
        fi

        #--- Check the total modules.
        if [ $(( ${ZROUTERETH} + ${ZROUTERSER} )) -gt 16 ]; then
            echo "ERROR: too many interfaces in line '${ZTXTLINE}'."
            exit 28
        fi

        #--- Device is running? - Get the pid.
        ZMATCH="${ZROUTEREXE} -n ${CFGNVRAMLEN} -m ${ZRAMMEMEXE} -e ${ZROUTERETH} -s ${ZROUTERSER} ${ZROUTERIDX}( |$)"
        ZROUTERRUNN=`echo "${ZPROCCESS}" | grep -v "${CFGIOUWRAPPER}" | grep -E "${ZMATCH}"`
        if [ -z "${ZROUTERRUNN}" ]; then
            ZROUTERRUNN=0
        else
            ZROUTERRUNN=`echo ${ZROUTERRUNN} | cut -d ' ' -f 1`
            if [ -z "${ZROUTERRUNN}" ]; then
                ZROUTERRUNN=0
            fi
        fi

        #--- Wrapper is running? - Get the pid.
        ZMATCH="${CFGIOUWRAPPER} -m ${ZROUTEREXE} -p ${ZROUTERPORT} -- -n ${CFGNVRAMLEN} -m ${ZRAMMEMEXE} -e ${ZROUTERETH} -s ${ZROUTERSER} ${ZROUTERIDX}( |$)"
        ZWRAPPERRUNN=`echo "${ZPROCCESS}" | grep "${CFGIOUWRAPPER}" | grep -E -- "${ZMATCH}"`
        if [ -z "${ZWRAPPERRUNN}" ]; then
            ZWRAPPERRUNN=0
        else
            ZWRAPPERRUNN=`echo ${ZWRAPPERRUNN} | cut -d ' ' -f 1`
            if [ -z "${ZWRAPPERRUNN}" ]; then
                ZWRAPPERRUNN=0
            fi
        fi

        #--- Fill the array.
        NMAPIOURTIDX[${ZROUTERIDX}]="${ZROUTERIDX}"
        NMAPIOURTACR[${ZROUTERIDX}]="${ZROUTERACR}"
        NMAPIOURTSCR[${ZROUTERIDX}]="${ZROUTERACR}"
        NMAPIOURTIOS[${ZROUTERIDX}]="${ZROUTEREXE}"
        NMAPIOURTRAM[${ZROUTERIDX}]="${ZRAMMEMEXE}"
        NMAPIOURTETH[${ZROUTERIDX}]="${ZROUTERETH}"
        NMAPIOURTSER[${ZROUTERIDX}]="${ZROUTERSER}"
        NMAPIOURTRUN[${ZROUTERIDX}]="${ZROUTERRUNN}"
        NMAPIOURTWRP[${ZROUTERIDX}]="${ZWRAPPERRUNN}"
        NMAPIOURTPRT[${ZROUTERIDX}]="${ZROUTERPORT}"

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done

    #--- Widest acronym.
    NMAPIOURTLNG=${ZWIDESTACR}

    return
    }

#--- Parse the netmap file for external devices (consoles).
function parserealdevs {
    #---  Return arrays:
    #---    NMAPREALRTIDX: device id.
    #---    NMAPREALRTACR: acronym.
    #---    NMAPREALRTCMD: command and arguments.
    #--- Return var:
    #---    NMAPREALRTLNG: length of the widest acronym.
    local ZLISTXT
    local ZNUMLIN
    local ZROUTERACR
    local ZROUTERCMD
    local ZROUTERIDX
    local ZTXTLINE
    local ZWIDESTACR

    #--- Do not refill the arrays.
    if [ ${#NMAPREALRTIDX[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPREALRTIDX
    unset NMAPREALRTACR
    unset NMAPREALRTCMD

    #--- Parse the netmap file.
    ZWIDESTACR=0
    ZNUMLIN=1
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#\/' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZROUTERIDX=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 2`
        ZROUTERACR=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 3`
        ZROUTERCMD=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 4-`

        #--- Check if empty.
        checkempty "deviceid" "${ZROUTERIDX}" "${ZTXTLINE}"
        checkempty "acronym"  "${ZROUTERACR}" "${ZTXTLINE}"
        checkempty "command"  "${ZROUTERCMD}" "${ZTXTLINE}"

        #--- Check the device id.
        validatenum ${ZROUTERIDX} 1 -1 "invalid device id in line '${ZTXTLINE}'"

        #--- Calc the widest acronym.
        if [ ${#ZROUTERACR} -gt ${ZWIDESTACR} ]; then
            ZWIDESTACR=${#ZROUTERACR}
        fi

        #--- Fill the array.
        NMAPREALRTIDX[${ZROUTERIDX}]="${ZROUTERIDX}"
        NMAPREALRTACR[${ZROUTERIDX}]="${ZROUTERACR}"
        NMAPREALRTCMD[${ZROUTERIDX}]="${ZROUTERCMD}"

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done

    #--- Widest acronym.
    NMAPREALRTLNG=${ZWIDESTACR}

    return
    }

#--- Parse the netmap file for internal connections.
function parseintconns {
    #--- Return arrays:
    #---    NMAPINTCONNNDX: connection index.
    #---    NMAPINTCONNDEF: line with the connection.
    #---    NMAPINTCONNCNT: number of interfaces in the connection.
    local ZCNTINTF
    local ZLISTXT
    local ZNUMLIN
    local ZTXTLINE

    #--- Do not refill the arrays.
    if [ ${#NMAPINTCONNNDX[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPINTCONNNDX
    unset NMAPINTCONNDEF
    unset NMAPINTCONNCNT

    #--- Parse the netmap file.
    ZNUMLIN=1
    ZLISTXT=`cat "${CFGNETMAPFILE}" | cut -d '#' -f 1 | cleanline | grep -v '^$' | grep -E '(^| )[0-1]*[0-9]{1,3}:[0-1]*[0-9]/[0-3](@| |$)'`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Check for incomplete connections.
        ZCNTINTF=$(( `echo "${ZTXTLINE}" | tr ' ' '\n' | wc -l` ))
        if [ ${ZCNTINTF} -lt 2 ]; then
            echo "ERROR: incomplete connection in line ${ZTXTLINE}."
            exit 29
        fi

        #--- If the connection have an @, exit with error.
        if [ ! -z "`echo \"${ZTXTLINE}\" | grep '@'`" ]; then
            echo "ERROR: internal connection with @hostname in line '${ZTXTLINE}'."
            exit 30
        fi

        #--- Fill the arrays.
        NMAPINTCONNNDX[${ZNUMLIN}]=${ZNUMLIN}
        NMAPINTCONNDEF[${ZNUMLIN}]="${ZTXTLINE}"
        NMAPINTCONNCNT[${ZNUMLIN}]=${ZCNTINTF}

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done
    return
    }

#--- Parse the netmap file for external udp connections.
function parseextudp {
    #--- Return arrays:
    #---    NMAPUDPRTIDNT: router id.
    #---    NMAPUDPRTLPRT: connection local port.
    #---    NMAPUDPRTRDIP: connection remote IP.
    #---    NMAPUDPRTRPRT: connection remote port.
    #---    NMAPUDPRTDEST: IOU side of the connection.
    #---    NMAPUDPRTRUNN: 0:stopped - <> 0: pid.
    local ZCONNLFT
    local ZCONNRGT
    local ZCONNRUNN
    local ZERROR
    local ZFOUND
    local ZINTNDX
    local ZLISTXT
    local ZLOCALPORT
    local ZNUMLIN
    local ZPROCCESS
    local ZREMOTEIP
    local ZREMOTEPORT
    local ZROUTERNUM
    local ZTOOMANYPM
    local ZTXTLINE

    #--- Force to clear the arrays.
    if [ "$1" == "FORCE" ]; then
        unset NMAPUDPRTIDNT
    fi

    #--- Do not refill the arrays.
    if [ ${#NMAPUDPRTIDNT[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPUDPRTIDNT
    unset NMAPUDPRTLPRT
    unset NMAPUDPRTRDIP
    unset NMAPUDPRTRPRT
    unset NMAPUDPRTDEST
    unset NMAPUDPRTRUNN

    #--- Get the internal connections.
    parseintconns

    #--- Parse the netmap file.
    ZNUMLIN=1
    ZPROCCESS=`ps ax -w -w | cleanline`
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#\\$' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZROUTERNUM=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 2`
        ZLOCALPORT=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 3`
        ZREMOTEIP=`echo "${ZTXTLINE}"   | cut -s -d ' ' -f 4`
        ZREMOTEPORT=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 5`
        ZTOOMANYPM=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 6-`

        #--- Check if there are too many parameters.
        if [ ! -z "${ZTOOMANYPM}" ]; then
            echo "ERROR: too many parameters in line '${ZTXTLINE}'."
            exit 31
        fi

        #--- Check if empty.
        checkempty "deviceid"   "${ZROUTERNUM}"  "${ZTXTLINE}"
        checkempty "localport"  "${ZLOCALPORT}"  "${ZTXTLINE}"
        checkempty "remoteip"   "${ZREMOTEIP}"   "${ZTXTLINE}"
        checkempty "remoteport" "${ZREMOTEPORT}" "${ZTXTLINE}"

        #--- Check the device id.
        validatenum ${ZROUTERNUM} 1 1023 "invalid device id in line '${ZTXTLINE}'"

        #--- Check the local port.
        validatenum ${ZLOCALPORT} 1024 65535 "invalid local port in line '${ZTXTLINE}'"

        #--- Check the remote IP.
        if [ "${CFGOPERSYSTEM}" == "BSD" ]; then
            gethost "${ZREMOTEIP}" > /dev/null 2> /dev/null
            ZERROR=$?
        else
            gethostip -d "${ZREMOTEIP}" > /dev/null 2> /dev/null
            ZERROR=$?
        fi
        if [ ${ZERROR} -ne 0 ]; then
            echo "ERROR: not resolvable remote host in line '${ZTXTLINE}'."
            exit 32
        fi

        #--- Check the remote port.
        validatenum ${ZREMOTEPORT} 1024 65535 "invalid remote port in line '${ZTXTLINE}'"

        #--- Connection is running?. Get the pid.
        ZCONNRUNN=`echo "${ZPROCCESS}" | grep "${CFGSCRIPTDIR}/${CFGIOU2NET}" | grep -- "-p ${ZROUTERNUM}" | grep -E -- "-u ${ZLOCALPORT}:${ZREMOTEIP}:${ZREMOTEPORT}( |$)"`
        if [ -z "${ZCONNRUNN}" ]; then
            ZCONNRUNN=0
        else
            ZCONNRUNN=`echo ${ZCONNRUNN} | cut -d ' ' -f 1`
            if [ -z "${ZCONNRUNN}" ]; then
                ZCONNRUNN=0
            fi
        fi

        #--- Get the internal connection (IOU side).
        ZFOUND=0
        for ZINTNDX in ${NMAPINTCONNNDX[@]}; do
            if [ ! -z "`echo \"${NMAPINTCONNDEF[${ZINTNDX}]}\" | grep -E \"(^| )${ZROUTERNUM}:\"`" ]; then
                ZFOUND=1
                break
            fi
        done

        #--- Error if the connection is not found.
        if [ ${ZFOUND} -eq 0 ]; then
            echo "ERROR: internal connection #${ZROUTERNUM} not found."
            exit 33
        fi

        #--- Error if is a multipoint connection.
        if [ ${NMAPINTCONNCNT[${ZINTNDX}]} -ne 2 ]; then
            echo "ERROR: connection #${ZROUTERNUM} is multipoint."
            echo "ERROR: with external connections, multipoint is not allowed."
            exit 34
        fi

        #--- Swap left and right if it is necessary.
        ZCONNLFT=`echo "${NMAPINTCONNDEF[${ZINTNDX}]}" | cut -d ' ' -f 1`
        ZCONNRGT=`echo "${NMAPINTCONNDEF[${ZINTNDX}]}" | cut -d ' ' -f 2`
        if [ ! -z "`echo \"${ZCONNLFT}\" | grep -E \"${ZROUTERNUM}:\"`" ]; then
            ZCONNLFT="${ZCONNRGT}"
        fi

        #--- Fill the arrays.
        NMAPUDPRTIDNT[${ZROUTERNUM}]="${ZROUTERNUM}"
        NMAPUDPRTLPRT[${ZROUTERNUM}]="${ZLOCALPORT}"
        NMAPUDPRTRDIP[${ZROUTERNUM}]="${ZREMOTEIP}"
        NMAPUDPRTRPRT[${ZROUTERNUM}]="${ZREMOTEPORT}"
        NMAPUDPRTDEST[${ZROUTERNUM}]="${ZCONNLFT}"
        NMAPUDPRTRUNN[${ZROUTERNUM}]="${ZCONNRUNN}"

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done
    return
    }

#--- Parse the netmap file for external interface connections.
function parseextintf {
    #--- Return arrays:
    #---    NMAPINTFRTIDNT: device id.
    #---    NMAPINTFRTIFAZ: interface to bind.
    #---    NMAPINTFRTRUNN: 0:stopped - <> 0: pid.
    #---    NMAPINTFRTDEST: IOU side of the connection.
    local ZCONNLFT
    local ZCONNRGT
    local ZCONNRUNN
    local ZFOUND
    local ZINTERFACE
    local ZINTNDX
    local ZLISTXT
    local ZNUMLIN
    local ZPROCCESS
    local ZROUTERNUM
    local ZTAPBRGNUM
    local ZTOOMANYPM
    local ZTXTLINE

    #--- Force to clear the arrays.
    if [ "$1" == "FORCE" ]; then
        unset NMAPINTFRTIDNT
    fi

    #--- Do not refill the arrays.
    if [ ${#NMAPINTFRTIDNT[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPINTFRTIDNT
    unset NMAPINTFRTIFAZ
    unset NMAPINTFRTRUNN
    unset NMAPINTFRTDEST

    #--- Get the internal connections.
    parseintconns

    #--- Parse the netmap file.
    ZNUMLIN=1
    ZPROCCESS=`ps ax -w -w | cleanline`
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#@' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZROUTERNUM=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 2`
        ZINTERFACE=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 3`
        ZTOOMANYPM=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 4-`

        #--- Check if there are too many parameters.
        if [ ! -z "${ZTOOMANYPM}" ]; then
            echo "ERROR: too many parameters in line '${ZTXTLINE}'."
            exit 35
        fi

        #--- Check if is empty.
        checkempty "deviceid"   "${ZROUTERNUM}"  "${ZTXTLINE}"
        checkempty "interface"  "${ZINTERFACE}"  "${ZTXTLINE}"

        #--- Check the device id.
        validatenum ${ZROUTERNUM} 1 1023 "invalid device id in line '${ZTXTLINE}'"

        #--- Check the interface.
        if [ -z "${ZINTERFACE}" ]; then
            echo "ERROR: empty interface in line '${ZTXTLINE}'."
            exit 36
        fi

        #--- Check the interface number.
        ZTAPBRGNUM=`printf "%04d" "${ZROUTERNUM}"`

        #--- Connection is running?. Get pid.
        ZCONNRUNN=`echo "${ZPROCCESS}" | grep "${CFGSCRIPTDIR}/${CFGIOU2NET}" | grep -- "-p ${ZROUTERNUM}" | grep -E -- "-t ${CFGINTFEXTTAP}${ZTAPBRGNUM}( |$)"`
        if [ -z "${ZCONNRUNN}" ]; then
            ZCONNRUNN=`echo "${ZPROCCESS}" | grep "${CFGSCRIPTDIR}/${CFGIOU2NET}" | grep -- "-p ${ZROUTERNUM}" | grep -E -- "-i ${ZINTERFACE}( |$)"`
        fi
        if [ -z "${ZCONNRUNN}" ]; then
            ZCONNRUNN=0
        else
            ZCONNRUNN=`echo ${ZCONNRUNN} | cut -d ' ' -f 1`
            if [ -z "${ZCONNRUNN}" ]; then
                ZCONNRUNN=0
            fi
        fi

        #--- Get the internal connection (IOU side).
        ZFOUND=0
        for ZINTNDX in ${NMAPINTCONNNDX[@]}; do
            if [ ! -z "`echo \"${NMAPINTCONNDEF[${ZINTNDX}]}\" | grep -E \"(^| )${ZROUTERNUM}:\"`" ]; then
                ZFOUND=1
                break
            fi
        done

        #--- Error if the connection is not found.
        if [ ${ZFOUND} -eq 0 ]; then
            echo "ERROR: internal connection #${ZROUTERNUM} not found."
            exit 37
        fi

        #--- Error if is a multipoint connection.
        if [ ${NMAPINTCONNCNT[${ZINTNDX}]} -ne 2 ]; then
            echo "ERROR: connection #${ZROUTERNUM} is multipoint."
            echo "ERROR: with external connections, multipoint is not allowed."
            exit 38
        fi

        #--- Swap left and right if it is necessary.
        ZCONNLFT=`echo "${NMAPINTCONNDEF[${ZINTNDX}]}" | cut -d ' ' -f 1`
        ZCONNRGT=`echo "${NMAPINTCONNDEF[${ZINTNDX}]}" | cut -d ' ' -f 2`
        if [ ! -z "`echo \"${ZCONNLFT}\" | grep -E \"${ZROUTERNUM}:\"`" ]; then
            ZCONNLFT="${ZCONNRGT}"
        fi

        #--- Fill the arrays.
        NMAPINTFRTIDNT[${ZROUTERNUM}]="${ZROUTERNUM}"
        NMAPINTFRTIFAZ[${ZROUTERNUM}]="${ZINTERFACE}"
        NMAPINTFRTRUNN[${ZROUTERNUM}]="${ZCONNRUNN}"
        NMAPINTFRTDEST[${ZROUTERNUM}]="${ZCONNLFT}"

        #--- Next line.
        ZNUMLIN=$(( ${ZNUMLIN} + 1))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done
    return
    }

#--- Parse the netmap file for external tap connections.
function parseexttap {
    #--- Return arrays:
    #---    NMAPTAPRTIDNT: router id.
    #---    NMAPTAPRTDEST: IOU side of the connection.
    #---    NMAPTAPPRUPLST: pre-up command list.
    #---    NMAPTAPPRDNLST: pre-down command list.
    #---    NMAPTAPPOUPLST: post-up command list.
    #---    NMAPTAPPODNLST: post-down command list.
    #---    NMAPTAPPLRUNN: 0:stopped - <> 0: pid.
    local ZCOMMAND
    local ZCONNLFT
    local ZCONNRGT
    local ZCONNRUNN
    local ZFOUND
    local ZINTNDX
    local ZLISTXT
    local ZNUMLIN
    local ZPREPOST
    local ZPROCCESS
    local ZROUTERNUM
    local ZTAPNUM
    local ZTXTLINE

    #--- Force to clear the arrays.
    if [ "$1" == "FORCE" ]; then
        unset NMAPTAPRTIDNT
    fi

    #--- Do not refill the arrays.
    if [ ${#NMAPTAPRTIDNT[@]} -ne 0 ]; then
        return
    fi

    #--- Clean the arrays.
    unset NMAPTAPRTIDNT
    unset NMAPTAPRTDEST
    unset NMAPTAPPRUPLST
    unset NMAPTAPPRDNLST
    unset NMAPTAPPOUPLST
    unset NMAPTAPPODNLST
    unset NMAPTAPPLRUNN

    #--- Get the internal connections.
    parseintconns

    #--- Parse the netmap file.
    ZNUMLIN=1
    ZPROCCESS=`ps ax -w -w | cleanline`
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#\\^' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZROUTERNUM=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 2`
        ZPREPOST=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 3`
        ZCOMMAND=`echo "${ZTXTLINE}"  | cut -s -d ' ' -f 4-`

        #--- Check if it is empty.
        checkempty "deviceid" "${ZROUTERNUM}" "${ZTXTLINE}"
        checkempty "pre/post" "${ZPREPOST}"   "${ZTXTLINE}"
        checkempty "command"  "${ZCOMMAND}"   "${ZTXTLINE}"

        #--- Check the device id.
        validatenum ${ZROUTERNUM} 1 1023 "invalid device id in line '${ZTXTLINE}'"

        #--- Connection is running?. Get pid.
        ZTAPNUM=`printf "%04d" "${ZROUTERNUM}"`
        ZCONNRUNN=`echo "${ZPROCCESS}" | grep "${CFGSCRIPTDIR}/${CFGIOU2NET}" | grep -- "-p ${ZROUTERNUM}" | grep -E -- "-t ${CFGINTFINTTAP}${ZTAPNUM}( |$)"`
        if [ -z "${ZCONNRUNN}" ]; then
            ZCONNRUNN=0
        else
            ZCONNRUNN=`echo ${ZCONNRUNN} | cut -d ' ' -f 1`
            if [ -z "${ZCONNRUNN}" ]; then
                ZCONNRUNN=0
            fi
        fi

        #--- Get the internal connection (IOU side).
        ZFOUND=0
        for ZINTNDX in ${NMAPINTCONNNDX[@]}; do
            if [ ! -z "`echo \"${NMAPINTCONNDEF[${ZINTNDX}]}\" | grep -E \"(^| )${ZROUTERNUM}:\"`" ]; then
                ZFOUND=1
                break
            fi
        done

        #--- Error if the connection is not found.
        if [ ${ZFOUND} -eq 0 ]; then
            echo "ERROR: internal connection #${ZROUTERNUM} not found."
            exit 94
        fi

        #--- Error if is a multipoint connection.
        if [ ${NMAPINTCONNCNT[${ZINTNDX}]} -ne 2 ]; then
            echo "ERROR: connection #${ZROUTERNUM} is multipoint."
            echo "ERROR: with external connections, multipoint is not allowed."
            exit 95
        fi

        #--- Swap left and right if is necessary.
        ZCONNLFT=`echo "${NMAPINTCONNDEF[${ZINTNDX}]}" | cut -d ' ' -f 1`
        ZCONNRGT=`echo "${NMAPINTCONNDEF[${ZINTNDX}]}" | cut -d ' ' -f 2`
        if [ ! -z "`echo \"${ZCONNLFT}\" | grep -E \"${ZROUTERNUM}:\"`" ]; then
            ZCONNLFT="${ZCONNRGT}"
        fi

        #--- Expand the command.
        ZCOMMAND=`echo "${ZCOMMAND}" | sed -e "s/<TAP>/${CFGINTFINTTAP}${ZTAPNUM}/g"`

        #--- Fill the arrays.
        NMAPTAPRTIDNT[${ZROUTERNUM}]="${ZROUTERNUM}"
        NMAPTAPRTDEST[${ZROUTERNUM}]="${ZCONNLFT}"
        NMAPTAPPLRUNN[${ZROUTERNUM}]="${ZCONNRUNN}"
        case "${ZPREPOST}" in
            "pre-up")
                NMAPTAPPRUPLST[${ZROUTERNUM}]="${NMAPTAPPRUPLST[${ZROUTERNUM}]}${ZCOMMAND}${CFGTAPSPRTR}"
                ;;
            "pre-down")
                NMAPTAPPRDNLST[${ZROUTERNUM}]="${NMAPTAPPRDNLST[${ZROUTERNUM}]}${ZCOMMAND}${CFGTAPSPRTR}"
                ;;
            "post-up")
                NMAPTAPPOUPLST[${ZROUTERNUM}]="${NMAPTAPPOUPLST[${ZROUTERNUM}]}${ZCOMMAND}${CFGTAPSPRTR}"
                ;;
            "post-down")
                NMAPTAPPODNLST[${ZROUTERNUM}]="${NMAPTAPPODNLST[${ZROUTERNUM}]}${ZCOMMAND}${CFGTAPSPRTR}"
                ;;
            *)
                echo "ERROR: invalid pre/post keyword in line '${ZTXTLINE}'"
                exit 96
                ;;
        esac

        #--- Next line.
        ZNUMLIN=$(( $ZNUMLIN + 1 ))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done

    return
    }

#--- Parse the netmap file for remote console telnet IP.
function parsetelnet {
    #--- Return vars:
    #---    NMAPTELNETDIP: ip address of the machine which run IOU.
    #---    NMAPTELNETREM: 0: local telnet. 1: remote telnet.
    local ZERROR
    local ZIPADDR
    local ZLINE
    local ZMYIPADDR
    local ZTOOMANYPM

    #--- Do not refill the variables.
    if [ ! -z "${NMAPTELNETDIP}" ]; then
        return
    fi

    #--- Parse the netmap file.
    ZLINE=`cat "${CFGNETMAPFILE}" | grep -e '^#&' | cleanline | tail -n 1`
    if [ -z "${ZLINE}" ]; then
        ZLINE="#& 127.0.0.1"
        NMAPTELNETREM=0
    else
        NMAPTELNETREM=1
    fi
    ZIPADDR=`echo "${ZLINE}" | cut -s -d ' ' -f 2`
    ZTOOMANYPM=`echo "${ZLINE}" | cut -s -d ' ' -f 3-`

    #--- Check for too many parameters.
    if [ ! -z "${ZTOOMANYPM}" ]; then
        echo "ERROR: too many parameters in line '${ZTXTLINE}'."
        exit 39
    fi

    #--- Check param.
    if [ -z "${ZIPADDR}" ]; then
        echo "ERROR: empty telnet host in line '${ZLINE}'."
        exit 40
    fi
    if [ "${CFGOPERSYSTEM}" == "BSD" ]; then
        gethost "${ZIPADDR}" > /dev/null 2> /dev/null
        ZERROR=$?
    else
        gethostip -d "${ZIPADDR}" > /dev/null 2> /dev/null
        ZERROR=$?
    fi
    if [ ${ZERROR} -ne 0 ]; then
        echo "ERROR: not resolvable telnet host in line '${ZLINE}'."
        exit 41
    fi

    #--- Get the ip address.
    ZMYIPADDR=`getlocalip`
    if [ "${ZIPADDR}" == "${ZMYIPADDR}" ] || [ "${ZIPADDR}" == "127.0.0.1" ]; then
        NMAPTELNETREM=0
    fi

    #--- Finish.
    NMAPTELNETDIP="${ZIPADDR}"
    return
    }

#--- Parse the netmap file for a descriptive text in the caption of screen.
function parsecaptext {
    #--- Return var:
    #---    NMAPCAPTIONTYPE: 0: no caption. 1: simple. 2: custom.
    #---    NMAPCAPTIONSRND: text for the sorendition screen command.
    #---    NMAPCAPTIONMODE: mode of the caption: splitonly or always.
    #---    NMAPCAPTIONSCRN: text of the caption for screen.
    #---    NMAPCAPTIONLIST: text of the caption for list.
    local ZCAPTIONLIST
    local ZCAPTIONMODE
    local ZCAPTIONSCRN
    local ZCAPTIONTYPE
    local ZCOLLIN
    local ZCOLORACRON
    local ZCOLORBGRND
    local ZCOLORCURSR
    local ZCOLORDESCR
    local ZLINE

    #--- Do not refill the variables.
    if [ ! -z "${NMAPCAPTIONTYPE}" ]; then
        return
    fi

    #--- Default values.
    ZCOLORACRON="${CFGSCREENCOLORS:0:1}"
    ZCOLORDESCR="${CFGSCREENCOLORS:1:1}"
    ZCOLORCURSR="${CFGSCREENCOLORS:2:1}"
    ZCOLORBGRND="${CFGSCREENCOLORS:3:1}"
    ZCAPTIONMODE="${CFGSCREENMODE}"
    ZCAPTIONSCRN="${CFGSCREENTEXT}"
    ZCAPTIONLIST=""
    ZCAPTIONTYPE=0

    #--- Parse the netmap file.
    ZLINE=`cat "${CFGNETMAPFILE}" | grep -e '^#=' | cleanline | tail -n 1`
    if [ ! -z "${ZLINE}" ]; then
        ZCAPTIONMODE="always"
        ZLINE=`echo "${ZLINE}" | sed -e 's/^#=//' | cleanline`
        #--- Definition line found. Check if is a simple or custom caption.
        if [ -z "`echo \"${ZLINE}\" | grep -E '^(always|splitonly) '`" ]; then
            #--- Simple caption. Check if have colors.
            ZCAPTIONTYPE=1
            if [ ! -z "`echo \"${ZLINE}\" | grep -E '^{'`" ]; then
                #--- Have colors. Get it.
                ZCOLLIN=`echo "${ZLINE}" | cut -d '{' -f 2 | cut -d '}' -f 1`
                if [ ! -z "${ZCOLLIN:0:1}" ]; then
                    ZCOLORACRON="${ZCOLLIN:0:1}"
                fi
                if [ ! -z "${ZCOLLIN:1:1}" ]; then
                    ZCOLORDESCR="${ZCOLLIN:1:1}"
                fi
                if [ ! -z "${ZCOLLIN:2:1}" ]; then
                    ZCOLORCURSR="${ZCOLLIN:2:1}"
                fi
                if [ ! -z "${ZCOLLIN:3:1}" ]; then
                    ZCOLORBGRND="${ZCOLLIN:3:1}"
                fi
                ZLINE=`echo "${ZLINE}" | cut -d '}' -f 2 | cleanline`
            fi
            #--- Get the caption text with description.
            if [ -z "${ZLINE}" ]; then
                ZCAPTIONTYPE=0
                ZCAPTIONLIST="<no caption>"
            else
                ZCAPTIONLIST="${ZLINE}"
                ZLINE="%=%{${ZCOLORDESCR}${ZCOLORBGRND}}[${ZLINE}]"
            fi
            ZCAPTIONSCRN="%{${ZCOLORACRON}${ZCOLORBGRND}}${ZCAPTIONSCRN}${ZLINE}"
        else
            #--- Custom caption.
            ZCAPTIONTYPE=2
            ZCAPTIONLIST="<custom>"
            ZCAPTIONMODE=`echo "${ZLINE}" | cut -d ' ' -f 1`
            ZCOLORCURSR=`echo "${ZLINE}" | cut -d ' ' -f 2 | tr -d '"' | tr -d "'"`
            ZCOLORCURSR="${ZCOLORCURSR:0:2}"
            if [ ! -z "${ZCOLORCURSR:1:1}" ]; then
                ZCOLORBGRND=""
            fi
            ZCAPTIONSCRN=`echo "${ZLINE}" | cut -d ' ' -f 3- | tr -d '"' | tr -d "'"`
            if [ -z "${ZCAPTIONSCRN}" ]; then
                ZCAPTIONTYPE=0
                ZCAPTIONSCRN="%{${ZCOLORACRON}${ZCOLORBGRND}}${ZCAPTIONSCRN}"
                ZCAPTIONLIST="<no caption>"
            fi
        fi
    else
        #--- No definition line found. Use the default values.
        ZCAPTIONTYPE=0
        ZCAPTIONSCRN="%{${ZCOLORACRON}${ZCOLORBGRND}}${ZCAPTIONSCRN}"
        ZCAPTIONLIST="<no caption>"
    fi

    #--- Store the result.
    NMAPCAPTIONTYPE=${ZCAPTIONTYPE}
    NMAPCAPTIONSRND="${ZCOLORCURSR}${ZCOLORBGRND}"
    NMAPCAPTIONMODE="${ZCAPTIONMODE}"
    NMAPCAPTIONSCRN="${ZCAPTIONSCRN}"
    NMAPCAPTIONLIST="${ZCAPTIONLIST}"

    return
    }

#--- Parse the netmap file for the initial startup config of IOU devices.
function parsestartup {
    #--- Return var:
    #---    NMAPSTARTUPCFG: plain text with the initial startup config.
    local ZCOMMAND
    local ZLISTXT
    local ZNUMLIN
    local ZTXTLINE

    #--- Do not refill the variables.
    if [ ! -z "${NMAPSTARTUPCFG}" ]; then
        return
    fi

    #--- Default values.
    NMAPSTARTUPCFG=""

    #--- Parse the netmap file.
    ZNUMLIN=1
    ZLISTXT=`cat "${CFGNETMAPFILE}" | grep -e '^#<' | cleanline`
    ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    while [ ! -z "${ZTXTLINE}" ]; do
        #--- Get the params.
        ZCOMMAND=`echo "${ZTXTLINE}" | cut -s -d ' ' -f 2- | cleanline`

        #--- Check if is empty.
        checkempty "command" "${ZCOMMAND}" "${ZTXTLINE}"

        #--- Append to the startup config.
        if [ "${ZCOMMAND}" == "global" ]; then
            NMAPSTARTUPCFG=$'\n'"${NMAPSTARTUPCFG}"$'\n'"${CFGTEMPLATESTART}"$'\n'
        else
            NMAPSTARTUPCFG=$'\n'"${NMAPSTARTUPCFG}"$'\n'"${ZCOMMAND}"$'\n'
        fi

        #--- Next line.
        ZNUMLIN=$(( $ZNUMLIN + 1 ))
        ZTXTLINE=`echo "${ZLISTXT}" | sed -n "${ZNUMLIN}p"`
    done

    #--- Delete empty lines and the end of text.
    NMAPSTARTUPCFG=`echo "${NMAPSTARTUPCFG}" | cleanline | sed -e 's/^end$//' | sed '/^$/d'`

    #--- Add the end of text.
    if [ ! -z "${NMAPSTARTUPCFG}" ]; then
        NMAPSTARTUPCFG="${NMAPSTARTUPCFG}"$'\n'"!"$'\n'"end"$'\n'
    fi

    return
    }

#--- Calculate the name of window for every device.
function calcwinnames {
    #--- Return arrays:
    #---    NMAPWINDOWIDX: router id.
    #---    NMAPWINDOWACR: acronym.
    #---    NMAPWINDOWNAM: window name.
    local ZDEVID
    local ZFILLCNT
    local ZFILLTXT
    local ZFULLWIDTH

    #--- Clean the arrays.
    unset NMAPWINDOWIDX
    unset NMAPWINDOWACR
    unset NMAPWINDOWNAM

    #--- Parse the netmap file.
    parseiourtsw
    parserealdevs
    parseseparat

    #--- Initial fill of the array.
    for ZDEVID in ${NMAPIOURTIDX[@]}; do
        NMAPWINDOWIDX[${ZDEVID}]=${ZDEVID}
        NMAPWINDOWACR[${ZDEVID}]="${NMAPIOURTACR[${ZDEVID}]}"
        NMAPWINDOWNAM[${ZDEVID}]="${NMAPIOURTACR[${ZDEVID}]}"
    done
    for ZDEVID in ${NMAPREALRTIDX[@]}; do
        NMAPWINDOWIDX[${ZDEVID}]=${ZDEVID}
        NMAPWINDOWACR[${ZDEVID}]="${NMAPREALRTACR[${ZDEVID}]}"
        NMAPWINDOWNAM[${ZDEVID}]="${NMAPREALRTACR[${ZDEVID}]}"
    done

    #--- Calculate the justification of separators.
    if [ ${NMAPREALRTLNG} -gt ${NMAPIOURTLNG} ]; then
        ZFULLWIDTH=${NMAPREALRTLNG}
    else
        ZFULLWIDTH=${NMAPIOURTLNG}
    fi
    ZFULLWIDTH=$(( ${ZFULLWIDTH} + ${NMAPSEPRTLNG} + 3 ))

    #--- Change the acronyms with separators.
    for ZDEVID in ${NMAPSEPRTIDX[@]}; do
        #--- Search the device.
        if [ "${NMAPWINDOWIDX[${ZDEVID}]}" == "${ZDEVID}" ]; then
            #--- Device found, change the acronym.
            ZFILLCNT=$(( ${ZFULLWIDTH} - ${#NMAPWINDOWACR[${ZDEVID}]} - ${#NMAPSEPRTTXT[${ZDEVID}]} ))
            ZFILLTXT=`printf "%${ZFILLCNT}s" | tr ' ' "${NMAPSEPRTCHR[${ZDEVID}]}"`
            NMAPWINDOWNAM[${ZDEVID}]="${NMAPWINDOWACR[${ZDEVID}]} ${ZFILLTXT}${NMAPSEPRTTXT[${ZDEVID}]}"
        else
            #--- Invalid separator id.
            echo "ERROR: invalid separator id #${ZDEVID}, device not found."
            exit 91
        fi
    done
    return
    }

#--- Show information about the IOU devices.
function loc_show_iou {
    local ZACRONFMT
    local ZLOCALIP
    local ZRTINDEX
    local ZSTATUS

    #--- Parse the netmap file.
    parseiourtsw
    parsetelnet

    #--- Header.
    echo "iou devices:"

    #--- Check if there are no devices.
    if [ ${#NMAPIOURTIDX[@]} -le 0 ]; then
        echo "  no iou devices."
        return
    fi

    #--- Get the local IP.
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        ZLOCALIP="${NMAPTELNETDIP}"
    else
        ZLOCALIP="127.0.0.1"
    fi

    #--- Show the IOU devices.
    for ZRTINDEX in ${NMAPIOURTIDX[@]}; do
        ZSTATUS="unknown"
        if [ ${NMAPTELNETREM} -ne 0 ]; then
            ZSTATUS="remote"
        elif [ ${NMAPIOURTRUN[${ZRTINDEX}]} -ne 0 ]; then
            ZSTATUS="running"
        else
            ZSTATUS="stopped"
        fi
        ZACRONFMT=`printf "%-${NMAPIOURTLNG}s" "${NMAPIOURTACR[${ZRTINDEX}]}"`
        echo "  ${ZACRONFMT} - ${ZSTATUS} - telnet ${ZLOCALIP} ${NMAPIOURTPRT[${ZRTINDEX}]}"
    done

    return
    }

#--- Show information about the external real devices.
function loc_show_realdev {
    local ZACRONFMT
    local ZRTINDEX

    #--- Parse the netmap file.
    parserealdevs

    #--- Header.
    echo "external devices:"

    #--- Check if there are no devices.
    if [ ${#NMAPREALRTIDX[@]} -le 0 ]; then
        echo "  no external devices."
        return
    fi

    #--- Show the external devices.
    for ZRTINDEX in ${NMAPREALRTIDX[@]}; do
        ZACRONFMT=`printf "%-${NMAPREALRTLNG}s" "${NMAPREALRTACR[${ZRTINDEX}]}"`
        echo "  ${ZACRONFMT} - external - ${NMAPREALRTCMD[${ZRTINDEX}]}"
    done

    return
    }

#--- Show information about the external udp connections.
function loc_show_udpconn {
    local ZACRDST
    local ZDSTSLP
    local ZNDXDST
    local ZRTINDEX
    local ZSTATUS
    local ZTXTDST
    local ZTXTNDX

    #--- Parse the netmap file.
    parseextudp
    parsetelnet
    parseiourtsw

    #--- Header.
    echo "external udp connections:"

    #--- Check if there are no connections.
    if [ ${#NMAPUDPRTIDNT[@]} -le 0 ]; then
        echo "  no udp connections."
        return
    fi

    #--- Show the connections.
    for ZRTINDEX in ${NMAPUDPRTIDNT[@]}; do
        ZSTATUS="unknown"
        if [ ${NMAPTELNETREM} -ne 0 ]; then
            ZSTATUS="remote"
        elif [ ${NMAPUDPRTRUNN[${ZRTINDEX}]} -ne 0 ]; then
            ZSTATUS="running"
        else
            ZSTATUS="stopped"
        fi
        ZTXTNDX=`printf "%4d" "${NMAPUDPRTIDNT[${ZRTINDEX}]}"`
        ZNDXDST=`echo "${NMAPUDPRTDEST[${ZRTINDEX}]}" | cut -d ':' -f 1`
        ZDSTSLP=`echo "${NMAPUDPRTDEST[${ZRTINDEX}]}" | cut -d ':' -f 2`
        ZACRDST="${NMAPIOURTACR[${ZNDXDST}]}"
        if [ -z "${ZACRDST}" ]; then
            ZACRDST="UNKNOWN"
        fi
        ZTXTDST=`printf "%${NMAPIOURTLNG}s:${ZDSTSLP}" "${ZACRDST}"`
        echo "  ${ZTXTNDX} - ${ZSTATUS} - ${ZTXTDST} <--> ${NMAPUDPRTLPRT[${ZRTINDEX}]}:${NMAPUDPRTRDIP[${ZRTINDEX}]}:${NMAPUDPRTRPRT[${ZRTINDEX}]}"
    done
    return
    }

#--- Show information about the external interface connections.
function loc_show_intfconn {
    local ZACRDST
    local ZDSTSLP
    local ZNDXDST
    local ZRTINDEX
    local ZSTATUS
    local ZTXTDST
    local ZTXTNDX

    #--- Parse the netmap file.
    parseextintf
    parsetelnet

    #--- Header.
    echo "external interface connections:"

    #--- Check if there are no connections.
    if [ ${#NMAPINTFRTIDNT[@]} -le 0 ]; then
        echo "  no interface connections."
        return
    fi

    #--- Show the connections.
    for ZRTINDEX in ${NMAPINTFRTIDNT[@]}; do
        ZSTATUS="unknown"
        if [ ${NMAPTELNETREM} -ne 0 ]; then
            ZSTATUS="remote"
        elif [ ${NMAPINTFRTRUNN[${ZRTINDEX}]} -ne 0 ]; then
            ZSTATUS="running"
        else
            ZSTATUS="stopped"
        fi
        ZTXTNDX=`printf "%4d" "${NMAPINTFRTIDNT[${ZRTINDEX}]}"`
        ZNDXDST=`echo "${NMAPINTFRTDEST[${ZRTINDEX}]}" | cut -d ':' -f 1`
        ZDSTSLP=`echo "${NMAPINTFRTDEST[${ZRTINDEX}]}" | cut -d ':' -f 2`
        ZACRDST="${NMAPIOURTACR[${ZNDXDST}]}"
        if [ -z "${ZACRDST}" ]; then
            ZACRDST="UNKNOWN"
        fi
        ZTXTDST=`printf "%${NMAPIOURTLNG}s:${ZDSTSLP}" "${ZACRDST}"`
        echo "  ${ZTXTNDX} - ${ZSTATUS} - ${ZTXTDST} <--> ${NMAPINTFRTIFAZ[${ZRTINDEX}]}"
    done
    return
    }

#--- Show information about the external tap connections.
function loc_show_tapconn {
    local ZACRDST
    local ZDSTSLP
    local ZNDXDST
    local ZNUMNDX
    local ZRTINDEX
    local ZSTATUS
    local ZTXTDST
    local ZTXTNDX

    #--- Parse the netmap file.
    parseexttap
    parsetelnet

    #--- Header.
    echo "external tap connections:"

    #--- Check if there are no connections.
    if [ ${#NMAPTAPRTIDNT[@]} -le 0 ]; then
        echo "  no tap connections."
        return
    fi

    #--- Show the connections.
    for ZRTINDEX in ${NMAPTAPRTIDNT[@]}; do
        ZSTATUS="unknown"
        if [ ${NMAPTELNETREM} -ne 0 ]; then
            ZSTATUS="remote"
        elif [ ${NMAPTAPPLRUNN[${ZRTINDEX}]} -ne 0 ]; then
            ZSTATUS="running"
        else
            ZSTATUS="stopped"
        fi
        ZTXTNDX=`printf "%4d" "${NMAPTAPRTIDNT[${ZRTINDEX}]}"`
        ZNUMNDX=`printf "%04d" "${NMAPTAPRTIDNT[${ZRTINDEX}]}"`
        ZNDXDST=`echo "${NMAPTAPRTDEST[${ZRTINDEX}]}" | cut -d ':' -f 1`
        ZDSTSLP=`echo "${NMAPTAPRTDEST[${ZRTINDEX}]}" | cut -d ':' -f 2`
        ZACRDST="${NMAPIOURTACR[${ZNDXDST}]}"
        ZTXTDST=`printf "%${NMAPIOURTLNG}s:${ZDSTSLP}" "${ZACRDST}"`
        echo "  ${ZTXTNDX} - ${ZSTATUS} - ${ZTXTDST} <--> ${CFGINTFINTTAP}${ZNUMNDX}"
    done

    return
    }

#--- Show information about the others params.
function loc_show_others {
    local ZWHERE

    #--- Parse the netmap file.
    parsetelnet
    parsecaptext
    parsestartup

    #--- Header.
    echo "other info:"

    #--- Show where the lab is running.
    ZWHERE="local"
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        ZWHERE="remote"
    fi
    echo "  this lab is running in ${NMAPTELNETDIP} (${ZWHERE})."

    #--- Show the caption of the screen window.
    echo "  caption of screen window: ${NMAPCAPTIONLIST}"

    #--- Show the initial startup config.
    if [ ! -z "${NMAPSTARTUPCFG}" ]; then
        echo "  with iou initial startup config."
    else
        echo "  without iou initial startup config."
    fi

    return
    }

#--- Show who is capturing traffic.
function loc_show_capturing {
    local ZSHOWCAP

    #--- Header.
    echo "capturing traffic:"

    #--- Search for any capture.
    #--- Format: Capture Type | Daemon PID | Second IOU Dev ID | Description of capture | Text for list.
    ZSHOWCAP=`find "${CFGTEMPDIRECTORY}/" -type f -name 'capturing-*.info' -exec cat "{}" \; | cut -d '|' -f 5 | sort`
    if [ -z "${ZSHOWCAP}" ]; then
        ZSHOWCAP="  none."
    fi
    echo "${ZSHOWCAP}"

    return
    }

#--- Show information about the netmap file.
function do_showlist {
    loc_show_iou
    loc_show_realdev
    loc_show_udpconn
    loc_show_intfconn
    loc_show_tapconn
    loc_show_capturing
    loc_show_others
    return
    }

#--- Start some IOU devices.
function do_rtstart {
    #--- Params: $1 .. $2 .. $N: IOU devices to start.
    local ZACRONYM
    local ZARRERRFILES
    local ZERRORFILE
    local ZFOUND
    local ZHAVEERROR
    local ZNVRAMF
    local ZONESTARTED
    local ZRTINDEX
    local ZSTARTDEVS
    local ZSTRTCFG

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing IOU device(s) to start."
        exit 42
    fi

    #--- Check if is local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "iou devices are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate the duplicate devices.
    ZSTARTDEVS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Parse the netmap file.
    parseiourtsw
    parsestartup

    #--- Arrays of error files of devices started.
    unset ZARRERRFILES

    #--- Start every IOU device.
    ZONESTARTED=0
    for ZACRONYM in ${ZSTARTDEVS}; do
        #--- Search the device.
        ZFOUND=0
        for ZRTINDEX in ${NMAPIOURTIDX[@]}; do
            if [ "${NMAPIOURTACR[${ZRTINDEX}]}" == "${ZACRONYM}" ]; then
                #--- Device found.
                ZFOUND=1
                if [ ${NMAPIOURTRUN[${ZRTINDEX}]} -eq 0 ]; then
                    #--- Inform.
                    echo "starting ${ZACRONYM}"

                    #--- Create the error files.
                    ZERRORFILE="`mktemp \"${CFGTEMPDIRECTORY}/deviouerr-XXXXXXXX\" 2> /dev/null`"
                    touch "${ZERRORFILE}1"
                    touch "${ZERRORFILE}2"

                    #--- Check for the nvram file.
                    ZNVRAMF=`printf "nvram_%05d" ${ZRTINDEX}`
                    if [ ! -f "${ZNVRAMF}" ] && [ ! -z "${NMAPSTARTUPCFG}" ]; then
                        #--- Create the nvram file with the initial startup config.
                        ZSTRTCFG=`echo "${NMAPSTARTUPCFG}" | sed -e "s/%H/${ZACRONYM}/g" -e "s/%D/${ZRTINDEX}/g"`
                        writenvram "${ZNVRAMF}" "${ZSTRTCFG}" 0
                        if [ ! -f "${ZNVRAMF}" ]; then
                            echo "warning: nvram of ${ZACRONYM} not created (too big)."
                        fi
                    fi

                    #--- Start the IOU device.
                    "${CFGSCRIPTDIR}/${CFGIOUWRAPPER}" -m "${NMAPIOURTIOS[${ZRTINDEX}]}" -p "${NMAPIOURTPRT[${ZRTINDEX}]}" -- -n ${CFGNVRAMLEN} -m "${NMAPIOURTRAM[${ZRTINDEX}]}" -e "${NMAPIOURTETH[${ZRTINDEX}]}" -s "${NMAPIOURTSER[${ZRTINDEX}]}" "${ZRTINDEX}" > "${ZERRORFILE}1" 2> "${ZERRORFILE}2" &
                    ZARRERRFILES[${ZRTINDEX}]="${ZERRORFILE}"
                    ZONESTARTED=1
                    sleep 3
                else
                    #--- Device is already running.
                    echo "skipping ${ZACRONYM}"
                fi
                break
            fi
        done
        if [ ${ZFOUND} -eq 0 ]; then
            echo "unknown ${ZACRONYM} ***"
        fi
    done

    #--- Recalculate the running devices.
    if [ ${ZONESTARTED} -ne 0 ]; then
        make_sleep $(( 5 + ( `echo "${ZSTARTDEVS}" | wc -w` / 2 ) ))
        parseiourtsw "FORCE"

        #--- Check if all devices are started.
        ZHAVEERROR=0
        for ZACRONYM in ${ZSTARTDEVS}; do
            #--- Search the device.
            for ZRTINDEX in ${NMAPIOURTIDX[@]}; do
                if [ "${NMAPIOURTACR[${ZRTINDEX}]}" == "${ZACRONYM}" ]; then
                    #--- Device found.
                    if [ ${NMAPIOURTRUN[${ZRTINDEX}]} -eq 0 ] || [ ${NMAPIOURTWRP[${ZRTINDEX}]} -eq 0 ]; then
                        #--- Device is not running --> Error.
                        echo "--------------------------------------------------------"
                        echo "ERROR: ${ZACRONYM} is not running."
                        echo ""
                        cat "${ZARRERRFILES[${ZRTINDEX}]}1"
                        cat "${ZARRERRFILES[${ZRTINDEX}]}2"
                        echo "--------------------------------------------------------"
                        echo ""
                        ZHAVEERROR=1
                    fi
                    break
                fi
            done
        done

        #--- Clean all error files.
        for ZRTINDEX in ${NMAPIOURTIDX[@]}; do
            if [ -f "${ZARRERRFILES[${ZRTINDEX}]}" ]; then
                rm -f "${ZARRERRFILES[${ZRTINDEX}]}"
                rm -f "${ZARRERRFILES[${ZRTINDEX}]}1"
                rm -f "${ZARRERRFILES[${ZRTINDEX}]}2"
            fi
        done

        #--- Inform no error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "devices started ok."
        else
            exit 43
        fi
    fi

    return
    }

#--- Start some external udp connections.
function loc_extudp_start {
    #--- Params: $1 .. $2 .. $N: connections to start.
    local ZARRERRFILES
    local ZCONNID
    local ZERRORFILE
    local ZHAVEERROR
    local ZHOSTNAME
    local ZNMAPNET
    local ZONESTARTED
    local ZSTARTCONNS

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing connection(s) to start."
        exit 44
    fi

    #--- Check if is local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "udp connections are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate the duplicate connections.
    ZSTARTCONNS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Arrays of error files of connections started.
    unset ZARRERRFILES

    #--- Start every connection.
    ZHOSTNAME="`hostname -s`"
    ZONESTARTED=0
    for ZCONNID in ${ZSTARTCONNS}; do
        #--- Search the connection.
        if [ "${NMAPUDPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found.
            if [ ${NMAPUDPRTRUNN[${ZCONNID}]} -eq 0 ]; then
                #--- Inform.
                echo "starting udp conn #${ZCONNID}"

                #--- Creates the error files.
                ZERRORFILE="`mktemp \"${CFGTEMPDIRECTORY}/cnudperr-XXXXXXXX\" 2> /dev/null`"
                touch "${ZERRORFILE}1"
                touch "${ZERRORFILE}2"

                #--- Creates the netmap file for iou2net.
                ZNMAPNET="${CFGTEMPDIRECTORY}/cnudpnmap-${ZCONNID}"
                echo "${NMAPUDPRTDEST[${ZCONNID}]}@${ZHOSTNAME} ${ZCONNID}:0/0@${ZHOSTNAME}" > "${ZNMAPNET}"

                #--- Start the connection.
                "${CFGSCRIPTDIR}/${CFGIOU2NET}" -n "${ZNMAPNET}" -p "${NMAPUDPRTIDNT[${ZCONNID}]}" -u "${NMAPUDPRTLPRT[${ZCONNID}]}:${NMAPUDPRTRDIP[${ZCONNID}]}:${NMAPUDPRTRPRT[${ZCONNID}]}" > "${ZERRORFILE}1" 2> "${ZERRORFILE}2" &
                ZARRERRFILES[${ZCONNID}]="${ZERRORFILE}"
                ZONESTARTED=1
                sleep 3
            else
                #--- Connection already running.
                echo "skipping udp conn #${ZCONNID}"
            fi
        fi
    done

    #--- Recalculate the running connections.
    if [ ${ZONESTARTED} -ne 0 ]; then
        make_sleep $(( 5 + ( `echo "${ZSTARTCONNS}" | wc -w` / 4 ) ))
        parseextudp "FORCE"

        #--- Check if all connections are started.
        ZHAVEERROR=0
        for ZCONNID in ${ZSTARTCONNS}; do
            #--- Search the connection.
            if [ "${NMAPUDPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
                #--- Connection found.
                if [ ${NMAPUDPRTRUNN[${ZCONNID}]} -eq 0 ]; then
                    #--- Connection is not running --> Error.
                    echo "--------------------------------------------------------"
                    echo "ERROR: udp connection #${ZCONNID} is not running."
                    echo ""
                    cat "${ZARRERRFILES[${ZCONNID}]}1"
                    cat "${ZARRERRFILES[${ZCONNID}]}2"
                    echo "--------------------------------------------------------"
                    echo ""
                    ZHAVEERROR=1
                fi
            fi
        done

        #--- Clean all error files.
        for ZCONNID in ${ZSTARTCONNS}; do
            if [ -f "${ZARRERRFILES[${ZCONNID}]}" ]; then
                rm -f "${ZARRERRFILES[${ZCONNID}]}"
                rm -f "${ZARRERRFILES[${ZCONNID}]}1"
                rm -f "${ZARRERRFILES[${ZCONNID}]}2"
            fi
        done

        #--- Inform no error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "udp connections started ok."
        else
            exit 45
        fi
    fi

    return
    }

#--- Start some external interface connections.
function loc_extintf_start {
    #--- Params: $1 .. $2 .. $N: connections to start.
    local ZARRERRFILES
    local ZCONNID
    local ZERRORFILE
    local ZFISINTF
    local ZHAVEERROR
    local ZHOSTNAME
    local ZMAININTF
    local ZNMAPNET
    local ZROOTCMD
    local ZROOTUSR
    local ZSTARTCMD
    local ZSTARTCONNS
    local ZSTARTNEED
    local ZTAPBRGNUM
    local ZVLANINT

    #---
    #--- This function need root privileges to configure and use the tap and
    #--- bridge interfaces. A temporal bash script is created and executed via
    #--- su or sudo. See the $CFGROOTSUDOSU in the configuration section.
    #---
    #--- Thanks to Brezular
    #---    http://brezular.com/2013/10/09/how-to-connect-iou-to-a-real-cisco-gear-using-iou2net-pl/
    #---
    #--- Short help:
    #---    This method is used for vlan communication between an IOU device
    #---    with a real equipment with the host port in trunk 802.1q mode.
    #---
    #---                                                  +----------------------------------------------------+
    #---    IOU device port EthernetX/X <--> iou2net.pl <-+-> interface tapNNNN <--> physical interface ethN <-+-> real equipment with host port in trunk mode.
    #---                                                  +------------ interface bridgeNNNN ------------------+
    #--- As root:
    #---    * Creates the interface tapN: tunctl -t tapN
    #---    * Creates the interface bridgeN: brctl addbr bridgeN
    #---    * Bring interfaces up:
    #---      ifconfig tapN up ; ifconfig bridgeN up ; ifconfig ethN up
    #---    * Add interfaces tapN and ethN to the bridgeN:
    #---      brctl addif bridgeN tapN ; brctl addif bridgeN ethN
    #---    * Start iou2net.pl: iou2net.pl -t tapN -p ID
    #---    * Connect the port of IOU router/switch with the id of iou2net.pl:
    #---      iouswitchID:X/X@hostname  iou2netID:0/0@hostname
    #---
    #--- The tap / bridge mode is used only when:
    #---    * The operating system is GNU/Linux.
    #---    * The eth interface not have an ip address configured. This is
    #---      because when the interface is joined to a bridge the ip address
    #---      of the eth interface is deactivated. If you manage the host
    #---      machine via this interface, you are immediately disconnected and
    #---      can't reconnect until the bridge is deconfigured.
    #---    * In non GNU/Linux systems, the interface mode is used always.
    #---
    #--- The subinterfaces with 802.1q vlans only are autoconfigured when the
    #--- operating system is GNU/Linux.
    #---

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing connection(s) to start."
        exit 46
    fi

    #--- Check if is a local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "interface connections are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate duplicate connections.
    ZSTARTCONNS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Creates the script of commands (for exec with root privileges).
    ZSTARTCMD="`mktemp \"${CFGTEMPDIRECTORY}/cnintfcmds-XXXXXXXX\" 2> /dev/null`"
    echo "#!/usr/bin/env bash" > "${ZSTARTCMD}"
    echo "PATH=\"${PATH}\"" >> "${ZSTARTCMD}"
    echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"

    #--- Arrays of error files of connections started.
    unset ZARRERRFILES

    #--- Start every connection, build the script.
    ZHOSTNAME="`hostname -s`"
    ZSTARTNEED=0
    for ZCONNID in ${ZSTARTCONNS}; do
        #--- Search the connection.
        if [ "${NMAPINTFRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. Is running?.
            if [ ${NMAPINTFRTRUNN[${ZCONNID}]} -eq 0 ]; then
                #--- Inform.
                echo "echo \"starting intf conn #${ZCONNID}\"" >> "${ZSTARTCMD}"

                #--- Creates the error files.
                ZERRORFILE="`mktemp \"${CFGTEMPDIRECTORY}/cnintferr-XXXXXXXX\" 2> /dev/null`"
                touch "${ZERRORFILE}1"
                touch "${ZERRORFILE}2"

                #--- Check and enable the main interface.
                ZMAININTF=`echo "${NMAPINTFRTIFAZ[${ZCONNID}]}" | cut -d '.' -f 1`
                if [ -z "`ifconfig \"${ZMAININTF}\" 2> /dev/null | grep \"UP\"`" ]; then
                    echo "ifconfig \"${ZMAININTF}\" up > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                fi

                #--- Check the subinterface.
                if [ -z "`ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" 2> /dev/null | grep \"UP\"`" ]; then
                    #--- Check if dot1q vlans is needed.
                    if [ ! -z "`echo \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" | grep '\.'`" ]; then
                        #--- Only in GNU/Linux.
                        if [ "${CFGOPERSYSTEM}" == "LINUX" ]; then
                            #--- Add the vlan.
                            ZFISINTF=`echo "${NMAPINTFRTIFAZ[${ZCONNID}]}" | cut -d '.' -f 1`
                            ZVLANINT=`echo "${NMAPINTFRTIFAZ[${ZCONNID}]}" | cut -d '.' -f 2`

                            echo "modprobe 8021q > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                            echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                            echo "vconfig add \"${ZFISINTF}\" ${ZVLANINT} > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                            echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                        fi
                        #--- Enable the subinterface.
                        echo "ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" up > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                        echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                    fi
                fi

                #--- Interface number.
                ZTAPBRGNUM=`printf "%04d" "${ZCONNID}"`

                #--- Creates the netmap file for iou2net.
                ZNMAPNET="${CFGTEMPDIRECTORY}/cnintfnmap-${ZCONNID}"
                echo "${NMAPINTFRTDEST[${ZCONNID}]}@${ZHOSTNAME} ${ZCONNID}:0/0@${ZHOSTNAME}" > "${ZNMAPNET}"

                #--- If the interface have an ip address, can't use bridge.
                #--- If the OS not is GNU/Linux, use interface mode.
                if [ ! -z "`ifconfig ${NMAPINTFRTIFAZ[${ZCONNID}]} 2> /dev/null | grep -E 'inet |inet6 '`" ] || [ "${CFGOPERSYSTEM}" != "LINUX" ]; then
                    #--- Start the connection: connect the iou2net device directly to the interface.
                    echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -n \"${ZNMAPNET}\" -p ${NMAPINTFRTIDNT[${ZCONNID}]} -i \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\" &" >> "${ZSTARTCMD}"
                else
                    #--- Use tap / bridge mode.
                    #--- Creates the tap interface.
                    echo "tunctl -t \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                    echo "ifconfig \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" up > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                    #--- Creates the bridge interface.
                    echo "brctl addbr \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                    echo "ifconfig \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" up > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                    #--- Add the interfaces to the bridge.
                    echo "brctl addif \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                    echo "brctl addif \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"

                    #--- Start the connection: connect the iou2net device to the tap interface.
                    echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -n \"${ZNMAPNET}\" -p ${NMAPINTFRTIDNT[${ZCONNID}]} -t \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\" &" >> "${ZSTARTCMD}"
                fi

                #--- Sleep.
                echo "sleep 2" >> "${ZSTARTCMD}"

                #--- Add error files.
                ZARRERRFILES[${ZCONNID}]="${ZERRORFILE}"
                ZSTARTNEED=1
            else
                #--- Connection is already running.
                echo "echo \"skipping intf conn #${ZCONNID}\"" >> "${ZSTARTCMD}"
            fi
        fi
    done
    echo "popd >/dev/null 2> /dev/null" >> "${ZSTARTCMD}"

    #--- Command to get root privileges.
    ZROOTCMD="sudo"
    ZROOTUSR="${USER}"
    if [ ${CFGROOTSUDOSU} -ne 0 ]; then
        ZROOTCMD="su root -c"
        ZROOTUSR="root"
    fi

    #--- Execute the script.
    chmod 700 "${ZSTARTCMD}"
    if [ ${ZSTARTNEED} -ne 0 ]; then
        ln -s "/tmp/netio${UID}" "/tmp/netio0" > /dev/null 2> /dev/null
        echo "=> To start the interface connections, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTARTCMD}"
        rm -f "/tmp/netio0" > /dev/null 2> /dev/null
    else
        . "${ZSTARTCMD}"
    fi
    rm -f "${ZSTARTCMD}"

    #--- Recalculate the running connections.
    if [ ${ZSTARTNEED} -ne 0 ]; then
        make_sleep $(( 5 + ( `echo "${ZSTARTCONNS}" | wc -w` / 4 ) ))
        parseextintf "FORCE"

        #--- Check if all connections are started.
        ZHAVEERROR=0
        for ZCONNID in ${ZSTARTCONNS}; do
            #--- Search the connection.
            if [ "${NMAPINTFRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
                #--- Connection found.
                if [ ${NMAPINTFRTRUNN[${ZCONNID}]} -eq 0 ]; then
                    #--- Connection is not running --> Error.
                    echo "--------------------------------------------------------"
                    echo "ERROR: intf connection #${ZCONNID} is not running."
                    echo ""
                    cat "${ZARRERRFILES[${ZCONNID}]}"
                    cat "${ZARRERRFILES[${ZCONNID}]}1"
                    cat "${ZARRERRFILES[${ZCONNID}]}2"
                    echo "--------------------------------------------------------"
                    echo ""
                    ZHAVEERROR=1
                fi
            fi
        done

        #--- Clean all error files.
        for ZCONNID in ${ZSTARTCONNS}; do
            if [ -f "${ZARRERRFILES[${ZCONNID}]}" ]; then
                rm -f "${ZARRERRFILES[${ZCONNID}]}"
                rm -f "${ZARRERRFILES[${ZCONNID}]}1"
                rm -f "${ZARRERRFILES[${ZCONNID}]}2"
            fi
        done

        #--- Inform no error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "intf connections started ok."
        else
            exit 47
        fi
    fi

    return
    }

#--- Start some external tap connections.
function loc_exttap_start {
    #--- Params: $1 .. $2 .. $N: connections to start.
    local ZARRERRFILES
    local ZCONNID
    local ZERRORFILE
    local ZHAVEERROR
    local ZHOSTNAME
    local ZNMAPNET
    local ZNUMFLD
    local ZROOTCMD
    local ZROOTUSR
    local ZSTARTCMD
    local ZSTARTCONNS
    local ZSTARTNEED
    local ZTAPNAME
    local ZTXTFLD

    #---
    #--- This function need root privileges to configure and use the tap
    #--- interfaces. A temporal bash script is created and executed via su or
    #--- sudo. See the $CFGROOTSUDOSU in the configuration section.
    #---

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing connection(s) to start."
        exit 97
    fi

    #--- Check if is a local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "tap connections are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate duplicate connections.
    ZSTARTCONNS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Creates the script of commands (for exec with root privileges).
    ZSTARTCMD="`mktemp \"${CFGTEMPDIRECTORY}/cntapcmds-XXXXXXXX\" 2> /dev/null`"
    echo "#!/usr/bin/env bash" > "${ZSTARTCMD}"
    echo "PATH=\"${PATH}\"" >> "${ZSTARTCMD}"
    echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"

    #--- Arrays of error files of connections started.
    unset ZARRERRFILES

    #--- Start every connection, build the script.
    ZHOSTNAME="`hostname -s`"
    ZSTARTNEED=0
    for ZCONNID in ${ZSTARTCONNS}; do
        #--- Search the connection.
        if [ "${NMAPTAPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. Is running?.
            if [ ${NMAPTAPPLRUNN[${ZCONNID}]} -eq 0 ]; then
                #--- Inform.
                echo "echo \"starting tap conn #${ZCONNID}\"" >> "${ZSTARTCMD}"

                #--- Creates the error files.
                ZERRORFILE="`mktemp \"${CFGTEMPDIRECTORY}/cntaperr-XXXXXXXX\" 2> /dev/null`"
                touch "${ZERRORFILE}1"
                touch "${ZERRORFILE}2"

                #--- Check the interface to bind.
                ZTAPNAME=`printf "%s%04d" "${CFGINTFINTTAP}" "${ZCONNID}"`
                if [ -z "`ifconfig ${ZTAPNAME} 2> /dev/null`" ]; then
                    #--- Pre-up commands.
                    ZNUMFLD=1
                    ZTXTFLD=`echo "${NMAPTAPPRUPLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    while [ "${ZTXTFLD}" != "" ]; do
                        if [ -z  "`echo ${ZTXTFLD} | grep '>'`" ]; then
                            echo "${ZTXTFLD} > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                        else
                            echo "${ZTXTFLD} 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                        fi
                        echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                        ZNUMFLD=$(( ${ZNUMFLD} + 1 ))
                        ZTXTFLD=`echo "${NMAPTAPPRUPLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    done

                    #--- Creates the interface.
                    echo "tunctl -u \"${USER}\" -t \"${ZTAPNAME}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                    echo "ifconfig \"${ZTAPNAME}\" up > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                    echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                    echo "sleep 1" >> "${ZSTARTCMD}"

                    #--- Post-up commands.
                    ZNUMFLD=1
                    ZTXTFLD=`echo "${NMAPTAPPOUPLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    while [ "${ZTXTFLD}" != "" ]; do
                        if [ -z  "`echo ${ZTXTFLD} | grep '>'`" ]; then
                            echo "${ZTXTFLD} > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                        else
                            echo "${ZTXTFLD} 2> \"${ZERRORFILE}2\"" >> "${ZSTARTCMD}"
                        fi
                        echo "cat \"${ZERRORFILE}1\" >> \"${ZERRORFILE}\" ; cat \"${ZERRORFILE}2\" >> \"${ZERRORFILE}\"" >> "${ZSTARTCMD}"
                        ZNUMFLD=$(( ${ZNUMFLD} + 1 ))
                        ZTXTFLD=`echo "${NMAPTAPPOUPLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    done
                fi

                #--- Creates the netmap file for iou2net.
                ZNMAPNET="${CFGTEMPDIRECTORY}/cntapnmap-${ZCONNID}"
                echo "${NMAPTAPRTDEST[${ZCONNID}]}@${ZHOSTNAME} ${ZCONNID}:0/0@${ZHOSTNAME}" > "${ZNMAPNET}"

                #--- Start the connection: connect the iou2net device to the tap interface.
                echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -n \"${ZNMAPNET}\" -p ${NMAPTAPRTIDNT[${ZCONNID}]} -t \"${ZTAPNAME}\" > \"${ZERRORFILE}1\" 2> \"${ZERRORFILE}2\" &" >> "${ZSTARTCMD}"
                echo "sleep 2" >> "${ZSTARTCMD}"

                #--- Add the error files.
                ZARRERRFILES[${ZCONNID}]="${ZERRORFILE}"
                ZSTARTNEED=1
            else
                #--- Connection is already running.
                echo "echo \"skipping tap conn #${ZCONNID}\"" >> "${ZSTARTCMD}"
            fi
        fi
    done
    echo "popd >/dev/null 2> /dev/null" >> "${ZSTARTCMD}"

    #--- Command to get root privileges.
    ZROOTCMD="sudo"
    ZROOTUSR="${USER}"
    if [ ${CFGROOTSUDOSU} -ne 0 ]; then
        ZROOTCMD="su root -c"
        ZROOTUSR="root"
    fi

    #--- Execute the script.
    chmod 700 "${ZSTARTCMD}"
    if [ ${ZSTARTNEED} -ne 0 ]; then
        ln -s "/tmp/netio${UID}" "/tmp/netio0" > /dev/null 2> /dev/null
        echo "=> To start the tap connections, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTARTCMD}"
        rm -f "/tmp/netio0" > /dev/null 2> /dev/null
    else
        . "${ZSTARTCMD}"
    fi
    rm -f "${ZSTARTCMD}"

    #--- Recalculate the running connections.
    if [ ${ZSTARTNEED} -ne 0 ]; then
        make_sleep $(( 5 + ( `echo "${ZSTARTCONNS}" | wc -w` / 4 ) ))
        parseexttap "FORCE"

        #--- Check if all connections are started.
        ZHAVEERROR=0
        for ZCONNID in ${ZSTARTCONNS}; do
            #--- Search the connection.
            if [ "${NMAPTAPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
                #--- Connection found.
                if [ ${NMAPTAPPLRUNN[${ZCONNID}]} -eq 0 ]; then
                    #--- Connection is not running --> Error.
                    echo "--------------------------------------------------------"
                    echo "ERROR: tap connection #${ZCONNID} is not running."
                    echo ""
                    cat "${ZARRERRFILES[${ZCONNID}]}"
                    cat "${ZARRERRFILES[${ZCONNID}]}1"
                    cat "${ZARRERRFILES[${ZCONNID}]}2"
                    echo "--------------------------------------------------------"
                    echo ""
                    ZHAVEERROR=1
                fi
            fi
        done

        #--- Clean all error files.
        for ZCONNID in ${ZSTARTCONNS}; do
            if [ -f "${ZARRERRFILES[${ZCONNID}]}" ]; then
                rm -f "${ZARRERRFILES[${ZCONNID}]}"
                rm -f "${ZARRERRFILES[${ZCONNID}]}1"
                rm -f "${ZARRERRFILES[${ZCONNID}]}2"
            fi
        done

        #--- Inform no error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "tap connections started ok."
        else
            exit 98
        fi
    fi

    return
    }

#--- Start some external udp, tap, and interface connections.
function do_cnstart {
    #--- Params: $1 .. $2 .. $N: connections to start.
    local ZALLCONS
    local ZCONNID

    #--- Start connections.
    parseextudp
    parseextintf
    parseexttap
    loc_extudp_start "$@"
    loc_extintf_start "$@"
    loc_exttap_start "$@"

    #--- Check for unknown connections.
    ZALLCONS=" ${NMAPUDPRTIDNT[@]} ${NMAPINTFRTIDNT[@]} ${NMAPTAPRTIDNT[@]} "
    for ZCONNID in "$@"; do
        if [ -z "`echo \"${ZALLCONS}\" | grep \" ${ZCONNID} \"`" ]; then
            echo "unknown conn #${ZCONNID} ***"
        fi
    done

    return
    }

#--- Start all IOU devices and external connections.
function do_start {
    #--- Start all IOU devices.
    parseiourtsw
    do_rtstart ${NMAPIOURTACR[@]}

    #--- Start all external connections.
    parseextudp
    parseextintf
    parseexttap
    if [ $(( ${#NMAPUDPRTIDNT[@]} + ${#NMAPINTFRTIDNT[@]} + ${#NMAPTAPRTIDNT[@]} )) -gt 0 ]; then
        do_cnstart ${NMAPUDPRTIDNT[@]} ${NMAPINTFRTIDNT[@]} ${NMAPTAPRTIDNT[@]}
    fi

    return
    }

#--- Show some help for external udp connections.
function do_cnudphelp {
    local ZLOCALIP
    local ZRTINDEX

    #--- Parse the netmap file.
    echo "external udp connections:"
    parseextudp
    parsetelnet

    #--- Show a brief help for every connection.
    for ZRTINDEX in ${NMAPUDPRTIDNT[@]}; do
        #--- Get the IP running the connection.
        if [ ${NMAPTELNETREM} -ne 0 ]; then
            ZLOCALIP="${NMAPTELNETDIP}"
        else
            if [ "${NMAPUDPRTRDIP[${ZRTINDEX}]:0:4}" == "127." ]; then
                ZLOCALIP="127.0.0.1"
            else
                ZLOCALIP=`getlocalip`
            fi
        fi
        #--- Show the help of the connection.
        echo "  virtual device #${ZRTINDEX}:"
        echo "    for virtualbox:"
        echo "      vboxmanage modifyvm VMNAME|UUID --nicN generic"
        echo "        --nictypeN Am79C970A|Am79C973|82540EM|82543GC|82545EM|virtio"
        echo "        --cableconnectedN on --nicpropertyN dest=${ZLOCALIP}"
        echo "        --nicpropertyN dport=${NMAPUDPRTLPRT[${ZRTINDEX}]} --nicpropertyN sport=${NMAPUDPRTRPRT[${ZRTINDEX}]}"
        echo "        --nicgenericdrvN UDPTunnel --macaddressN auto|HHHHHHHHHHHH"
        echo ""
        echo "    for qemu:"
        echo "      -net nic,model=virtio|e1000,vlan=N,macaddr=HH:HH:HH:HH:HH:HH"
        echo "        -net socket,vlan=N,udp=${ZLOCALIP}:${NMAPUDPRTLPRT[${ZRTINDEX}]},localaddr=:${NMAPUDPRTRPRT[${ZRTINDEX}]}"
        echo ""
    done

    #--- No external udp connections.
    if [ ${#NMAPUDPRTIDNT[@]} -eq 0 ]; then
        echo "  no external udp connections."
    fi

    return
    }

#--- Show some help for external tap connections.
function do_cntaphelp {
    local ZLOCALIP
    local ZRTINDEX

    #--- Parse the netmap file.
    echo "external tap connections:"
    parseexttap
    parsetelnet

    #--- Show a brief help for every connection.
    for ZRTINDEX in ${NMAPTAPRTIDNT[@]}; do
        #--- Show the help of the connection.
        echo "  virtual device #${ZRTINDEX}:"
        echo "    for NETMAP:"
        echo "      #^   ID     post-up    brctl addbr BRIDGENAME0"
        echo "      #^   ID     post-up    brctl addif BRIDGENAME0 <TAP>"
        echo "      #^   ID     post-up    brctl stp BRIDGENAME0 off"
        echo "      #^   ID     post-up    ifconfig BRIDGENAME0 up"
        echo ""
        echo "    for virtualbox:"
        echo "      vboxmanage modifyvm VMNAME|UUID --nicN bridged"
        echo "        --nictypeN Am79C970A|Am79C973|82540EM|82543GC|82545EM|virtio"
        echo "        --cableconnectedN on --bridgeadapterN BRIDGENAME0"
        echo "        --macaddressN auto|HHHHHHHHHHHH"
        echo ""
        echo "    for qemu:"
        echo "      su root -c 'tunctl -t TAPNAME0 -u USERNAME ; ifconfig TAPNAME0 up ;"
        echo "        brctl addif BRIDGENAME0 TAPNAME0'"
        echo "      qemu-system-x86_64 ... -net nic,model=virtio|e1000,vlan=N,"
        echo "          macaddr=HH:HH:HH:HH:HH:HH"
        echo "        -net tap,vlan=N,ifname=TAPNAME0,script=no,downscript=no"
        echo ""
        echo "    for docker:"
        echo "      docker network create -d macvlan --gateway X.X.X.X"
        echo "        --subnet X.X.X.X/NN -o parent=BRIDGENAME0 DKNETNAME0"
        echo "      docker create ... -h HOSTNAME --ip X.X.X.X --name CONTNAME"
        echo "        --network DKNETNAME0 --entrypoint COMMAND IMAGE ARGS"
        echo "      docker network connect ... --ip X.X.X.X DKNETNAME0 CONTNAME"
        echo ""
    done

    #--- No external tap connections.
    if [ ${#NMAPTAPRTIDNT[@]} -eq 0 ]; then
        echo "  no external tap connections."
    fi

    return
    }

#--- Stop some IOU devices.
function do_rtstop {
    #--- Params: $1 .. $2 .. $N: devices to stop.
    local ZACRONYM
    local ZANYSTOP
    local ZFOUND
    local ZHAVEERROR
    local ZRTINDEX
    local ZSTOPDEVS

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing device(s) to stop."
        exit 48
    fi

    #--- Check if is a local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "iou devices are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate the duplicate devices.
    ZSTOPDEVS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Parse the netmap file.
    parseiourtsw

    #--- Stop every IOU device.
    ZANYSTOP=0
    for ZACRONYM in ${ZSTOPDEVS}; do
        #--- Search the device.
        ZFOUND=0
        for ZRTINDEX in ${NMAPIOURTIDX[@]}; do
            if [ "${NMAPIOURTACR[${ZRTINDEX}]}" == "${ZACRONYM}" ]; then
                #--- Device found. Is running?.
                ZFOUND=1
                if [ ${NMAPIOURTRUN[${ZRTINDEX}]} -ne 0 ]; then
                    #--- Check if is capturing traffic.
                    if [ ! -z "`find \"${CFGTEMPDIRECTORY}/\" -type f -name \"capturing-${NMAPIOURTIDX[${ZRTINDEX}]}.flag\" 2> /dev/null`" ]; then
                        #--- The device is capturing traffic. Skip the stop.
                        echo "device ${ZACRONYM} is capturing **"
                    else
                        #--- Stop the device.
                        echo "stopping ${ZACRONYM}"
                        kill "${NMAPIOURTWRP[${ZRTINDEX}]}" > /dev/null 2> /dev/null
                        sleep 0.5
                        kill "${NMAPIOURTRUN[${ZRTINDEX}]}" > /dev/null 2> /dev/null
                        sleep 0.5
                        kill -9 "${NMAPIOURTWRP[${ZRTINDEX}]}" > /dev/null 2> /dev/null
                        sleep 0.5
                        ZANYSTOP=1
                    fi
                else
                    #--- Device already stopped.
                    echo "skipping ${ZACRONYM}"
                fi
                break
            fi
        done
        if [ ${ZFOUND} -eq 0 ]; then
            echo "unknown ${ZACRONYM} ***"
        fi
    done

    #--- If any device has been stopped, check this.
    if [ ${ZANYSTOP} -ne 0 ]; then
        #--- Recalculate the running devices.
        make_sleep $(( 5 + ( $# / 4 ) ))
        parseiourtsw "FORCE"

        #---Check if all devices are stopped.
        ZHAVEERROR=0
        for ZACRONYM in ${ZSTOPDEVS}; do
            #--- Search the device.
            for ZRTINDEX in ${NMAPIOURTIDX[@]}; do
                if [ "${NMAPIOURTACR[${ZRTINDEX}]}" == "${ZACRONYM}" ]; then
                    #--- Device found.
                    if [ ${NMAPIOURTRUN[${ZRTINDEX}]} -ne 0 ] || [ ${NMAPIOURTWRP[${ZRTINDEX}]} -ne 0 ]; then
                        #--- Device is running --> Error.
                        echo "ERROR: ${ZACRONYM} is not stopped."
                        ZHAVEERROR=1
                    fi
                    break
                fi
            done
        done

        #--- Inform error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "devices stopped ok."
        else
            exit 49
        fi
    fi

    return
    }

#--- Stop some external udp connections.
function loc_extudp_stop {
    #--- Params: $1 .. $2 .. $N: connections to stop.
    local ZANYSTOP
    local ZCONNID
    local ZHAVEERROR
    local ZSTOPCONNS

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing connection(s) to stop."
        exit 50
    fi

    #--- Check if is a local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "udp connections are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate the duplicate connections.
    ZSTOPCONNS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Stop every connection.
    ZANYSTOP=0
    for ZCONNID in ${ZSTOPCONNS}; do
        #--- Search the connection.
        if [ "${NMAPUDPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. is running?.
            if [ ${NMAPUDPRTRUNN[${ZCONNID}]} -ne 0 ]; then
                #--- Check if is capturing traffic.
                if [ ! -z "`find \"${CFGTEMPDIRECTORY}/\" -type f -name \"capturing-${NMAPUDPRTIDNT[${ZCONNID}]}.flag\" 2> /dev/null`" ]; then
                    #--- The device is capturing traffic. Skip the stop.
                    echo "connection #${NMAPUDPRTIDNT[${ZCONNID}]} is capturing **"
                else
                    #--- Stop the connection.
                    echo "stopping udp conn #${ZCONNID}"
                    kill "${NMAPUDPRTRUNN[${ZCONNID}]}" > /dev/null 2> /dev/null
                    sleep 0.5
                    rm -f "${CFGTEMPDIRECTORY}/cnudpnmap-${ZCONNID}" > /dev/null 2> /dev/null
                    ZANYSTOP=1
                fi
            else
                #--- Connection already stopped.
                echo "skipping udp conn #${ZCONNID}"
            fi
        fi
    done

    #--- If any connection has been stopped, check this.
    if [ ${ZANYSTOP} -ne 0 ]; then
        #--- Recalculate the running connections.
        make_sleep $(( 5 + ( `echo "${ZSTOPCONNS}" | wc -w` / 4 ) ))
        parseextudp "FORCE"

        #---Check if all connections are stopped.
        ZHAVEERROR=0
        for ZCONNID in ${ZSTOPCONNS}; do
            #--- Search the connection.
            if [ "${NMAPUDPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
                #--- Connection found.
                if [ ${NMAPUDPRTRUNN[${ZCONNID}]} -ne 0 ]; then
                    #--- Connection is running --> Error.
                    echo "ERROR: udp conn ${ZCONNID} is not stopped."
                    ZHAVEERROR=1
                fi
            fi
        done

        #--- Inform error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "udp connections stopped ok."
        else
            exit 51
        fi
    fi

    return
    }

#--- Stop some external interface connections.
function loc_extintf_stop {
    #--- Params: $1 .. $2 .. $N: connections to stop.
    local ZCONNID
    local ZHAVEERROR
    local ZROOTCMD
    local ZROOTUSR
    local ZSTOPCONNS
    local ZSTOPNEED
    local ZSTOPSCR
    local ZTAPBRGNUM

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing connection to stop."
        exit 52
    fi

    #--- Check if is a local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "interface connections are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate the duplicate connections.
    ZSTOPCONNS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Creates the script of commands (for exec with root privileges).
    ZSTOPSCR="`mktemp \"${CFGTEMPDIRECTORY}/cnintfcmds-XXXXXXXX\" 2> /dev/null`"
    echo "#!/usr/bin/env bash" > "${ZSTOPSCR}"
    echo "PATH=\"${PATH}\"" >> "${ZSTOPSCR}"
    echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"

    #--- Build the script, stop every connection.
    ZSTOPNEED=0
    for ZCONNID in ${ZSTOPCONNS}; do
        #--- Search the connection.
        if [ "${NMAPINTFRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. Is running?.
            if [ ${NMAPINTFRTRUNN[${ZCONNID}]} -ne 0 ]; then
                #--- Check if is capturing traffic.
                if [ ! -z "`find \"${CFGTEMPDIRECTORY}/\" -type f -name \"capturing-${NMAPINTFRTIDNT[${ZCONNID}]}.flag\" 2> /dev/null`" ]; then
                    #--- The device is capturing traffic. Skip the stop.
                    echo "connection #${NMAPINTFRTIDNT[${ZCONNID}]} is capturing **"
                else
                    #--- Interface number.
                    ZTAPBRGNUM=`printf "%04d" "${ZCONNID}"`

                    #--- Stop the connection. Inform.
                    echo "echo \"stopping intf conn #${ZCONNID}\"" >> "${ZSTOPSCR}"

                    #--- Kill the iou2net.
                    echo "kill \"${NMAPINTFRTRUNN[${ZCONNID}]}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    echo "sleep 0.5" >> "${ZSTOPSCR}"
                    echo "rm -f \"${CFGTEMPDIRECTORY}/cnintfnmap-${ZCONNID}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"

                    #--- Deconfigure the bridge interface.
                    if [ ! -z "`ifconfig \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" 2> /dev/null`" ]; then
                        echo "ifconfig \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        echo "brctl delif \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        echo "brctl delif \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        echo "brctl delbr \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    fi

                    #--- Deconfigure the tap interface.
                    if [ ! -z "`ifconfig \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" 2> /dev/null`" ]; then
                        echo "ifconfig \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        echo "tunctl -d \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    fi

                    #--- Deconfigure the physical interface.
                    if [ ! -z "`ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" 2> /dev/null`" ]; then
                        #--- If not have an ip address configured...
                        if [ -z "`ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" 2> /dev/null | grep -E 'inet |inet6 '`" ]; then
                            echo "ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                            #--- If the OS is GNU/Linux and are a subinterface with vlan...
                            if [ "${CFGOPERSYSTEM}" == "LINUX" ] && [ ! -z "`echo ${NMAPINTFRTIFAZ[${ZCONNID}]} | grep '\.'`" ]; then
                                echo "vconfig rem \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                            fi
                        fi
                    fi

                    #--- Need root privileges to stop the connection.
                    ZSTOPNEED=1
                fi
            else
                #--- Device already stopped.
                echo "echo \"skipping intf conn #${ZCONNID}\"" >> "${ZSTOPSCR}"
            fi
        fi
    done
    echo "popd >/dev/null 2> /dev/null" >> "${ZSTOPSCR}"

    #--- Command to get root privileges.
    ZROOTCMD="sudo"
    ZROOTUSR="${USER}"
    if [ ${CFGROOTSUDOSU} -ne 0 ]; then
        ZROOTCMD="su root -c"
        ZROOTUSR="root"
    fi

    #--- Execute the script.
    chmod 700 "${ZSTOPSCR}"
    if [ ${ZSTOPNEED} -ne 0 ]; then
        echo "=> To stop the interface connections, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTOPSCR}"
    else
        . "${ZSTOPSCR}"
    fi
    rm -f "${ZSTOPSCR}"

    #--- If any connection has been stopped, check this.
    if [ ${ZSTOPNEED} -ne 0 ]; then
        #--- Recalculate the running connections.
        make_sleep $(( 5 + ( `echo "${ZSTOPCONNS}" | wc -w` / 4 ) ))
        parseextintf "FORCE"

        #---Check if all connections are stopped.
        ZHAVEERROR=0
        for ZCONNID in ${ZSTOPCONNS}; do
            #--- Search the connection.
            if [ "${NMAPINTFRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
                #--- Connection found.
                if [ ${NMAPINTFRTRUNN[${ZCONNID}]} -ne 0 ]; then
                    #--- Connection is running --> Error.
                    echo "ERROR: intf conn ${ZCONNID} is not stopped."
                    ZHAVEERROR=1
                fi
            fi
        done

        #--- Inform error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "intf connections stopped ok."
        else
            exit 53
        fi
    fi

    return
    }

#--- Stop some external tap connections.
function loc_exttap_stop {
    #--- Params: $1 .. $2 .. $N: connections to stop.
    local ZCONNID
    local ZHAVEERROR
    local ZNUMFLD
    local ZROOTCMD
    local ZROOTUSR
    local ZSTOPCONNS
    local ZSTOPNEED
    local ZSTOPSCR
    local ZTAPBRGNUM
    local ZTXTFLD

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing connection to stop."
        exit 99
    fi

    #--- Check if is a local console telnet.
    parsetelnet
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "interface connections are running in ${NMAPTELNETDIP}."
        return
    fi

    #--- Eliminate the duplicate connections.
    ZSTOPCONNS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Creates the script of commands (for exec with root privileges).
    ZSTOPSCR="`mktemp \"${CFGTEMPDIRECTORY}/cntapcmds-XXXXXXXX\" 2> /dev/null`"
    echo "#!/usr/bin/env bash" > "${ZSTOPSCR}"
    echo "PATH=\"${PATH}\"" >> "${ZSTOPSCR}"
    echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"

    #--- Build the script, stop every connection.
    ZSTOPNEED=0
    for ZCONNID in ${ZSTOPCONNS}; do
        #--- Search the connection.
        if [ "${NMAPTAPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. Is running?.
            if [ ${NMAPTAPPLRUNN[${ZCONNID}]} -ne 0 ]; then
                #--- Check if is capturing traffic.
                if [ ! -z "`find \"${CFGTEMPDIRECTORY}/\" -type f -name \"capturing-${NMAPTAPRTIDNT[${ZCONNID}]}.flag\" 2> /dev/null`" ]; then
                    #--- The device is capturing traffic. Skip the stop.
                    echo "connection #${NMAPTAPRTIDNT[${ZCONNID}]} is capturing **"
                else
                    #--- Interface number.
                    ZTAPBRGNUM=`printf "%04d" "${ZCONNID}"`

                    #--- Stop the connection. Inform.
                    echo "echo \"stopping intf conn #${ZCONNID}\"" >> "${ZSTOPSCR}"

                    #--- Kill the iou2net.
                    echo "kill \"${NMAPTAPPLRUNN[${ZCONNID}]}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    echo "sleep 0.5" >> "${ZSTOPSCR}"
                    echo "rm -f \"${CFGTEMPDIRECTORY}/cntapnmap-${ZCONNID}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"

                    #--- Pre-down commands.
                    ZNUMFLD=1
                    ZTXTFLD=`echo "${NMAPTAPPRDNLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    while [ "${ZTXTFLD}" != "" ]; do
                        if [ -z  "`echo ${ZTXTFLD} | grep '>'`" ]; then
                            echo "${ZTXTFLD} > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        else
                            echo "${ZTXTFLD} 2> /dev/null" >> "${ZSTOPSCR}"
                        fi
                        ZNUMFLD=$(( ${ZNUMFLD} + 1 ))
                        ZTXTFLD=`echo "${NMAPTAPPRDNLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    done

                    #--- Deconfigure the tap interface.
                    if [ ! -z "`ifconfig \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" 2> /dev/null`" ]; then
                        echo "ifconfig \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        echo "tunctl -d \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    fi

                    #--- Post-down commands.
                    ZNUMFLD=1
                    ZTXTFLD=`echo "${NMAPTAPPODNLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    while [ "${ZTXTFLD}" != "" ]; do
                        if [ -z  "`echo ${ZTXTFLD} | grep '>'`" ]; then
                            echo "${ZTXTFLD} > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                        else
                            echo "${ZTXTFLD} 2> /dev/null" >> "${ZSTOPSCR}"
                        fi
                        ZNUMFLD=$(( ${ZNUMFLD} + 1 ))
                        ZTXTFLD=`echo "${NMAPTAPPODNLST[${ZCONNID}]}" | cut -d "${CFGTAPSPRTR}" -f ${ZNUMFLD}`
                    done

                    #--- Need root privileges to stop the connection.
                    ZSTOPNEED=1
                fi
            else
                #--- Device already stopped.
                echo "echo \"skipping tap conn #${ZCONNID}\"" >> "${ZSTOPSCR}"
            fi
        fi
    done
    echo "popd >/dev/null 2> /dev/null" >> "${ZSTOPSCR}"

    #--- Command to get root privileges.
    ZROOTCMD="sudo"
    ZROOTUSR="${USER}"
    if [ ${CFGROOTSUDOSU} -ne 0 ]; then
        ZROOTCMD="su root -c"
        ZROOTUSR="root"
    fi

    #--- Execute the script.
    chmod 700 "${ZSTOPSCR}"
    if [ ${ZSTOPNEED} -ne 0 ]; then
        echo "=> To stop the tap connections, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTOPSCR}"
    else
        . "${ZSTOPSCR}"
    fi
    rm -f "${ZSTOPSCR}"

    #--- If any connection has been stopped, check this.
    if [ ${ZSTOPNEED} -ne 0 ]; then
        #--- Recalculate the running connections.
        make_sleep $(( 5 + ( `echo "${ZSTOPCONNS}" | wc -w` / 4 ) ))
        parseexttap "FORCE"

        #---Check if all connections are stopped.
        ZHAVEERROR=0
        for ZCONNID in ${ZSTOPCONNS}; do
            #--- Search the connection.
            if [ "${NMAPTAPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
                #--- Connection found.
                if [ ${NMAPTAPPLRUNN[${ZCONNID}]} -ne 0 ]; then
                    #--- Connection is running --> Error.
                    echo "ERROR: tap conn ${ZCONNID} is not stopped."
                    ZHAVEERROR=1
                fi
            fi
        done

        #--- Inform error.
        if [ ${ZHAVEERROR} -eq 0 ]; then
            echo "tap connections stopped ok."
        else
            exit 100
        fi
    fi

    return
    }

#--- Stop some external udp, tap, and interface connections.
function do_cnstop {
    #--- Params: $1 .. $2 .. $N: connections to stop.
    local ZALLCONS
    local ZCONNID

    #--- Stop the external connections.
    parseextintf
    parseexttap
    parseextudp
    loc_extintf_stop "$@"
    loc_exttap_stop "$@"
    loc_extudp_stop "$@"

    #--- Check for unknown connections.
    ZALLCONS=" ${NMAPUDPRTIDNT[@]} ${NMAPINTFRTIDNT[@]} ${NMAPTAPRTIDNT[@]} "
    for ZCONNID in "$@"; do
        if [ -z "`echo \"${ZALLCONS}\" | grep \" ${ZCONNID} \"`" ]; then
            echo "unknown conn #${ZCONNID} ***"
        fi
    done

    return
    }

#--- Stop all IOU routers and all external connections.
function do_stop {
    local ZSCREEN

    #--- Are you sure?.
    echo "==> WARNING: This command stop the lab."
    echo "==> WARNING: You must respond YES to stop the lab."
    read -p "==> WARNING: Are you sure? [YES/no]: " -t 15 ZANSWER
    if [ "${ZANSWER}" != "YES" ]; then
        echo ""
        echo "stopping cancelled by the user."
        return
    fi

    #--- Stop all connections.
    parseextudp
    parseextintf
    parseexttap
    if [ $(( ${#NMAPINTFRTIDNT[@]} + ${#NMAPUDPRTIDNT[@]} + ${#NMAPTAPRTIDNT[@]} )) -gt 0 ]; then
        do_cnstop ${NMAPINTFRTIDNT[@]} ${NMAPUDPRTIDNT[@]} ${NMAPTAPRTIDNT[@]}
    fi

    #--- Stop all IOU devices.
    parseiourtsw
    do_rtstop ${NMAPIOURTACR[@]}

    #--- Kill screen.
    sleep 1
    ZSCREEN=$(( `screen -list "${CFGSCREENNAME}" 2> /dev/null | grep "${CFGSCREENNAME}" | cut -d '.' -f 1` ))
    if [ ${ZSCREEN} -ne 0 ]; then
        kill ${ZSCREEN}
    fi

    return
    }

#--- Force deconfigure all tap, bridge and unused eth interfaces.
function do_cndeconfigure {
    local ZCONNID
    local ZROOTCMD
    local ZROOTUSR
    local ZSTOPNEED
    local ZSTOPSCR
    local ZTAPBRGNUM

    #--- Refuses if any external interface connection is running.
    parseextintf
    for ZCONNID in ${NMAPINTFRTIDNT[@]}; do
        if [ ${NMAPINTFRTRUNN[${ZCONNID}]} -ne 0 ]; then
            echo "ERROR: the intf conn #${ZCONNID} is running [PID: ${NMAPINTFRTRUNN[${ZCONNID}]}]."
            echo "       First stop it before deconfigure the interfaces."
            exit 101
        fi
    done

    #--- Refuses if any external tap connection is running.
    parseexttap
    for ZCONNID in ${NMAPTAPRTIDNT[@]}; do
        if [ ${NMAPTAPPLRUNN[${ZCONNID}]} -ne 0 ]; then
            echo "ERROR: the tap conn #${ZCONNID} is running [PID: ${NMAPTAPPLRUNN[${ZCONNID}]}]."
            echo "       First stop it before deconfigure the interfaces."
            exit 102
        fi
    done

    #--- Creates the script of commands (for exec with root privileges).
    ZSTOPSCR="`mktemp \"${CFGTEMPDIRECTORY}/cndeconf-XXXXXXXX\" 2> /dev/null`"
    echo "#!/usr/bin/env bash" > "${ZSTOPSCR}"
    echo "PATH=\"${PATH}\"" >> "${ZSTOPSCR}"
    echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"

    #--- Build the script, deconfigure all unused physical, tap and bridge interfaces.
    ZSTOPNEED=0
    for ZCONNID in ${NMAPINTFRTIDNT[@]}; do
        #--- Search the connection.
        if [ "${NMAPINTFRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. Interface number.
            ZTAPBRGNUM=`printf "%04d" "${ZCONNID}"`

            #--- Is the bridge interface configured?.
            if [ ! -z "`ifconfig \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" 2> /dev/null`" ]; then
                echo "echo \"deconfiguring ${CFGINTFEXTBRG}${ZTAPBRGNUM}\"" >> "${ZSTOPSCR}"
                echo "ifconfig \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                echo "brctl delif \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                echo "brctl delif \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                echo "brctl delbr \"${CFGINTFEXTBRG}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null " >> "${ZSTOPSCR}"
                ZSTOPNEED=1
            fi

            #--- Is the tap interface configured?.
            if [ ! -z "`ifconfig \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" 2> /dev/null`" ]; then
                echo "echo \"deconfiguring ${CFGINTFEXTTAP}${ZTAPBRGNUM}\"" >> "${ZSTOPSCR}"
                echo "ifconfig \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                echo "tunctl -d \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                ZSTOPNEED=1
            fi

            #--- Is the physical interface configured?.
            if [ ! -z "`ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" 2> /dev/null`" ]; then
                #--- If not have an IP address configured...
                if [ -z "`ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" 2> /dev/null | grep -E 'inet |inet6 '`" ]; then
                    echo "echo \"deconfiguring ${NMAPINTFRTIFAZ[${ZCONNID}]}\"" >> "${ZSTOPSCR}"
                    echo "ifconfig \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    #--- If the OS is GNU/Linux and are a subinterface with vlan...
                    if [ "${CFGOPERSYSTEM}" == "LINUX" ] && [ ! -z "`echo ${NMAPINTFRTIFAZ[${ZCONNID}]} | grep '\.'`" ]; then
                        echo "vconfig rem \"${NMAPINTFRTIFAZ[${ZCONNID}]}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                    fi
                    ZSTOPNEED=1
                fi
            fi
        fi
    done

    #--- Build the script, deconfigure all unused internal tap interfaces.
    for ZCONNID in ${NMAPTAPRTIDNT[@]}; do
        #--- Search the connection.
        if [ "${NMAPTAPRTIDNT[${ZCONNID}]}" == "${ZCONNID}" ]; then
            #--- Connection found. Interface number.
            ZTAPBRGNUM=`printf "%04d" "${ZCONNID}"`

            #--- Is the tap interface configured?.
            if [ ! -z "`ifconfig \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" 2> /dev/null`" ]; then
                echo "echo \"deconfiguring ${CFGINTFINTTAP}${ZTAPBRGNUM}\"" >> "${ZSTOPSCR}"
                echo "ifconfig \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" down > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                echo "tunctl -d \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null" >> "${ZSTOPSCR}"
                ZSTOPNEED=1
            fi
        fi
    done

    #--- End of the subscript.
    echo "popd >/dev/null 2> /dev/null" >> "${ZSTOPSCR}"

    #--- Command to get root privileges.
    ZROOTCMD="sudo"
    ZROOTUSR="${USER}"
    if [ ${CFGROOTSUDOSU} -ne 0 ]; then
        ZROOTCMD="su root -c"
        ZROOTUSR="root"
    fi

    #--- Execute the script.
    chmod 700 "${ZSTOPSCR}"
    if [ ${ZSTOPNEED} -ne 0 ]; then
        echo "=> To deconfigure unused interfaces, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTOPSCR}"
    fi
    rm -f "${ZSTOPSCR}"

    #--- Show the remaining interfaces.
    echo "remaining interfaces enabled:"
    ifconfig -a | grep -E "^[a-zA-Z]|inet "

    return
    }

#--- Send one command to some devices.
function loc_send_command {
    #--- === Currently unused ===
    #--- Params:
    #---    $1: list of devices (acronym), space separated.
    #---    $2: command to send.
    local ZACRONYM
    local ZCOMMAND
    local ZDEVLIST
    local ZINDEX
    local ZWINNAME

    #--- Params.
    ZDEVLIST="${1}"
    ZCOMMAND="${2}"

    #--- Process every device.
    for ZACRONYM in ${ZDEVLIST}; do
        #--- Search the device.
        for ZINDEX in ${NMAPIOURTIDX[@]}; do
            if [ "${NMAPIOURTACR[${ZINDEX}]}" == "${ZACRONYM}" ]; then
                #--- Window name.
                ZWINNAME="${NMAPWINDOWNAM[${ZINDEX}]}"

                #--- Send the command.
                screen -S "${CFGSCREENNAME}" -p "${ZWINNAME}" -X stuff "${ZCOMMAND}"

                #--- Done.
                break
            fi
        done
    done
    return
    }

#--- Export the startup-config of some IOU devices.
function do_rtexport {
    #--- Params:
    #---    $1, $2, $N: acronym of the devices.
    local ZACRONYM
    local ZDATENAME
    local ZDEVEXPORT
    local ZFILENAME
    local ZFNVRAM
    local ZFOUND
    local ZINDEX

    #--- Check the devices.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing device(s) to export."
        exit 92
    fi

    #--- Date for the configurations filenames.
    ZDATENAME=`date "+%Y%m%d-%H%M%S"`

    #--- Eliminate the duplicate devices.
    ZDEVEXPORT=`echo "${@}" | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Parse the netmap file.
    parseiourtsw

    #--- Export every IOU device configuration.
    for ZACRONYM in ${ZDEVEXPORT}; do
        #--- Search the device.
        ZFOUND=0
        for ZINDEX in ${NMAPIOURTIDX[@]}; do
            if [ "${NMAPIOURTACR[${ZINDEX}]}" == "${ZACRONYM}" ]; then
                #--- Device found.
                ZFOUND=1

                #--- Check the nvram file.
                ZFNVRAM=`printf "nvram_%05d" "${ZINDEX}"`
                if [ -f "${ZFNVRAM}" ]; then
                    #--- Export the startup config.
                    ZFILENAME="${CFGCONFIGPREFIX}-${ZACRONYM}-${ZDATENAME}.txt"
                    echo -n "exporting ${ZACRONYM} to ${ZFILENAME}: "
                    readnvram "${ZFNVRAM}" "${ZFILENAME}"
                    if [ -f "${ZFILENAME}" ]; then
                        echo "OK."
                    else
                        echo "ERROR."
                    fi
                else
                    #--- Nvram file not found.
                    echo "nvram of ${ZACRONYM} not found ***"
                fi

                #--- Done.
                break
            fi
        done
        if [ ${ZFOUND} -eq 0 ]; then
            echo "unknown or not IOU device: ${ZACRONYM} ***"
        fi
    done
    return
    }

#--- Export the startup-config of all IOU devices.
function do_export {
    parseiourtsw
    do_rtexport ${NMAPIOURTACR[@]}

    return
    }

#--- Import the startup config of one IOU device.
function do_rtimport {
    #--- Params:
    #---    $1: acronym of the device.
    #---    $2: filename with the startup config.
    local ZACRONYM
    local ZFNVRAM
    local ZFOUND
    local ZINDEX
    local ZSTARTFILE
    local ZTXTCONFIG

    #--- Get the params.
    ZACRONYM="$1"
    ZSTARTFILE="$2"

    #--- Check the params.
    if [ -z "${ZACRONYM}" ]; then
        echo "ERROR: missing device to import."
        exit 106
    fi
    if [ -z "${ZSTARTFILE}" ]; then
        echo "ERROR: missing filename to import."
        exit 107
    fi
    if [ ! -f "${ZSTARTFILE}" ]; then
        echo "ERROR: file ${ZSTARTFILE} not found."
        exit 108
    fi

    #--- Parse the netmap file.
    parseiourtsw

    #--- Search the acronym of the IOU device.
    ZFOUND=0
    for ZINDEX in ${NMAPIOURTIDX[@]}; do
        if [ "${NMAPIOURTACR[${ZINDEX}]}" == "${ZACRONYM}" ]; then
            ZFOUND=1
            break
        fi
    done

    #--- If the device is found, try to create the nvram.
    if [ ${ZFOUND} -ne 0 ]; then
        ZFNVRAM=`printf "nvram_%05d" ${ZINDEX}`
        #--- Check the nvram file.
        if [ -f "${ZFNVRAM}" ]; then
            echo "ERROR: the nvram ${ZFNVRAM} of ${ZACRONYM} exist."
            echo "ERROR: configuration not imported."
            echo "SOLUTION: manually delete it."
            exit 109
        fi

        #--- Creates the nvram.
        ZTXTCONFIG=`cat "${ZSTARTFILE}"`
        writenvram "${ZFNVRAM}" "${ZTXTCONFIG}" 1
        unset ZTXTCONFIG
    else
        #--- Unknown device.
        echo "ERROR: unknown or not IOU device: ${ZACRONYM}"
    fi

    return
    }

#--- Telnet to the console of some devices.
function do_rttelcons {
    #--- Params: $1, $2, $N: acronyms.
    local ZACRONYM
    local ZDEVID
    local ZDEVSCOMMD
    local ZDEVSINDEX
    local ZNUMTLNTS
    local ZPROCCESS
    local ZSCRNCREATED
    local ZTLNETDEVS

    #--- Check the params.
    if [ $# -eq 0 ]; then
        echo "ERROR: missing device(s) for console."
        exit 58
    fi

    #--- Parse the netmap file.
    calcwinnames
    parsetelnet
    parsecaptext
    parsealldevices

    #--- Check if screen is attached.
    if [ ! -z "`screen -list | grep \"${CFGSCREENNAME}\" | grep 'Attached'`" ]; then
        echo "ERROR: screen session is already attached."
        echo "SOLUTION: detach first with 'screen -d -r ${CFGSCREENNAME}'."
        exit 59
    fi

    #--- Eliminate the duplicate devices.
    ZTLNETDEVS=`echo ${@} | tr ' ' '\n' | sort -u | tr '\n' ' '`

    #--- Build an array with all devices and their commands.
    unset ZDEVSINDEX
    unset ZDEVSCOMMD
    for ZDEVID in ${NMAPIOURTIDX[@]}; do
        ZDEVSINDEX[${ZDEVID}]=${ZDEVID}
        ZDEVSCOMMD[${ZDEVID}]="telnet ${NMAPTELNETDIP} ${NMAPIOURTPRT[${ZDEVID}]}"
    done
    for ZDEVID in ${NMAPREALRTIDX[@]}; do
        ZDEVSINDEX[${ZDEVID}]=${ZDEVID}
        ZDEVSCOMMD[${ZDEVID}]="${NMAPREALRTCMD[${ZDEVID}]}"
    done

    #--- Check if screen is running.
    ZSCRNCREATED=0
    if [ -z "`screen -list 2> /dev/null | grep ${CFGSCREENNAME}`" ]; then
        #--- Screen is not running, creates a new instance.
        screen -d -m -S "${CFGSCREENNAME}" -t "shell"
        ZSCRNCREATED=1
    fi

    #--- Decorate the window.
    screen -X -S "${CFGSCREENNAME}" msgwait 0
    screen -X -S "${CFGSCREENNAME}" vbell off
    screen -X -S "${CFGSCREENNAME}" vbellwait 0
    screen -X -S "${CFGSCREENNAME}" scrollback 2000
    screen -X -S "${CFGSCREENNAME}" defscrollback 2000
    screen -X -S "${CFGSCREENNAME}" sorendition "${NMAPCAPTIONSRND}"
    screen -X -S "${CFGSCREENNAME}" caption "${NMAPCAPTIONMODE}" "${NMAPCAPTIONSCRN}"

    #--- Creates the windows sessions.
    ZPROCCESS=`ps ax -w -w | cleanline`
    for ZACRONYM in ${ZTLNETDEVS}; do
        #--- Search the device.
        for ZDEVID in ${ZDEVSINDEX[@]}; do
            if [ "${NMAPWINDOWACR[${ZDEVID}]}" == "${ZACRONYM}" ]; then
                #--- Device found. Check if the device not have a console session....
                if [ -z "`echo \"${ZPROCCESS}\" | grep -E \"${ZDEVSCOMMD[${ZDEVID}]}( |$)\"`" ]; then
                    #--- Creates the new window.
                    screen -X -S "${CFGSCREENNAME}" screen -t "${NMAPWINDOWNAM[${ZDEVID}]}" ${ZDEVSCOMMD[${ZDEVID}]}
                fi
                break
            fi
        done
    done

    #--- Kill the window #0 (created with the session).
    if [ ${ZSCRNCREATED} -ne 0 ]; then
        screen -X -S "${CFGSCREENNAME}" -p 0 select 0
        screen -X -S "${CFGSCREENNAME}" -p 0 kill
    fi

    #--- Reorder and sort all screen windows.
    sleep 1
    ZPROCCESS=`ps ax -w -w | cleanline`
    ZNUMTLNTS=0
    for ZDEVID in ${NMAPALLDEVSIDX[@]}; do
        #--- If the device have a telnet session....
        if [ ! -z "`echo \"${ZPROCCESS}\" | grep -E \"${ZDEVSCOMMD[${ZDEVID}]}( |$)\"`" ]; then
            #--- Renum the window.
            screen -X -S "${CFGSCREENNAME}" -p "${NMAPWINDOWNAM[${ZDEVID}]}" number "${ZNUMTLNTS}"
            ZNUMTLNTS=$(( ${ZNUMTLNTS} + 1 ))
        fi
    done

    #--- Attach to screen.
    screen -X -S "${CFGSCREENNAME}" msgwait 2
    screen -X -S "${CFGSCREENNAME}" -p 0 select 0
    screen -r "${CFGSCREENNAME}"

    return
    }

#--- Telnet to the console of all devices.
function do_telnetcons {
    #--- Console to all devices.
    parseiourtsw
    parserealdevs

    do_rttelcons ${NMAPIOURTACR[@]} ${NMAPREALRTACR[@]}

    return
    }

#--- Start capture traffic.
function do_captureon {
    #--- Params:
    #---    $1: Acronym of the IOU device.
    #---    $2: Slot/port of the IOU device.
    #---    $3: Encapsulation of the interface.
    #---    $4: Optional description of the capture.
    local ZCAPFILE
    local ZCONNIOU
    local ZDEVTEST
    local ZENCAPTYPE
    local ZENCNDX
    local ZFOUND
    local ZLFTCONNALL
    local ZLFTDEVID
    local ZMININET
    local ZNMAPNET
    local ZPARAMACRON
    local ZPARAMDESCR
    local ZPARAMSENCP
    local ZPARAMSLPRT
    local ZPIDFILE
    local ZPIDSNIFF
    local ZRGTCONNALL
    local ZRGTDEVID
    local ZROOTCMD
    local ZROOTUSR
    local ZSOCKETDIR
    local ZSTARTCMD
    local ZTAPBRGNUM
    local ZTEMP
    local ZTEMPDIR
    local ZTOOMANYPRM
    local ZTXTFORLST

    #--- Parse the netmap file.
    parseiourtsw
    parseintconns
    parseextudp
    parseextintf
    parseexttap
    parsetelnet

    #--- Get the params.
    ZPARAMACRON="${1}"
    ZPARAMSLPRT="${2}"
    ZPARAMSENCP="${3}"
    ZPARAMDESCR="${4}"
    ZTOOMANYPRM="${5}"

    #--- Check if is a remote lab.
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "ERROR: the lab is running in ${NMAPTELNETDIP}."
        exit 61
    fi

    #--- Check the params.
    if [ -z "${ZPARAMACRON}" ]; then
        echo "ERROR: missing device for capture."
        exit 62
    fi

    if [ -z "${ZPARAMSLPRT}" ]; then
        echo "ERROR: missing slot/port for capture."
        exit 63
    fi

    if [ -z "${ZPARAMSENCP}" ]; then
        echo "ERROR: missing encapsulation type for capture."
        exit 64
    fi

    if [ ! -z "${ZTOOMANYPRM}" ]; then
        echo "ERROR: too many parameters."
        exit 65
    fi

    #--- Check the encapsulation type.
    ZFOUND=0
    ZENCNDX=0
    while [ ${ZENCNDX} -lt ${#CFGPCAPENCAPSHORT[@]} ]; do
        if [ "${ZPARAMSENCP}" == "${CFGPCAPENCAPSHORT[${ZENCNDX}]}" ]; then
            ZFOUND=1
            break
        fi
        ZENCNDX=$(( ${ZENCNDX} + 1 ))
    done
    if [ ${ZFOUND} -eq 0 ]; then
        echo "ERROR: invalid encapsulation type."
        exit 66
    fi
    ZENCAPTYPE=${CFGPCAPENCAPVALUE[${ZENCNDX}]}

    #--- Check the device.
    ZFOUND=0
    for ZLFTDEVID in ${NMAPIOURTIDX[@]}; do
        if [ "${NMAPIOURTACR[${ZLFTDEVID}]}" == "${ZPARAMACRON}" ]; then
            ZFOUND=1
            break
        fi
    done

    if [ ${ZFOUND} -eq 0 ]; then
        echo "ERROR: unknown IOU device ${ZPARAMACRON}."
        exit 67
    fi

    #--- Check the slot / port.
    ZFOUND=0
    for ZCONNIOU in ${NMAPINTCONNNDX[@]}; do
        if [ ! -z "`echo \"${NMAPINTCONNDEF[${ZCONNIOU}]}\" | grep -E \"(^| )${ZLFTDEVID}:${ZPARAMSLPRT}( |$)\"`" ]; then
            ZFOUND=1
            break
        fi
    done

    if [ ${ZFOUND} -eq 0 ]; then
        echo "ERROR: unknown connection for ${ZPARAMACRON} ${ZPARAMSLPRT}."
        exit 68
    fi

    #--- Check if the connection is multipoint.
    if [ ${NMAPINTCONNCNT[${ZCONNIOU}]} -ne 2 ]; then
        echo "ERROR: can not capture traffic in port with multipoint connection."
        exit 69
    fi

    #--- Check if device is running.
    if [ ${NMAPIOURTRUN[${ZLFTDEVID}]} -eq 0 ]; then
        echo "ERROR: the device ${ZPARAMACRON} is not running."
        exit 70
    fi

    #--- Swap the connection.
    ZLFTCONNALL=`echo "${NMAPINTCONNDEF[${ZCONNIOU}]}" | cut -d ' ' -f 1`
    ZRGTCONNALL=`echo "${NMAPINTCONNDEF[${ZCONNIOU}]}" | cut -d ' ' -f 2`
    if [ "${ZLFTDEVID}:${ZPARAMSLPRT}" == "${ZRGTCONNALL}" ]; then
        ZTEMP="${ZLFTCONNALL}"
        ZLFTCONNALL="${ZRGTCONNALL}"
        ZRGTCONNALL="${ZTEMP}"
    fi

    #--- Get the destination device id.
    ZRGTDEVID=`echo "${ZRGTCONNALL}" | cut -d ':' -f 1`

    #--- Check if any device is already capturing traffic.
    for ZDEVTEST in ${ZLFTDEVID} ${ZRGTDEVID}; do
        if [ ! -z "`find \"${CFGTEMPDIRECTORY}/\" -type f -name \"capturing-${ZDEVTEST}.flag\" 2> /dev/null`" ]; then
            echo "ERROR: the device ${NMAPIOURTACR[${ZDEVTEST}]} is already capturing traffic."
            exit 71
        fi
    done

    #--- Normalize the description.
    ZPARAMDESCR=`echo "${ZPARAMDESCR}" | sed -e 's/[^a-zA-Z0-9]//g'`
    ZPARAMDESCR=${ZPARAMDESCR:0:24}

    #--- Creates the temporal directory.
    ZTEMPDIR="${CFGTEMPDIRECTORY}/capture-${ZLFTDEVID}"
    mkdir -p "${ZTEMPDIR}" > /dev/null 2> /dev/null

    #========== Check if is an internal IOU to IOU connection ==========
    if [ ! -z "`echo \" ${NMAPIOURTIDX[@]} \" | grep \" ${ZRGTDEVID} \"`" ]; then
        #--- Check if the other IOU device is running.
        if [ ${NMAPIOURTRUN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the device ${NMAPIOURTACR[${ZRGTDEVID}]} is not running."
            exit 72
        fi

        #--- Move the sockets of both IOU devices.
        ZSOCKETDIR="/tmp/netio${UID}"

        mv -f "${ZSOCKETDIR}/${ZLFTDEVID}" "${ZTEMPDIR}/${ZLFTDEVID}"
        mv -f "${ZSOCKETDIR}/${ZRGTDEVID}" "${ZTEMPDIR}/${ZRGTDEVID}"

        ln -s "${ZTEMPDIR}/${ZLFTDEVID}" "${ZSOCKETDIR}/${ZLFTDEVID}"
        ln -s "${ZTEMPDIR}/${ZRGTDEVID}" "${ZSOCKETDIR}/${ZRGTDEVID}"

        ln -s "${ZSOCKETDIR}/${ZLFTDEVID}.lck" "${ZTEMPDIR}/${ZLFTDEVID}.lck"
        ln -s "${ZSOCKETDIR}/${ZRGTDEVID}.lck" "${ZTEMPDIR}/${ZRGTDEVID}.lck"

        #--- Generate the netmap file for iousniff.
        ZMININET="${ZTEMPDIR}/capturing-${ZLFTDEVID}.nmap"
        echo -n "${NMAPINTCONNDEF[${ZCONNIOU}]} ${ZENCAPTYPE}" > "${ZMININET}"

        #--- Start the capture.
        "${CFGSCRIPTDIR}/${CFGIOUSNIFF}" -o -f -i "${ZTEMPDIR}" -n "${ZMININET}" -s "${ZTEMPDIR}" > /dev/null 2> /dev/null &
        ZPIDSNIFF=$!
        sleep 3

        #--- Check if is running.
        if [ -z "`ps ax -w -w | grep -v 'grep' | grep \"${ZPIDSNIFF}\" | grep \"${CFGSCRIPTDIR}/${CFGIOUSNIFF}\" | grep -- \"-n ${ZMININET}\"`" ]; then
            echo "ERROR: the capture has failed to start."
            exit 73
        fi

        #--- Set the flag of capturing traffic.
        touch "${ZTEMPDIR}/capturing-${ZLFTDEVID}.flag"
        touch "${ZTEMPDIR}/capturing-${ZRGTDEVID}.flag"

        #--- Set the info of the capture.
        #--- Format: Capture Type | Daemon PID | Second IOU Dev ID | Description of capture | Text for list.
        if [ -z "${ZPARAMDESCR}" ]; then
            ZTXTFORLST=""
        else
            ZTXTFORLST=" - ${ZPARAMDESCR}"
        fi
        ZTXTFORLST="  ${NMAPIOURTACR[${ZLFTDEVID}]} `echo \"${ZLFTCONNALL}\" | cut -d ':' -f 2` <--> ${NMAPIOURTACR[${ZRGTDEVID}]} `echo \"${ZRGTCONNALL}\" | cut -d ':' -f 2` [${ZPARAMSENCP}]${ZTXTFORLST}"

        echo "0|${ZPIDSNIFF}|${ZRGTDEVID}|${ZPARAMDESCR}|${ZTXTFORLST}" > "${ZTEMPDIR}/capturing-${ZLFTDEVID}.info"

    #========== Check if is an external udp connection ==========
    elif [ ! -z "`echo \" ${NMAPUDPRTIDNT[@]} \" | grep \" ${ZRGTDEVID} \"`" ]; then
        #--- Check if the connection is running.
        if [ ${NMAPUDPRTRUNN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the connection #${NMAPUDPRTIDNT[${ZRGTDEVID}]} is not running."
            exit 74
        fi

        #--- Files to use.
        ZCAPFILE="${ZTEMPDIR}/`echo ${ZLFTCONNALL} | tr ':' '-' | tr '/' '.'`-`date '+%s'`.pcap"
        ZNMAPNET="${CFGTEMPDIRECTORY}/cnudpnmap-${ZRGTDEVID}"

        #--- Kill the original connection.
        kill "${NMAPUDPRTRUNN[${ZRGTDEVID}]}" > /dev/null 2> /dev/null
        sleep 1

        #--- Creates the new connection, start the capture.
        "${CFGSCRIPTDIR}/${CFGIOU2NET}" -f "${ZCAPFILE}" -n "${ZNMAPNET}" -p "${NMAPUDPRTIDNT[${ZRGTDEVID}]}" -u "${NMAPUDPRTLPRT[${ZRGTDEVID}]}:${NMAPUDPRTRDIP[${ZRGTDEVID}]}:${NMAPUDPRTRPRT[${ZRGTDEVID}]}" > /dev/null 2> /dev/null &
        ZPIDSNIFF=$!
        sleep 3

        #--- Check if is running.
        parseextudp "FORCE"
        if [ ${NMAPUDPRTRUNN[${ZRGTDEVID}]} -eq 0 ] || [ ${NMAPUDPRTRUNN[${ZRGTDEVID}]} -ne ${ZPIDSNIFF} ]; then
            echo "ERROR: the capture has failed to start, check the conn status."
            exit 75
        fi

        #--- Set the flag of capturing traffic.
        touch "${ZTEMPDIR}/capturing-${ZLFTDEVID}.flag"
        touch "${ZTEMPDIR}/capturing-${ZRGTDEVID}.flag"

        #--- Set the info of the capture.
        #--- Format: Capture Type | Daemon PID | Second IOU Dev ID | Description of capture | Text for list
        if [ -z "${ZPARAMDESCR}" ]; then
            ZTXTFORLST=""
        else
            ZTXTFORLST=" - ${ZPARAMDESCR}"
        fi
        ZTXTFORLST="  ${NMAPIOURTACR[${ZLFTDEVID}]} `echo \"${ZLFTCONNALL}\" | cut -d ':' -f 2` <--> udp ${NMAPUDPRTLPRT[${ZRGTDEVID}]}:${NMAPUDPRTRDIP[${ZRGTDEVID}]}:${NMAPUDPRTRPRT[${ZRGTDEVID}]} [${ZPARAMSENCP}]${ZTXTFORLST}"
        echo "1|${ZPIDSNIFF}|${ZRGTDEVID}|${ZPARAMDESCR}|${ZTXTFORLST}" > "${ZTEMPDIR}/capturing-${ZLFTDEVID}.info"

    #========== Check if is an external interface connection ==========
    elif [ ! -z "`echo \" ${NMAPINTFRTIDNT[@]} \" | grep \" ${ZRGTDEVID} \"`" ]; then
        #--- Check if the connection is running.
        if [ ${NMAPINTFRTRUNN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the connection #${NMAPINTFRTIDNT[${ZRGTDEVID}]} is not running."
            exit 76
        fi

        #--- Files to use.
        ZCAPFILE="${ZTEMPDIR}/`echo ${ZLFTCONNALL} | tr ':' '-' | tr '/' '.'`-`date '+%s'`.pcap"
        ZNMAPNET="${CFGTEMPDIRECTORY}/cnintfnmap-${ZRGTDEVID}"
        ZPIDFILE="${ZTEMPDIR}/capturing-${ZRGTDEVID}.dpid"

        #--- Interface number.
        ZTAPBRGNUM=`printf "%04d" "${NMAPINTFRTIDNT[${ZRGTDEVID}]}"`

        #--- Creates the script of commands (for exec with root privileges).
        ZSTARTCMD="`mktemp \"${CFGTEMPDIRECTORY}/cnintfcmds-XXXXXXXX\" 2> /dev/null`"
        echo "#!/usr/bin/env bash" > "${ZSTARTCMD}"
        echo "PATH=\"${PATH}\"" >> "${ZSTARTCMD}"
        echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Kill the original connection.
        echo "kill \"${NMAPINTFRTRUNN[${ZRGTDEVID}]}\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"
        echo "sleep 1" >> "${ZSTARTCMD}"

        #--- If the interface have an ip address, can't use bridge. If the OS not is GNU/Linux, use interface mode.
        if [ ! -z "`ifconfig ${NMAPINTFRTIFAZ[${ZRGTDEVID}]} 2> /dev/null | grep -E 'inet |inet6 '`" ] || [ "${CFGOPERSYSTEM}" != "LINUX" ]; then
            #--- Start the connection: connect the iou2net device directly to the interface.
            echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -f \"${ZCAPFILE}\" -n \"${ZNMAPNET}\" -p ${NMAPINTFRTIDNT[${ZRGTDEVID}]} -i \"${NMAPINTFRTIFAZ[${ZRGTDEVID}]}\" > /dev/null 2> /dev/null &" >> "${ZSTARTCMD}"
        else
            #--- Start the connection: connect the iou2net device using the tap / bridge mode.
            echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -f \"${ZCAPFILE}\" -n \"${ZNMAPNET}\" -p ${NMAPINTFRTIDNT[${ZRGTDEVID}]} -t \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null &" >> "${ZSTARTCMD}"
        fi

        #--- Get new PID and change owner.
        echo "ZPIDSNIFF=\$!" >> "${ZSTARTCMD}"
        echo "echo \${ZPIDSNIFF} > \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "sleep 3" >> "${ZSTARTCMD}"
        echo "chown ${UID} \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "touch \"${ZCAPFILE}\"" >> "${ZSTARTCMD}"
        echo "chown ${UID} \"${ZCAPFILE}\"" >> "${ZSTARTCMD}"

        #--- Finish the subscript.
        echo "popd >/dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Command to get root privileges.
        ZROOTCMD="sudo"
        ZROOTUSR="${USER}"
        if [ ${CFGROOTSUDOSU} -ne 0 ]; then
            ZROOTCMD="su root -c"
            ZROOTUSR="root"
        fi

        #--- Execute the script.
        chmod 700 "${ZSTARTCMD}"
        ln -s "/tmp/netio${UID}" "/tmp/netio0" > /dev/null 2> /dev/null
        echo "=> To start the capture, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTARTCMD}"
        rm -f "/tmp/netio0" > /dev/null 2> /dev/null
        rm -f "${ZSTARTCMD}"

        #--- Check if is running.
        parseextintf "FORCE"
        ZPIDSNIFF=`cat "${ZPIDFILE}" 2> /dev/null`
        rm -f "${ZPIDFILE}"
        if [ ${NMAPINTFRTRUNN[${ZRGTDEVID}]} -eq 0 ] || [ "${NMAPINTFRTRUNN[${ZRGTDEVID}]}" != "${ZPIDSNIFF}" ]; then
            echo "ERROR: the capture has failed to start, check the conn status."
            exit 77
        fi

        #--- Set the flag of capturing traffic.
        touch "${ZTEMPDIR}/capturing-${ZLFTDEVID}.flag"
        touch "${ZTEMPDIR}/capturing-${ZRGTDEVID}.flag"

        #--- Set the info of the capture.
        #--- Format: Capture Type | Daemon PID | Second IOU Dev ID | Description of capture | Text for list
        if [ -z "${ZPARAMDESCR}" ]; then
            ZTXTFORLST=""
        else
            ZTXTFORLST=" - ${ZPARAMDESCR}"
        fi
        ZTXTFORLST="  ${NMAPIOURTACR[${ZLFTDEVID}]} `echo \"${ZLFTCONNALL}\" | cut -d ':' -f 2` <--> ${NMAPINTFRTIFAZ[${ZRGTDEVID}]} [${ZPARAMSENCP}]${ZTXTFORLST}"
        echo "2|${ZPIDSNIFF}|${ZRGTDEVID}|${ZPARAMDESCR}|${ZTXTFORLST}" > "${ZTEMPDIR}/capturing-${ZLFTDEVID}.info"

    #========== Check if is an external tap connection ==========
    elif [ ! -z "`echo \" ${NMAPTAPRTIDNT[@]} \" | grep \" ${ZRGTDEVID} \"`" ]; then
        #--- Check if the connection is running.
        if [ ${NMAPTAPPLRUNN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the connection #${NMAPTAPRTIDNT[${ZRGTDEVID}]} is not running."
            exit 76
        fi

        #--- Files to use.
        ZCAPFILE="${ZTEMPDIR}/`echo ${ZLFTCONNALL} | tr ':' '-' | tr '/' '.'`-`date '+%s'`.pcap"
        ZNMAPNET="${CFGTEMPDIRECTORY}/cntapnmap-${ZRGTDEVID}"
        ZPIDFILE="${ZTEMPDIR}/capturing-${ZRGTDEVID}.dpid"

        #--- Interface number.
        ZTAPBRGNUM=`printf "%04d" "${NMAPTAPRTIDNT[${ZRGTDEVID}]}"`

        #--- Creates the script of commands (for exec with root privileges).
        ZSTARTCMD="`mktemp \"${CFGTEMPDIRECTORY}/cntapcmds-XXXXXXXX\" 2> /dev/null`"
        echo "#!/usr/bin/env bash" > "${ZSTARTCMD}"
        echo "PATH=\"${PATH}\"" >> "${ZSTARTCMD}"
        echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Kill the original connection.
        echo "kill \"${NMAPTAPPLRUNN[${ZRGTDEVID}]}\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"
        echo "sleep 1" >> "${ZSTARTCMD}"

        #--- Start the connection: connect the iou2net device using the tap / bridge mode.
        echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -f \"${ZCAPFILE}\" -n \"${ZNMAPNET}\" -p ${NMAPTAPRTIDNT[${ZRGTDEVID}]} -t \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null &" >> "${ZSTARTCMD}"

        #--- Get new PID and change owner.
        echo "ZPIDSNIFF=\$!" >> "${ZSTARTCMD}"
        echo "echo \${ZPIDSNIFF} > \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "sleep 3" >> "${ZSTARTCMD}"
        echo "chown ${UID} \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "touch \"${ZCAPFILE}\"" >> "${ZSTARTCMD}"
        echo "chown ${UID} \"${ZCAPFILE}\"" >> "${ZSTARTCMD}"

        #--- Finish the subscript.
        echo "popd >/dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Command to get root privileges.
        ZROOTCMD="sudo"
        ZROOTUSR="${USER}"
        if [ ${CFGROOTSUDOSU} -ne 0 ]; then
            ZROOTCMD="su root -c"
            ZROOTUSR="root"
        fi

        #--- Execute the script.
        chmod 700 "${ZSTARTCMD}"
        ln -s "/tmp/netio${UID}" "/tmp/netio0" > /dev/null 2> /dev/null
        echo "=> To start the capture, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTARTCMD}"
        rm -f "/tmp/netio0" > /dev/null 2> /dev/null
        rm -f "${ZSTARTCMD}"

        #--- Check if is running.
        parseexttap "FORCE"
        ZPIDSNIFF=`cat "${ZPIDFILE}" 2> /dev/null`
        rm -f "${ZPIDFILE}"
        if [ ${NMAPTAPPLRUNN[${ZRGTDEVID}]} -eq 0 ] || [ "${NMAPTAPPLRUNN[${ZRGTDEVID}]}" != "${ZPIDSNIFF}" ]; then
            echo "ERROR: the capture has failed to start, check the conn status."
            exit 103
        fi

        #--- Set the flag of capturing traffic.
        touch "${ZTEMPDIR}/capturing-${ZLFTDEVID}.flag"
        touch "${ZTEMPDIR}/capturing-${ZRGTDEVID}.flag"

        #--- Set the info of the capture.
        #--- Format: Capture Type | Daemon PID | Second IOU Dev ID | Description of capture | Text for list
        if [ -z "${ZPARAMDESCR}" ]; then
            ZTXTFORLST=""
        else
            ZTXTFORLST=" - ${ZPARAMDESCR}"
        fi
        ZTXTFORLST="  ${NMAPIOURTACR[${ZLFTDEVID}]} `echo \"${ZLFTCONNALL}\" | cut -d ':' -f 2` <--> ${CFGINTFINTTAP}`printf \"%04d\" ${ZRGTDEVID}` [${ZPARAMSENCP}]${ZTXTFORLST}"
        echo "3|${ZPIDSNIFF}|${ZRGTDEVID}|${ZPARAMDESCR}|${ZTXTFORLST}" > "${ZTEMPDIR}/capturing-${ZLFTDEVID}.info"

    #========== Unknown device ==========
    else
        echo "ERROR: unknown device id #${ZRGTDEVID}."
        exit 78
    fi

    #--- Inform.
    echo "capture in ${ZPARAMACRON} ${ZPARAMSLPRT} started."

    return
    }

#--- Stop capture traffic.
function do_captureoff {
    #--- Params:
    #---    $1: Acronym of the IOU device.
    local ZCONNTYPE
    local ZDATECAP
    local ZDESCRIP
    local ZERROR
    local ZFINALCAP
    local ZFOUND
    local ZINFOALL
    local ZINFOFILE
    local ZLFTDEVID
    local ZNMAPNET
    local ZPARAMACRON
    local ZPCAPBASE
    local ZPCAPFILE
    local ZPIDDAEMON
    local ZPIDFILE
    local ZPIDSNIFF
    local ZRGTDEVID
    local ZROOTCMD
    local ZROOTUSR
    local ZSLTPRT
    local ZSOCKETDIR
    local ZSTARTCMD
    local ZTAPBRGNUM
    local ZTEMPDIR
    local ZTOOMANYPRM

    #--- Parse the netmap file.
    parseiourtsw
    parseintconns
    parseextudp
    parseextintf
    parseexttap
    parsetelnet

    #--- Get the params.
    ZPARAMACRON="${1}"
    ZTOOMANYPRM="${2}"

    #--- Check if is a remote lab.
    if [ ${NMAPTELNETREM} -ne 0 ]; then
        echo "ERROR: the lab is running in ${NMAPTELNETDIP}."
        exit 79
    fi

    #--- Check the params.
    if [ -z "${ZPARAMACRON}" ]; then
        echo "ERROR: missing device for stop capture."
        exit 80
    fi

    if [ ! -z "${ZTOOMANYPRM}" ]; then
        echo "ERROR: too many parameters."
        exit 81
    fi

    #--- Check the device.
    ZFOUND=0
    for ZLFTDEVID in ${NMAPIOURTIDX[@]}; do
        if [ "${NMAPIOURTACR[${ZLFTDEVID}]}" == "${ZPARAMACRON}" ]; then
            ZFOUND=1
            break
        fi
    done

    if [ ${ZFOUND} -eq 0 ]; then
        echo "ERROR: unknown IOU device ${ZPARAMACRON}."
        exit 82
    fi

    #--- Check if the IOU device is running.
    if [ ${NMAPIOURTRUN[${ZLFTDEVID}]} -eq 0 ]; then
        echo "ERROR: the device ${NMAPIOURTACR[${ZLFTDEVID}]} is not running."
        exit 83
    fi

    #--- Temp and sockets directories.
    ZTEMPDIR="${CFGTEMPDIRECTORY}/capture-${ZLFTDEVID}"
    ZSOCKETDIR="/tmp/netio${UID}"

    #--- Check the device is capturing traffic.
    ZINFOFILE="${ZTEMPDIR}/capturing-${ZLFTDEVID}.info"
    if [ ! -f "${ZINFOFILE}" ]; then
        echo "ERROR: the device ${ZPARAMACRON} is not capturing traffic."
        exit 84
    fi

    #--- Get the common connection info.
    #--- Format: Capture Type | Daemon PID | Second IOU Dev ID | Description of capture | Text for list.
    ZINFOALL=`cat "${ZINFOFILE}" 2> /dev/null`
    ZCONNTYPE=`echo "${ZINFOALL}" | cut -d '|' -f 1`
    ZPIDDAEMON=`echo "${ZINFOALL}" | cut -d '|' -f 2`
    ZRGTDEVID=`echo "${ZINFOALL}" | cut -d '|' -f 3`
    ZDESCRIP=`echo "${ZINFOALL}" | cut -d '|' -f 4`

    #========== Connection type IOU to IOU ==========
    if [ ${ZCONNTYPE} -eq 0 ]; then
        #--- Check if the other IOU device is running.
        if [ ${NMAPIOURTRUN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the device ${NMAPIOURTACR[${ZRGTDEVID}]} is not running."
            exit 85
        fi

        #--- Kill iousniff.
        kill "${ZPIDDAEMON}" > /dev/null 2> /dev/null
        sleep 1

        #--- Normalize the IOU sockets.
        rm -f "${ZTEMPDIR}/${ZLFTDEVID}.lck" > /dev/null 2> /dev/null
        rm -f "${ZTEMPDIR}/${ZRGTDEVID}.lck" > /dev/null 2> /dev/null

        rm -f "${ZSOCKETDIR}/${ZLFTDEVID}" > /dev/null 2> /dev/null
        rm -f "${ZSOCKETDIR}/${ZRGTDEVID}" > /dev/null 2> /dev/null

        mv -f "${ZTEMPDIR}/${ZLFTDEVID}" "${ZSOCKETDIR}/${ZLFTDEVID}"
        mv -f "${ZTEMPDIR}/${ZRGTDEVID}" "${ZSOCKETDIR}/${ZRGTDEVID}"

    #========== Connection type IOU to udp external ==========
    elif [ ${ZCONNTYPE} -eq 1 ]; then
        #--- Check if the connection is running.
        if [ ${NMAPUDPRTRUNN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the connection #${NMAPUDPRTIDNT[${ZRGTDEVID}]} is not running."
            exit 86
        fi

        #--- Files to use.
        ZNMAPNET="${CFGTEMPDIRECTORY}/cnudpnmap-${ZRGTDEVID}"

        #--- Kill the capture connection.
        kill "${NMAPUDPRTRUNN[${ZRGTDEVID}]}" > /dev/null 2> /dev/null
        sleep 1

        #--- Creates the new connection without capture.
        "${CFGSCRIPTDIR}/${CFGIOU2NET}" -n "${ZNMAPNET}" -p "${NMAPUDPRTIDNT[${ZRGTDEVID}]}" -u "${NMAPUDPRTLPRT[${ZRGTDEVID}]}:${NMAPUDPRTRDIP[${ZRGTDEVID}]}:${NMAPUDPRTRPRT[${ZRGTDEVID}]}" > /dev/null 2> /dev/null &
        ZPIDSNIFF=$!
        sleep 3

        #--- Check if is running.
        parseextudp "FORCE"
        if [ ${NMAPUDPRTRUNN[${ZRGTDEVID}]} -eq 0 ] || [ ${NMAPUDPRTRUNN[${ZRGTDEVID}]} -ne ${ZPIDSNIFF} ]; then
            echo "ERROR: the capture has failed to stop, check the conn status."
            exit 87
        fi

    #========== Connection type IOU to interface external ==========
    elif [ ${ZCONNTYPE} -eq 2 ]; then
        #--- Check if the connection is running.
        if [ ${NMAPINTFRTRUNN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the connection #${NMAPINTFRTIDNT[${ZRGTDEVID}]} is not running."
            exit 88
        fi

        #--- Files to use.
        ZNMAPNET="${CFGTEMPDIRECTORY}/cnintfnmap-${ZRGTDEVID}"
        ZPIDFILE="${ZTEMPDIR}/capturing-${ZRGTDEVID}.dpid"

        #--- Interface number.
        ZTAPBRGNUM=`printf "%04d" "${NMAPINTFRTIDNT[${ZRGTDEVID}]}"`

        #--- Creates the script of commands (for exec with root privileges).
        ZSTARTCMD="`mktemp \"${CFGTEMPDIRECTORY}/cnintfcmds-XXXXXXXX\" 2> /dev/null`"
        echo "#!/usr/bin/env bash" > "${ZSTARTCMD}"
        echo "PATH=\"${PATH}\"" >> "${ZSTARTCMD}"
        echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Kill the original connection.
        echo "kill \"${NMAPINTFRTRUNN[${ZRGTDEVID}]}\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"
        echo "sleep 1" >> "${ZSTARTCMD}"

        #--- If the interface have an ip address, can't use bridge. If the OS not is Linux, use interface mode.
        if [ ! -z "`ifconfig ${NMAPINTFRTIFAZ[${ZRGTDEVID}]} 2> /dev/null | grep -E 'inet |inet6 '`" ] || [ "${CFGOPERSYSTEM}" != "LINUX" ]; then
            #--- Start the connection: connect the iou2net device directly to the interface.
            echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -n \"${ZNMAPNET}\" -p ${NMAPINTFRTIDNT[${ZRGTDEVID}]} -i \"${NMAPINTFRTIFAZ[${ZRGTDEVID}]}\" > /dev/null 2> /dev/null &" >> "${ZSTARTCMD}"
        else
            #--- Start the connection: connect the iou2net device using the tap / bridge mode.
            echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -n \"${ZNMAPNET}\" -p ${NMAPINTFRTIDNT[${ZRGTDEVID}]} -t \"${CFGINTFEXTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null &" >> "${ZSTARTCMD}"
        fi
        echo "ZPIDSNIFF=\$!" >> "${ZSTARTCMD}"
        echo "echo \$ZPIDSNIFF >> \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "chown ${UID} \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "sleep 3" >> "${ZSTARTCMD}"

        #--- Finish the subscript.
        echo "popd >/dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Command to get root privileges.
        ZROOTCMD="sudo"
        ZROOTUSR="${USER}"
        if [ ${CFGROOTSUDOSU} -ne 0 ]; then
            ZROOTCMD="su root -c"
            ZROOTUSR="root"
        fi

        #--- Execute the script.
        chmod 700 "${ZSTARTCMD}"
        ln -s "/tmp/netio${UID}" "/tmp/netio0" > /dev/null 2> /dev/null
        echo "=> To stop the capture, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTARTCMD}"
        rm -f "/tmp/netio0" > /dev/null 2> /dev/null
        rm -f "${ZSTARTCMD}"

        #--- Check if is running.
        parseextintf "FORCE"
        ZPIDSNIFF=`cat "${ZPIDFILE}" 2> /dev/null`
        rm -f "${ZPIDFILE}"
        if [ ${NMAPINTFRTRUNN[${ZRGTDEVID}]} -eq 0 ] || [ "${NMAPINTFRTRUNN[${ZRGTDEVID}]}" != "${ZPIDSNIFF}" ]; then
            echo "ERROR: the capture has failed to stop, check the conn status."
            exit 89
        fi

    #========== Connection type IOU to tap interface ==========
    elif [ ${ZCONNTYPE} -eq 3 ]; then
        #--- Check if the connection is running.
        if [ ${NMAPTAPPLRUNN[${ZRGTDEVID}]} -eq 0 ]; then
            echo "ERROR: the connection #${NMAPTAPRTIDNT[${ZRGTDEVID}]} is not running."
            exit 104
        fi

        #--- Files to use.
        ZNMAPNET="${CFGTEMPDIRECTORY}/cntapnmap-${ZRGTDEVID}"
        ZPIDFILE="${ZTEMPDIR}/capturing-${ZRGTDEVID}.dpid"

        #--- Interface number.
        ZTAPBRGNUM=`printf "%04d" "${NMAPTAPRTIDNT[${ZRGTDEVID}]}"`

        #--- Creates the script of commands (for exec with root privileges).
        ZSTARTCMD="`mktemp \"${CFGTEMPDIRECTORY}/cntapcmds-XXXXXXXX\" 2> /dev/null`"
        echo "#!/usr/bin/env bash" > "${ZSTARTCMD}"
        echo "PATH=\"${PATH}\"" >> "${ZSTARTCMD}"
        echo "pushd \"`pwd`\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Kill the original connection.
        echo "kill \"${NMAPTAPPLRUNN[${ZRGTDEVID}]}\" > /dev/null 2> /dev/null" >> "${ZSTARTCMD}"
        echo "sleep 1" >> "${ZSTARTCMD}"

        #--- Start the connection: connect the iou2net device using the tap / bridge mode.
        echo "\"${CFGSCRIPTDIR}/${CFGIOU2NET}\" -n \"${ZNMAPNET}\" -p ${NMAPTAPRTIDNT[${ZRGTDEVID}]} -t \"${CFGINTFINTTAP}${ZTAPBRGNUM}\" > /dev/null 2> /dev/null &" >> "${ZSTARTCMD}"

        #--- Get new PID and change owner.
        echo "ZPIDSNIFF=\$!" >> "${ZSTARTCMD}"
        echo "echo \$ZPIDSNIFF >> \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "chown ${UID} \"${ZPIDFILE}\"" >> "${ZSTARTCMD}"
        echo "sleep 3" >> "${ZSTARTCMD}"

        #--- Finish the subscript.
        echo "popd >/dev/null 2> /dev/null" >> "${ZSTARTCMD}"

        #--- Command to get root privileges.
        ZROOTCMD="sudo"
        ZROOTUSR="${USER}"
        if [ ${CFGROOTSUDOSU} -ne 0 ]; then
            ZROOTCMD="su root -c"
            ZROOTUSR="root"
        fi

        #--- Execute the script.
        chmod 700 "${ZSTARTCMD}"
        ln -s "/tmp/netio${UID}" "/tmp/netio0" > /dev/null 2> /dev/null
        echo "=> To stop the capture, we need root privileges."
        echo "=> Enter the ${ZROOTUSR} password."
        ${ZROOTCMD} "${ZSTARTCMD}"
        rm -f "/tmp/netio0" > /dev/null 2> /dev/null
        rm -f "${ZSTARTCMD}"

        #--- Check if is running.
        parseexttap "FORCE"
        ZPIDSNIFF=`cat "${ZPIDFILE}" 2> /dev/null`
        rm -f "${ZPIDFILE}"
        if [ ${NMAPTAPPLRUNN[${ZRGTDEVID}]} -eq 0 ] || [ "${NMAPTAPPLRUNN[${ZRGTDEVID}]}" != "${ZPIDSNIFF}" ]; then
            echo "ERROR: the capture has failed to stop, check the conn status."
            exit 105
        fi

    #========== Invalid connection type ==========
    else
        echo "ERROR: internal error."
        exit 90
    fi

    #--- Info.
    echo "capture in ${ZPARAMACRON} stopped."

    #--- Delete the extra capture of iousniff.
    sleep 0.5
    find "${ZTEMPDIR}/" -type f -name '*.pcap' \! -name "${ZLFTDEVID}-*.pcap" -exec rm -f "{}" \;

    #--- Move the capture files.
    mkdir -p "${CFGCAPTURESDIR}" > /dev/null 2> /dev/null
    chmod 700 "${CFGCAPTURESDIR}"
    ZPCAPFILE=`find "${ZTEMPDIR}/" -type f -name "${ZLFTDEVID}-*.pcap" | sort | tail -n 1`
    if [ -s "${ZPCAPFILE}" ]; then
        ZPCAPBASE=`basename "${ZPCAPFILE}" | sed -e 's/\.pcap$//'`
        ZSLTPRT=`echo "${ZPCAPBASE}" | cut -d '-' -f 2 | tr '.' '-'`
        ZDATECAP=`echo "${ZPCAPBASE}" | cut -d '-' -f 3`
        ZDATECAP=`date -d "1970-01-01 00:00:00 +0000 + ${ZDATECAP} seconds" '+%Y%m%d-%H%M%S'`
        if [ ! -z "${ZDESCRIP}" ]; then
            ZDESCRIP="-${ZDESCRIP}"
        fi
        ZFINALCAP="${CFGCAPTURESDIR}/cap-${ZPARAMACRON}-${ZSLTPRT}-${ZDATECAP}${ZDESCRIP}.pcap"

        #--- Strip the empty frames in pcap file (iou2net bug?).
        tcpdump -n -r "${ZPCAPFILE}" -w "${ZFINALCAP}" 'greater 1' 2> /dev/null
        ZERROR=$?
        if [ ${ZERROR} -ne 0 ]; then
            #--- DLT of pcap file not supported by tcpdump.
            cp -f "${ZPCAPFILE}" "${ZFINALCAP}"
        fi
        rm -f "${ZPCAPFILE}"
        echo "file: ${ZFINALCAP}"
    fi

    #--- Delete the temporal files.
    rm -f "${ZTEMPDIR}/capturing-${ZLFTDEVID}.flag"
    rm -f "${ZTEMPDIR}/capturing-${ZRGTDEVID}.flag"
    rm -f "${ZTEMPDIR}/capturing-${ZLFTDEVID}.info"
    rm -f "${ZTEMPDIR}/capturing-${ZLFTDEVID}.nmap"

    return
    }

#--- Show the list of captures files.
function do_capturelist {
    local ZLOCALADDR
    local ZLSTFILES

    #--- Get the list of capture files.
    ZLSTFILES=`find "${CFGCAPTURESDIR}/" -type f -name 'cap-*.pcap' 2> /dev/null`

    #--- Inform.
    ZLOCALADDR=`getlocalip`
    echo "capture files in `hostname -s` [${ZLOCALADDR}]:"
    if [ -z "${ZLSTFILES}" ]; then
        echo "  no capture files."
    else
        echo "${ZLSTFILES}" | sed -e 's/^/  /'
    fi
    return
    }

#--- Show a brief help about capture.
function do_capturehelp {
    local ZINDEX

    echo ""
    echo "captureon usage:"
    echo "captureon DEV SL/PRT TYPE [DESC]: start capture traffic (tcpdump/tshark)."
    echo "  DEV: the acronym of the IOU device (for example: R1)."
    echo "  SL/PRT: slot and port (for example: 1/3)."
    echo "  TYPE: type of encapsulation of the interface. See below."
    echo "  DESC: optional brief description about the capture, for example: TestBGP"

    echo ""
    echo "For example, this command:"
    echo "  captureon R1 0/3 eth TestBGP"
    echo "produces this capture file:"
    echo "  /tmp/runiou{UID}/captures/cap-R1-0-3-{DATE}-{TIME}-TestBGP.pcap"

    echo ""
    echo "Is mandatory to enter the encapsulation type. See the IOUsniffer docs."
    echo "The encapsulation type can be any of this:"
    ZINDEX=0
    while [ ${ZINDEX} -lt ${#CFGPCAPENCAPSHORT[@]} ]; do
        printf "%9s - %s\n" "${CFGPCAPENCAPSHORT[${ZINDEX}]}" "${CFGPCAPENCAPDESCR[${ZINDEX}]}"
        ZINDEX=$(( ${ZINDEX} + 1 ))
    done

    echo ""
    echo "If you need another encapsulation, please add it to the runiou script"
    echo "in the CFGPCAPENCAP* vars. See http://www.tcpdump.org/linktypes.html"
    echo "for the complete list of encapsulation types."

    echo ""
    return
    }

#--- Creates a netmap file with the template.
function do_netmap {
    #--- If the netmap file not exist in the current directory,
    #--- creates them.

    #--- Check if exist.
    if [ -f "./${CFGNETMAPFILE}" ]; then
        echo "ERROR: ${CFGNETMAPFILE} already exist in the current directory."
        echo "SOLUTION: manually delete the ${CFGNETMAPFILE} file first."
        return
    fi

    #--- Creates the file.
    loc_template
    echo "${NETMAPTEMPLT}" > "${CFGNETMAPFILE}"
    echo "template ${CFGNETMAPFILE} file created."

    return
    }

#--- Display the netmap template.
function do_template {
    loc_template
    echo "${NETMAPTEMPLT}"
    return
    }

#--- Show usage information.
function showusage {
    echo "runiou - a bash script frontend for Cisco IOU."
    echo
    echo "Usage: "`basename "${0}"`" command [args...]"
    echo " commands:"
    echo "  start:               start all iou devices and external connections."
    echo "  stop:                stop all iou devices and external connections."
    echo "  rtstart ACR [ACR]:   start some iou devices."
    echo "  rtstop ACR [ACR]:    stop some iou devices."
    echo "  cnstart ID1 [ID2]:   start some external connections."
    echo "  cnstop ID1 [ID2]:    stop some external connections."
    echo "  cndeconfigure:       force deconfigure unused tap, bridge and eth interfaces."
    echo "  cnhelp:              help for integrate the external connections."
    echo "  show:                show information about the netmap file."
    echo "  list:                equivalent to show."
    echo "  telnet:              console to all iou and external devices."
    echo "  console:             equivalent to telnet."
    echo "  rttelnet ACR [ACR]:  console to some iou devices and/or external devices."
    echo "  rtconsole ACR [ACR]: equivalent to rttelnet."
    echo "  netmap:              creates a netmap template file in the current dir."
    echo "  template:            display the netmap template without file creation."
    echo "  captureon DEV SL/PRT TYPE [DESC]: start capture traffic (tcpdump/tshark)."
    echo "  captureoff DEV:      stop capture traffic."
    echo "  capturelist:         show the list of captures."
    echo "  capturehelp:         show a brief help for capture."
    echo "  export:              export all iou devices startup-config."
    echo "  rtexport ACR [ACR]:  export some iou devices startup-config."
    echo "  rtimport ACR FILE:   import startup-config to an iou device."
    echo "  version:             show version."
    echo "  help:                show this help."
    return
    }

#--- Show version.
function do_shversion {
    echo "runiou version 0055"
    echo "(c) 2012-2019 by miguel scapolla"
    echo "License GPLv3+: GNU GPL version 3 or later"
    return
    }

#--- Execute the command line.
function execmcdline {
    case "${1}" in
        "start")
            do_start
            ;;
        "stop")
            do_stop
            ;;
        "rtstart")
            shift
            do_rtstart "$@"
            ;;
        "rtstop")
            shift
            do_rtstop "$@"
            ;;
        "cnstart")
            shift
            do_cnstart "$@"
            ;;
        "cnstop")
            shift
            do_cnstop "$@"
            ;;
        "cndeconfigure")
            do_cndeconfigure
            ;;
        "cnhelp")
            do_cnudphelp
            do_cntaphelp
            ;;
        "show"|"list")
            do_showlist
            ;;
        "telnet"|"console")
            do_telnetcons
            ;;
        "rttelnet"|"rtconsole")
            shift
            do_rttelcons "$@"
            ;;
        "netmap")
            do_netmap
            ;;
        "template")
            do_template
            ;;
        "captureon")
            shift
            do_captureon "$@"
            ;;
        "captureoff")
            shift
            do_captureoff "$@"
            ;;
        "capturelist")
            do_capturelist
            ;;
        "capturehelp")
            do_capturehelp
            ;;
        "export")
            do_export
            ;;
        "rtexport")
            shift
            do_rtexport "$@"
            ;;
        "rtimport")
            shift
            do_rtimport "$@"
            ;;
        "version")
            do_shversion
            ;;
        "help|--help")
            showusage
            ;;
        *)
            showusage
            ;;
    esac
    return
    }

#--- Start of script.
checkallscr "$@"
execmcdline "$@"

#--- End.
exit 0
